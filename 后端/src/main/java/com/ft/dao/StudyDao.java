package com.ft.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ft.domain.Post;
import com.ft.domain.Study;
import org.apache.ibatis.annotations.*;

/**
 * @Author: ft
 * @Date: 2023/01/11/16:17
 * @Description:
 */
@Mapper
public interface StudyDao extends BaseMapper<Study> {
    /**
     * 增加帖子
     * @param study post对象
     * @return 受影响的行数
     */
    @Insert ("insert into study(id,type,content,issue_user,issue_user_name,issue_user_img,praisenum,commentnum,sharenum,location,praise_useridlist,commentidlist,post_imglist," +
            "post_indeximg,state) values(#{id},#{type},#{content},#{issueUser},#{issueUserName},#{issueUserImg},#{praisenum},#{commentnum},#{sharenum}," +
            "#{location},#{praiseUseridlist},#{commentidlist},#{postImglist},#{postIndeximg},#{state})")
    public int addStudy(Study study);
    /**
     * 获取帖子的创建时间信息
     * @param id 订单号
     * @return 时间
     */
    @Select("select create_time from study where id =#{id}")
    public String getcreateTime(Long id);

    /**
     * 获取帖子的更新时间
     * @param id 订单号
     * @return 时间
     */
    @Select ("select update_time from study where id =#{id}")
    public String getupdateTime(Long id);

    /**
     * 获取点赞用户id集合
     * @param id
     * @return
     */
    @Select ("select praise_useridlist from study where id =#{id}")
    public String getpraiseUseridlist(Long id);
    /**
     * 增加，修改帖子点赞数和点赞用户集合
     * @param praise 用户id集合
     * @param number 点赞个数
     * @return
     */
    @Update ("update study set praise_useridlist = #{praise}, praisenum = #{number} where id =#{id}")
    public int addAndUpdatePraiseAndNumber(String praise,int number,Long id);

    /**
     * 修改所有评论回复数量
     * @param commentnum
     * @return
     */
    @Update ("update study set commentnum = #{commentnum} where id =#{id}")
    public int undataCommentnum(int commentnum,Long id);
    //修改评论用户id集合
    @Update ("update study set commentidlist = #{commentidlist} where id =#{id}")
    public int updataCommentList(String commentidlist,Long id);

    /**
     * 获取所有回复数量
     * @param id
     * @return
     */
    @Select ("select commentnum from study where id =#{id}")
    public int getCommentnum(Long id);
    /**
     * 更新帖子的回复数
     * @param id
     * @return
     */
    @Update ("update study set commentnum = #{commentnum} where id =#{id}")
    public int updataCommentnum(int commentnum, Long id);
    /**
     * 增加帖子的评论
     * @param commentidlist 评论id集合字符串
     * @param id 增加的评论id
     * @return 受影响的行数
     */
    @Update ("update study set commentidlist = #{commentidlist},commentnum = #{commentnum} where id = #{id}")
    public int addPraiseCommentidList(String commentidlist, int commentnum, Long id);

    /**
     * 获取用户评论id字段
     * @param id
     * @return
     */
    @Select ("select commentidlist from study where id = #{id}")
    public String getCommentidList(Long id);
    //根据id删除字段
    @Delete ("delete  from study where id = #{id}")
    public int deleteStudy(Long id);
}

