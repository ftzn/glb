/**
 * @Aurhor 把手给我
 * @Date 2022/9/6 20:20
 * @Version 1.0
 * websoket配置类
 **/
package com.ft.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

/**
 * 开启WebSocket支持,websocket的配置类
 * @author ft
 */
@Configuration
public class MySoketConfig {
    @Bean
    public ServerEndpointExporter serverEndpointExporter() {
        return new ServerEndpointExporter ();
    }
}
