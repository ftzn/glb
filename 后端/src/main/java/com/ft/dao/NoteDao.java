package com.ft.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ft.domain.Note;
import org.apache.ibatis.annotations.*;

/**
 * @Author: ft
 * @Date: 2023/02/09/16:47
 * @Description:
 */
@Mapper
public interface NoteDao extends BaseMapper<Note> {
    /**
     * 增加纸条
     * @param note
     * @return
     */
    @Insert ("insert into note(id,title,content,issue_userid,issue_username,issue_userimg,belong_userid,belong_username,belong_userimg,sex,state)" +
            " values(#{id},#{title},#{content},#{issueUserid},#{issueUsername},#{issueUserimg},#{belongUserid},#{belongUsername},#{belongUserimg},#{sex},#{state})")
    public int addNote(Note note);

    /**
     * 删除纸条
     * @param id
     * @return
     */
    @Delete ("delete from note where id = #{id}")
    public int deleteNote(Long id);

    /**
     * 获取纸条创建时间
     * @param id
     * @return
     */
    @Select("select create_time from note where id =#{id}")
    public String getcreateTime(Long id);

    /**
     * 修改纸条标题内容
     * @param id
     * @return
     */
    @Update("update note set title = #{title},content = #{content} where id = #{id}")
    public int updateNote(String title,String content,Long id);

    /**
     * 获取纸条所有信息
     * @param id
     * @return
     */
    @Select ("select * from note where id = #{id}")
    public Note getNoteById(Long id);
    /**
     * 或者纸条是男生纸条还是女生纸条
     * @param id
     * @return
     */
    @Select ("select sex from note where id = #{id}")
    public int getNoteSex(Long id);
}
