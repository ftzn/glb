package com.ft.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ft.dao.BuytypeDao;
import com.ft.domain.Buytype;
import com.ft.service.IBuytypeService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author com/ft
 * @since 2022-08-24
 */
@Service
public class NeedtypeServiceImpl extends ServiceImpl<BuytypeDao, Buytype> implements IBuytypeService {

}
