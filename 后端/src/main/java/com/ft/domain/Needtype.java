package com.ft.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @Author: ft
 * @Date: 2022/11/21/16:57
 * @Description:
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("needtype")
public class Needtype implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String name;

    /**
     * 0正常，1锁定，2删除
     */
    private Integer state;


}
