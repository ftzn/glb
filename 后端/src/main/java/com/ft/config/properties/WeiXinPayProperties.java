package com.ft.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
@Configuration
@ConfigurationProperties("weixin.pay.info")
@Data
public class WeiXinPayProperties {
    private String gateway;
    private String tkurl;
    private String tucxurl;
    private String txurl;
    private String appsecret;
    private String mchid;
    private String appid;
    private String type;
    private String notifyPath;
}
