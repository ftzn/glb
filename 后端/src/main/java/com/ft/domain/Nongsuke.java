package com.ft.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * @Author: ft
 * @Date: 2023/02/08/9:01
 * @Description:
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("nongsuke")
public class Nongsuke implements Serializable {
    private static final long serialVersionUID = 1L;
    //所有属性的总和，每增加一个加1
    public static final int total = 19;
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    //商品类型
    private Integer type;
    /**
     *  1已付款，2上架中，3交易中，4完成，5问题订单
     */
    private Integer state;
    //标题
    private String title;
    /**
     * 订单内容
     */
    private String content;

    /**
     * 发布用户
     */
    private Integer issueUser;
    /**
     * 发布用户名
     */
    private String issueUserName;
    /**
     * 发布用户头像
     */
    private String issueUserImg;

    /**
     * 支付方式，0支付宝,1微信
     */
    private Integer payType;

    /**
     * 订单创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Timestamp createTime;

    /**
     * 订单修改时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Timestamp updateTime;

    /**
     * 订单完成时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Timestamp overTime;
    //价格
    private Double price;
    //商品树数量
    private int num;
    //订单图片地址
    private String orderImg;
    //订单首页图片
    private String indexImg;
    /**
     * 订单所属类型id
     */
    private Integer ordertypeid;
    /**
     * 购买人数
     */
    private int buyusernum;
    /**
     * 商品规格描述
     */
    private String norms;

}
