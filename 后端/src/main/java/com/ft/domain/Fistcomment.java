package com.ft.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * @Author: ft
 * @Date: 2023/01/11/15:07
 * @Description:
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("fistcommen")
public class Fistcomment implements Serializable {
    private static final long serialVersionUID = 1L;
    //所有属性的总和，每增加一个加1
    public static final int total = 11;
    //id
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    //评论内容
    private String content;
    //评论用户id
    private int commentUserid;
    //评论用户名称
    private String commentUsername;
    //评论用户头像
    private String commentUserimg;
    //回复给评论的回复id集合
    private String answerIdlist;
    //所属上级帖子id
    @JsonSerialize(using = ToStringSerializer.class)
    private Long belongpost;
    //帖子创建时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp createTime;
    //帖子修改时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp updateTime;
    //帖子删除时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp overTime;
    //评论状态
    private int state;
}
