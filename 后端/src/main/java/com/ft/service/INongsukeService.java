package com.ft.service;

import com.ft.domain.Nongsuke;
import com.ft.domain.Tzmarket;

import java.util.List;

/**
 * @Author: ft
 * @Date: 2023/02/08/10:14
 * @Description:
 */
public interface INongsukeService {
    //增加商品
    public int addNongsuke(Nongsuke nongsuke) throws IllegalAccessException;
    //查询所有商品
    public Nongsuke getNongsuke(Long id);
    /**
     * 查询指定字段的数据用于微信支付
     * @param id 订单id
     * @return 订单对象
     */
    public Nongsuke selectNongsukeToPay(Long id);

    /**
     * 付款后修改订单状态
     * state:修改的状态
     * id:订单id
     * userid:用户id
     * @return 返回受影响的结果
     */
    public int  updataOrder(int state,String id,int userid);
    /**
     * 跳蚤市场分页获取订单数据
     * @param page
     * @return
     */
    public List getOrder(int type, int page);

    /**
     * 跳蚤市场领搜索内容
     * @param content 搜索的数据
     * @return 订单集合
     */
    public List search(String content,int type,int page);

    public Nongsuke getNongsukeByIdAndOne(String id);

    public List getUserIssueOrderList(int userid);
}
