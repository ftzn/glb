package com.ft.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.ft.domain.Message;
import com.ft.domain.User;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ft.domain.UserAddress;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Set;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author com/ft
 * @since 2022-08-24
 */
public interface IUserService extends IService<User> {

    /**
     * 登录校验
     *
     * @return
     */
    public int loginCheckUser(String username, String password);

    /**
     * 发送验证码
     * 返回值：-1失败，0出现问题，1成功
     */
    public int sendMessage(String phone);

    /**
     * 短信验证码登录校验
     */
    public int PhoneLoginCheck(String phone, String code);

    /**
     * 忘记密码发送验证码功能
     *
     * @param phone
     * @return
     */
    public int wjmmSendMessage(String phone);

    /**
     * 重置密码功能
     *
     * @param phone
     * @param code
     * @return
     */
    public int resetPassword(String phone, String code, String password);

    /**
     * 添加用户
     *
     * @param user 传入的用户对象
     * @return
     */
    public int addUser(User user);

    /**
     * 根据手机号查询用户
     *
     * @param phone 查询的手机号
     * @return 返回用户对象
     */
    public User selectUserByPhone(String phone);

    /**
     * 根据ID查询用户
     *
     * @param ID 查询的手机号
     * @return 返回用户对象
     */
    public User selectUserByID(String ID);

    //获取用户性别
    public int getUserSex(int id);

    /**
     * 修改用户信息
     *
     * @param user 传入修改的用户对象
     * @return 返回影响结果数 1成功，0失败
     */
    public int updateUser(User user);

    /**
     * 查询所有用户信息
     *
     * @return 返回所有用户List集合
     */
    public List<User> selectAll();

    /**
     * 通过id查询用户
     *
     * @return
     */
    public User selectUserInfoById(int id);

    /**
     * 根据传入的用户id对用户做逻辑删除
     *
     * @param id 要删除用户的id
     * @return 返回受影响的结果
     */
    public int deleteUser(int id);

    /**
     * 查询已经逻辑删除的用户
     *
     * @return 返回查询的list集合
     */
    public List<User> selectByState();

    /**
     * 根据传入的页码查询页面的数据
     *
     * @param page 要查询的第几页的页码
     * @return 返回查询的List结果集
     */
    public List<User> selectDataByPage(int page);

    /**
     * 获取用户好友列表
     *
     * @return 用户列表集合
     */
    public List<User> getUserList(String userid);

    /**
     * str 传入地址字符串
     *
     * @return 1 增加成功 ，0失败
     */
    public int addUserAddress(UserAddress str, int id) throws JsonProcessingException;

    /**
     * 查询用户地址信息
     *
     * @return
     */
    public List<UserAddress> selectAddress(int id) throws JsonProcessingException;

    /**
     * 根据用户id和地址下标修改地址
     *
     * @param id    修改地址的用户id
     * @param index 修改地址的下标
     * @return 受影响的行数
     */
    public int deleteUserAddress(int id, int index);

    /**
     * 修改用户地址信息
     *
     * @param id          修改地址的用户id
     * @param userAddress 修改用户的地址
     * @return 受影响的行数
     */
    public int updateUserAddress(int id, Object userAddress);

    /**
     * 获取用户离线消息
     *
     * @return
     */
    public List getOfflineUsermessage(int userid);

    /**
     * 添加用户离线信息
     *
     * @param message
     * @return
     */
    public int addOfflineUsermessage(Message message) throws IllegalAccessException;

    /**
     * 删除用户离线信息
     *
     * @param userid
     * @return
     */
    public int deleteOfflineUsermessage(int userid);

    /**
     * 添加好友列表
     * @param userid
     * @return
     */
    public int addUserFrientList(String userid,int touserid);

    /**
     * 修改用户信息-个人
     * @param user
     * @return
     */
    public int updateUserInfo(User user);

    public double getUserMeney(int userid);
}
