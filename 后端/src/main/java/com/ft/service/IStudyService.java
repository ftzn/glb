package com.ft.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ft.domain.Post;
import com.ft.domain.Study;
import com.ft.utils.Code;
import com.ft.utils.Result;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Author: ft
 * @Date: 2023/01/11/17:02
 * @Description:
 */
public interface IStudyService extends IService<Study> {
    /**
     *增加帖子
     * @param post post对象
     * @return
     */
    public int addStudy(Study study) throws IllegalAccessException;

    /**
     * 获取帖子信息
     * @return 帖子集合
     * @throws IllegalAccessException
     */
    public List getStudyList(int type,int page);

    /**
     * 修改用户点赞数量和用户点赞id集合
     * @param praise
     * @param number
     * @return
     */
    public int addPraiseAndNumber(String praise,int number, Long id);

    /**
     * 搜索内容
     * @param content
     * @param type
     * @param page
     * @return
     */
    public List search(String content,int type,int page) ;
    //获取点赞用户id集合
    public String getPraiseUseridlist(Long id);
    @Transactional
    //删除帖子
    public int deleteStudy(Long id);
}
