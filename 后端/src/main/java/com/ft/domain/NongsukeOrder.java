package com.ft.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.sql.Timestamp;

@Data
@EqualsAndHashCode(callSuper = false)
@TableName("nongsukeordercontent")
public class NongsukeOrder implements Serializable {
    private static final long serialVersionUID = 1L;
    //所有属性的总和，每增加一个加1
    public static final int total = 14;
    //id
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    //订单标题
    private String title;
    //订单内容
    private String content;
    //商品id
    @JsonSerialize(using = ToStringSerializer.class)
    private String goodsid;
    //下单时商品单价
    private Double goodprice;
    //下单时商品分类
    private int goodtype;
    //购买的商品数量
    private int buynum;
    //商品总价
    private Double totalprice;
    //订单创建时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp createTime;
    //订单修改时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp updateTime;
    //订单删除时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp overTime;
    //订单状态
    private int state;
    //发布用户id
    private int issuseuserid;
    //订单图片
    private String imglist;
    //展示图片
    private String indeximg;
    //所属上级订单
    private String belongid;
}
