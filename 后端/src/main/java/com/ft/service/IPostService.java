package com.ft.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ft.domain.Post;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Author: ft
 * @Date: 2023/01/11/17:02
 * @Description:
 */
public interface IPostService extends IService<Post> {
    /**
     *增加帖子
     * @param post post对象
     * @return
     */
    public int addPost(Post post) throws IllegalAccessException;

    /**
     * 获取帖子信息
     * @return 帖子集合
     * @throws IllegalAccessException
     */
    public List getPostList(int type,int page);

    /**
     * 修改用户点赞数量和用户点赞id集合
     * @param praise
     * @param number
     * @return
     */
    public int addPraiseAndNumber(String praise,int number, Long id);

    /**
     * 搜索内容
     * @param content
     * @param type
     * @param page
     * @return
     */
    public List search(String content,int type,int page) ;
    //获取点赞数量
    public String getPostcommentNum(Long id);
    @Transactional
    //删除帖子
    public int deletePost(Long id);
}
