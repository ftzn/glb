package com.ft.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ft.domain.Buytype;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author com/ft
 * @since 2022-08-24
 */
@Mapper
public interface NeedtypeDao extends BaseMapper<Buytype> {

}
