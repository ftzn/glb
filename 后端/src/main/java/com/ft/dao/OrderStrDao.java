package com.ft.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ft.domain.Buyorder;
import com.ft.domain.OrderStr;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface OrderStrDao extends BaseMapper<OrderStr> {

    //添加退款id表
    @Insert("insert into orderstr(id,belongorderid,tkstrorderid,state) values(#{id},#{belongorderid},#{tkstrorderid},#{state})")
    public int addOrderStr(OrderStr orderStr);

    //查询退款id表
    @Select("select * from orderstr where belongorderid = #{orderid}")
    public OrderStr getOrderStr(String orderid);

    //修改退款id
    @Update("update orderstr set tkstrorderid=#{tkstrorderid} where belongorderid = #{orderid}")
    public int updateOrderStr(String tkstrorderid,String orderid);
}
