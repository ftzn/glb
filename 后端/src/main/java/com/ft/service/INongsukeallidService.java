package com.ft.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ft.domain.Nongsuke;
import com.ft.domain.NongsukeOrder;
import com.ft.domain.Nongsukeallid;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;

import java.io.IOException;
import java.util.List;

@Transactional
public interface INongsukeallidService extends IService<Nongsukeallid> {
    //增加商品
    public int addOrder(Nongsukeallid order) throws IllegalAccessException;
    //根据id查询订单所有数据
    public Nongsukeallid getOrder(Long id);
    /**
     * 查询指定字段的数据用于微信支付
     * @param id 订单id
     * @return 订单对象
     */
    public Nongsukeallid selectOrderToPay(Long id);

    /**
     * 付款后修改订单状态
     * state:修改的状态
     * id:订单id
     * @return 返回受影响的结果
     */
    public int  updataOrderState(String id,int userid);
    /**
     * 获取用户所有订单数据
     * @return
     */
    public List getOrderAllToUser(int userid);

    /**
     * 用户退款
     * @param orderid
     * @return
     */
    public int updateOrderRefund(String orderid);

    /**
     * 获取单个订单数据
     * @param orderid
     * @return
     */
    public Nongsukeallid getNongsukeallidByOne(Long orderid);

    /**
     * 用户完成订单
     * @param orderid
     * @param userid
     * @return
     * @throws IOException
     * @throws IllegalAccessException
     */
    public int issueOverOrder(String orderid, int userid) throws IOException, IllegalAccessException;
}
