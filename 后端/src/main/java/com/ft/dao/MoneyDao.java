package com.ft.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ft.domain.MoneyInfo;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface MoneyDao extends BaseMapper<MoneyInfo> {

    /**
     * 添加交易明细
     * @param moneyInfo
     * @return
     */
    @Insert("insert into moneyinfo(id,getmoenyorderid,issueuser,continueuser,money,state) values(#{id},#{getmoenyorderid},#{issueuser},#{continueuser},#{money},#{state})")
    public int addMoneyInfo(MoneyInfo moneyInfo);

    /**
     * 获取用户交易明细
     * @return
     */
    @Select("select * from moneyinfo where continueuser=#{continueuser}")
    public List<MoneyInfo> getMoneyInfoByUser(int userid);
}
