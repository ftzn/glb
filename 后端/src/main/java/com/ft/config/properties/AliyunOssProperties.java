package com.ft.config.properties;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.ObjectMetadata;
import com.aliyun.oss.model.PutObjectRequest;
import com.aliyun.oss.model.VoidResult;
import com.baomidou.mybatisplus.extension.exceptions.ApiException;
import com.ft.utils.Base64ToMultipartFile;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import lombok.Data;
import lombok.Synchronized;
import net.sf.jsqlparser.expression.DateTimeLiteralExpression;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.MultipartFile;
import sun.misc.BASE64Decoder;

import java.io.*;
import java.util.UUID;

/**
 * @Author: ft
 * @Date: 2022/10/17/8:51
 * @Description: 阿里云OSS对象参数
 */
@Configuration
@ConfigurationProperties("aliyun.oss")
@Data
public  class AliyunOssProperties {
    private String endpoint;
    private String accessKeyId;
    private String accessKeySecret;
    private String bucketName;

    /**
     * 上传图片
     * @param base64 前端传入的base64图片
     * @return 返回上传阿里云oss的图片链接地址
     * @throws Exception
     */
    public String uploadOneFile(String base64,String name) throws Exception {
        //将base64转换成MultipartFile
        MultipartFile multipartFile = Base64ToMultipartFile.base64ToMultipart (base64);
        String s = uploadFileAvatar (multipartFile,name);
        return s;
    }

    /**
     * 删除图片
     * @param fileUrl 图片链接
     * @return 是否删除成功
     */
    public boolean deleteImages(String fileUrl) {
        //创建OSSClient实例
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
        String fileName = fileUrl.replace("https://ft-glb.oss-cn-chengdu.aliyuncs.com/", "");
        // 根据BucketName,objectName删除文件
        ossClient.deleteObject (bucketName, fileName);
        boolean found = ossClient.doesObjectExist(bucketName, fileName);
        ossClient.shutdown();
        return found;
    }

    /**
     * 上传图片
     * @param file 图片MultipartFile对象
     * @return
     */
    public  String uploadFileAvatar(MultipartFile file, String name) {
         try {
            // 创建OSSClient实例。
            OSS ossClient = new OSSClientBuilder ().build (endpoint, accessKeyId, accessKeySecret);

            // 获取上传文件的输入流
            InputStream inputStream = file.getInputStream ();
            //调用OSS方法实现上传
            //设置返回参数
            ObjectMetadata objectMetadata = new ObjectMetadata ();
            objectMetadata.setContentType ("image/jpg");
            objectMetadata.setContentDisposition ("inline");
            //第一个参数 Bucket名称
            //第二个参数  上传到OSS文件路径和文件名称
            //第三个参数  上传文件输入流
            PutObjectRequest putObjectRequest = new PutObjectRequest (bucketName, name, inputStream,objectMetadata);
            ossClient.putObject(putObjectRequest);
            // 关闭OSSClient。
            ossClient.shutdown ();
            //把上传之后文件路径返回
            //阿里云oss路径手动拼接
            String url = "https://" + bucketName + "." + endpoint + "/" + name;
            return url;
        } catch (Exception e) {
            e.printStackTrace ();
            return null;
        }
    }

}