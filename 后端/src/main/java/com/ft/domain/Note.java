package com.ft.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * @Author: ft
 * @Date: 2023/02/09/16:48
 * @Description: 纸条实体类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("note")
public class Note implements Serializable {
    private static final long serialVersionUID = 1L;
    //所有属性的总和，每增加一个加1
    public static final int total = 14;
    //id
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    //纸条标题
    private String title;
    //纸条内容
    private String content;
    //发表该纸条的用户id
    private int issueUserid;
    //发布的用户名称
    private String issueUsername;
    //发布的用户头像
    private String issueUserimg;
    //抽到的用户id
    @JsonSerialize(using = ToStringSerializer.class)
    private Long belongUserid;
    //抽到的用户名称
    private String belongUsername;
    //抽到的用户头像
    private String belongUserimg;
    //男生纸条，女生纸条
    private int sex;
    //帖子创建时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp createTime;
    //帖子修改时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp updateTime;
    //帖子删除时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp overTime;
    //回复状态
    private int state;
}
