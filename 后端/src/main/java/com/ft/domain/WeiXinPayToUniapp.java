package com.ft.domain;

import lombok.Data;

/**
 * @Author: ft
 * @Date: 2022/10/10/18:09
 * @Description: 微信支付，预支付返回uniapp数据类
 */
@Data
public class WeiXinPayToUniapp {
    //商品订单id
    private String orderid;
    //支付类型固定值
    private String provider ;
    //时间戳
    private String timeStamp ;
    //随机字符串
    private String nonceStr ;
    //固定值
    private String packAge ;
    //签名类型
    private String signType;
    //签名
    private String paySign;
}
