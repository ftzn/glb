package com.ft.utils;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author: ft
 * @Date: 2022/11/02/13:22
 * @Description:
 */
public class ObjectUtils {
    /**
     * 获取利用反射获取类里面的值和名称
     *
     * @param obj
     * @return
     * @throws IllegalAccessException
     */
    public static Map<String, Object> objectToMap(Object obj) throws IllegalAccessException {
        Map<String, Object> map = new HashMap<> ();
        Class<?> clazz = obj.getClass();
        System.out.println(clazz);
        for (Field field : clazz.getDeclaredFields()) {
            field.setAccessible(true);
            String fieldName = field.getName();
            //排除还不需要的属性，便于后续封装redis
            if(fieldName.equals ("serialVersionUID") | fieldName.equals ("total")){
                continue;
            }
            Object value = field.get(obj);
            map.put(fieldName, value);
        }
        return map;
    }
}
