package com.ft.domain;

/**
 * @Author: ft
 * @Date: 2022/10/17/10:08
 * @Description: base64上传图片实体类
 */
public class aliyun {
    //base64字符串
    private String base64;
    //上传的文件名
    private String name;

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getBase64() {
        return base64;
    }

    public void setBase64(String base64) {
        this.base64 = base64;
    }
}
