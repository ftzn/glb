/**
 * @Aurhor 把手给我
 * @Date 2022/8/30 9:16
 * @Version 1.0
 * 服务器端发送给客服端的消息类
 **/
package com.ft.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.sql.Timestamp;

/**
 * 系统公告表
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("systemmessage")
public class SystemMessage {
    private static final long serialVersionUID = 1L;
    //所有属性的总和，每增加一个加1
    public static final int total = 11;
    //系统公告id
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    //标题
    private String title;
    //内容
    private String content;
    //公告图片
    private String imglist;
    //发布用户
    private int issueUserId;
    //发布用户名
    private String issueUserName;
    //发布用户头像
    private String issueUserImg;
    //状态
    private int state;
    //创建时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private String createTime;
    //修改时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp updateTime;
    //结束时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp overTime;

}
