/**
 * @Aurhor 把手给我
 * @Date 2022/8/30 9:10
 * @Version 1.0
 * 客户端发送给服务器端的消息类
 **/
package com.ft.domain;

public class Message {
    //该消息的id
    private String id;
    //发送消息用户
    private String sendUser;
    //发送用户名称
    private String sendUserName;
    //发送用户头像
    private String sendUserImg;
    //接收信息的用户
    private String receiveUser;
    //发送的内容
    private String messagecontent;
    //是否是系统消息,默认0系统消息，1好友消息,2添加好友，3,心跳校测,4,上线提示,5发送的消息是否成功，6，订单相关通知消息
    private int isSystem = 0;
    //消息为6时创建时间
    private String createTime ;
    public String getSendUser() {
        return sendUser;
    }

    public void setSendUser(String sendUser) {
        this.sendUser = sendUser;
    }

    public String getReceiveUser() {
        return receiveUser;
    }

    public void setReceiveUser(String receiveUser) {
        this.receiveUser = receiveUser;
    }

    public String getMessagecontent() {
        return messagecontent;
    }

    public void setMessagecontent(String messagecontent) {
        this.messagecontent = messagecontent;
    }

    public int getIsSystem() {
        return isSystem;
    }

    public void setIsSystem(int isSystem) {
        this.isSystem = isSystem;
    }

    public String getSendUserName() {
        return sendUserName;
    }

    public void setSendUserName(String sendUserName) {
        this.sendUserName = sendUserName;
    }

    public String getSendUserImg() {
        return sendUserImg;
    }

    public void setSendUserImg(String sendUserImg) {
        this.sendUserImg = sendUserImg;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "Message{" +
                "id='" + id + '\'' +
                ", sendUser='" + sendUser + '\'' +
                ", sendUserName='" + sendUserName + '\'' +
                ", sendUserImg='" + sendUserImg + '\'' +
                ", receiveUser='" + receiveUser + '\'' +
                ", messagecontent='" + messagecontent + '\'' +
                ", isSystem=" + isSystem +
                ", createTime='" + createTime + '\'' +
                '}';
    }
}
