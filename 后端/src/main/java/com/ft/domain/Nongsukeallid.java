package com.ft.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.sql.Timestamp;

@Data
@EqualsAndHashCode(callSuper = false)
@TableName("nongsukeallid")
public class Nongsukeallid {
    private static final long serialVersionUID = 1L;
    //所有属性的总和，每增加一个加1
    public static final int total = 12;
    //id
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    //下单用户
    private int issueid;
    //标题
    private String title;
    //订单内容id集合
    private String orderlist;
    //联系人
    private String buyname;
    //联系人电话
    private String buyphone;
    //联系人地址
    private String buyaddress;
    //价格
    private double price;
    //订单状态
    private int state;
    //帖子创建时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp createTime;
    //帖子修改时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp updateTime;
    //帖子删除时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp overTime;
}
