package com.ft.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ft.domain.NongsukeOrder;
import com.ft.domain.TzmarketOrder;

import java.util.List;

/**
 * @Author: ft
 * @Date: 2023/02/08/10:14
 * @Description:
 */
public interface ITzmarketOrderService extends IService<TzmarketOrder> {
    //增加商品
    public int addOrder(TzmarketOrder order) throws IllegalAccessException;
    //查询订单所有数据
    public TzmarketOrder getOrder(Long id);
    /**
     * 查询指定字段的数据用于微信支付
     * @param id 订单id
     * @return 订单对象
     */
    public TzmarketOrder selectOrderToPay(Long id);

    /**
     * 付款后修改订单状态
     * state:修改的状态
     * id:订单id
     * userid:用户id
     * @return 返回受影响的结果
     */
    public int  updataOrder(int state,String id,int userid);
    /**
     * 获取用户订单数据
     * @return
     */
    public List getOrder(int userid);


}
