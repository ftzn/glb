package com.ft.domain;

import lombok.Data;

/**
 * @Author: ft
 * @Date: 2022/10/20/15:11
 * @Description: 用于微信支付上传订单信息和获取openid的实体类
 */
@Data
public class WeiXinPay {
    //订单id
    private String orderid;
    //获取的openid
    private String openid;
    //订单名称
    private String name;
    //订单价格
    private String price;
}
