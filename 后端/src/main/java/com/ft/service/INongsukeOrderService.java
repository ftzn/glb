package com.ft.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ft.domain.Buyorder;
import com.ft.domain.Nongsuke;
import com.ft.domain.NongsukeOrder;

import java.util.List;

/**
 * @Author: ft
 * @Date: 2023/02/08/10:14
 * @Description:
 */
public interface INongsukeOrderService  extends IService<NongsukeOrder> {
    //增加商品
    public int addOrder(NongsukeOrder order) throws IllegalAccessException;
    //根据id查询订单所有数据
    public NongsukeOrder getOrder(Long id);
    /**
     * 付款后修改订单状态
     * state:修改的状态
     * id:订单id
     * userid:用户id
     * @return 返回受影响的结果
     */
    public int  updataOrderState(int state,String id);

    /**
     * 用户退款
     * @param orderid
     * @return
     */
    public int updateOrderRefund(String orderid,int state);

}
