/**
 * @Aurhor 把手给我
 * @Date 2022/9/2 20:32
 * @Version 1.0
 * 随机命名工具类
 **/
package com.ft.utils;

import java.util.Random;

public class UserName {
    public static String getname(){
        String name = "";
        //创建名字数组
        String[] names = {"善良的","美丽的","无聊的","霸气的","高贵的","优雅的","社交牛人",""};
        String[] ming = {"张三","李四","王五","赵六","孙七","周八","吴九","郑十"};
        name += names[new Random().nextInt(6)];
        name += ming[new Random().nextInt(7)];
        return name;
    }
}
