package com.ft.service.impl;

import com.ft.service.IndexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @Author: ft
 * @Date: 2022/11/04/10:27
 * @Description:
 */
@Service
public class IndexServiceImpl implements IndexService {
    @Autowired
    RedisTemplate redisTemplate;
    /**
     * 获取首页优质帮我买订单
     * @return 订单集合
     */
    @Override
    public List getIndexBuyOrder() {
        /**
         * 1.先获取redis中buyorderId中zset大于1的数据
         * 2.随机返回5个数据
         */
        List result = new ArrayList();  //返回结果集合
        Set buyorderId = redisTemplate.opsForZSet ().reverseRangeByScore("buyorderId", 1, 99);
        //先查看数据是否满足返回的个数
        if(buyorderId.size () < 3){
            List ids = new ArrayList<>(buyorderId);
            for(int i = 0;i < ids.size(); i++){
                Map entries = redisTemplate.opsForHash ().entries ("buyorder:" + ids.get (i));
                if((int)entries.get("state") == 1){
                    //对订单号处理成字符串，防止前端精度丢失
                    Object id = entries.get ("id");
                    entries.put ("id",""+id);
                    result.add (entries);
                }
            }
            return result;
        }
        //个数满足的情况
        //要返回的list集合
        List list = new ArrayList<>();
        List ids = new ArrayList<>(buyorderId);
        //随机出订单集合
        int T = 0;
        while (T < 3) {
            int a = new Random ().nextInt (ids.size ());
            if(! list.contains(ids.get(a))) {
                list.add(ids.get(a));
                T++;
            }else {
                continue;
            }
        }
        //获取订单数据返回
        for(int i = 0;i < list.size(); i++){
            Map entries = redisTemplate.opsForHash ().entries ("buyorder:" + list.get (i));
            if((int)entries.get("state") == 1){
                //对订单号处理成字符串，防止前端精度丢失
                Object id = entries.get ("id");
                entries.put ("id",""+id);
                result.add (entries);
            }
        }
        return result;
    }
    /**
     * 获取首页优质帮我送订单
     * @return 订单集合
     */
    @Override
    public List getIndexSendorder() {
        /**
         * 1.先获取redis中buyorderId中zset大于1的数据
         * 2.随机返回5个数据
         */
        List result = new ArrayList();  //返回结果集合
        Set senderId = redisTemplate.opsForZSet ().reverseRangeByScore("sendorderId", 1, 99);
        System.out.println("senderId的长度是："+senderId.size ());
        //先查看数据是否满足返回的个数
        if(senderId.size () < 3){
            List ids = new ArrayList<>(senderId);
            //获取订单数据返回
            for(int i = 0;i < ids.size(); i++){
                System.out.println("sendorder:"+ids.get(i));
                Map entries = redisTemplate.opsForHash ().entries ("sendorder:" + ids.get (i));
                System.out.println("对象是:"+entries.toString());
                if((int)entries.get("state") == 1){
                    //对订单号处理成字符串，防止前端精度丢失
                    Object id = entries.get ("id");
                    entries.put ("id",""+id);
                    result.add (entries);
                }
            }
            System.out.println("sendorder返回的数据是："+result);
            return result;
        }
        //个数满足的情况
        //要返回的list集合
        List list = new ArrayList<>();
        List ids = new ArrayList<>(senderId);
        //随机出订单集合
        int T = 0;
        while (T < 3) {
            int a = new Random ().nextInt (ids.size ());
            if(! list.contains(ids.get(a))) {
                list.add(ids.get(a));
                T++;
            }else {
                continue;
            }
        }
        //获取订单数据返回
        for(int i = 0;i < list.size(); i++){
            System.out.println("大于3的情况sendorder:"+ids.get(i));
            Map entries = redisTemplate.opsForHash ().entries ("sendorder:" + list.get (i));
            System.out.println("对象是:"+entries.toString());
            if((int)entries.get("state") == 1){
                //对订单号处理成字符串，防止前端精度丢失
                Object id = entries.get ("id");
                entries.put ("id",""+id);
                result.add (entries);
            }
        }
        return result;
    }

    @Override
    public List getIndexWorkorder() {

        /**
         * 1.先获取redis中buyorderId中zset大于1的数据
         * 2.随机返回5个数据
         */
        List result = new ArrayList();  //返回结果集合
        Set workorderId = redisTemplate.opsForZSet ().reverseRangeByScore("workorderId", 1, 99);
        //先查看数据是否满足返回的个数
        if(workorderId.size () < 3){
            List ids = new ArrayList<>(workorderId);
            //获取订单数据返回
            for(int i = 0;i < ids.size(); i++){
                Map entries = redisTemplate.opsForHash ().entries ("workorder:" + ids.get (i));
                if((int)entries.get("state") == 1){
                    //对订单号处理成字符串，防止前端精度丢失
                    Object id = entries.get ("id");
                    entries.put ("id",""+id);
                    result.add (entries);
                }
            }
            System.out.println("sendorder返回的数据是："+result);
            return result;
        }
        //个数满足的情况
        //要返回的list集合
        List list = new ArrayList<>();
        List ids = new ArrayList<>(workorderId);
        //随机出订单集合
        int T = 0;
        while (T < 3) {
            int a = new Random ().nextInt (ids.size ());
            if(! list.contains(ids.get(a))) {
                list.add(ids.get(a));
                T++;
            }else {
                continue;
            }
        }
        //获取订单数据返回
        for(int i = 0;i < list.size(); i++){
            Map entries = redisTemplate.opsForHash ().entries ("workorder:" + list.get (i));
            if((int)entries.get("state") == 1){
                //对订单号处理成字符串，防止前端精度丢失
                Object id = entries.get ("id");
                entries.put ("id",""+id);
                result.add (entries);
            }
        }
        return result;
    }

    @Override
    public List getIndexNeed() {

        /**
         * 1.先获取redis中buyorderId中zset大于1的数据
         * 2.随机返回5个数据
         */
        List result = new ArrayList();  //返回结果集合
        Set needId = redisTemplate.opsForZSet ().reverseRangeByScore("needId", 1, 99);
        //先查看数据是否满足返回的个数
        if(needId.size () < 3){
            List ids = new ArrayList<>(needId);
            //获取订单数据返回
            for(int i = 0;i < ids.size(); i++){
                Map entries = redisTemplate.opsForHash ().entries ("need:" + ids.get (i));
                if((int)entries.get("state") == 1){
                    //对订单号处理成字符串，防止前端精度丢失
                    Object id = entries.get ("id");
                    entries.put ("id",""+id);
                    result.add (entries);
                }
            }
            System.out.println("needId："+result);
            return result;
        }
        //个数满足的情况
        //要返回的list集合
        List list = new ArrayList<>();
        List ids = new ArrayList<>(needId);
        //随机出订单集合
        int T = 0;
        while (T < 3) {
            int a = new Random ().nextInt (ids.size ());
            if(! list.contains(ids.get(a))) {
                list.add(ids.get(a));
                T++;
            }else {
                continue;
            }
        }
        //获取订单数据返回
        for(int i = 0;i < list.size(); i++){
            Map entries = redisTemplate.opsForHash ().entries ("need:" + list.get (i));
            if((int)entries.get("state") == 1){
                //对订单号处理成字符串，防止前端精度丢失
                Object id = entries.get ("id");
                entries.put ("id",""+id);
                result.add (entries);
            }
        }
        return result;
    }
}
