package com.ft.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ft.domain.NongsukeOrder;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface NongsukeOrderDao  extends BaseMapper<NongsukeOrder> {
    @Insert("insert into nongsukeordercontent(id,title,goodsid,goodprice,goodtype,buynum," +
            "totalprice,state,issuseuserid,imglist,indeximg,content,belongid) values(#{id},#{title},#{goodsid},#{goodprice},#{goodtype}," +
            "#{buynum},#{totalprice},#{state},#{issuseuserid},#{imglist},#{indeximg},#{content},#{belongid})")
    public int addOrder(NongsukeOrder nongsuke);
    /**
     * 根据订单id查询订单信息
     * @param id 订单id
     * @return 订单对象
     */
    @Select("select * from nongsukeordercontent where id =#{id}")
    public NongsukeOrder selectOrder(Long id);

    /**
     * 查询指定字段的数据用于微信支付
     * @param id 订单id
     * @return 订单对象
     */
    @Select("select id,title,totalprice from nongsukeordercontent where id =#{id}")
    public NongsukeOrder selectOrderToPay(Long id);

    /**
     * 付款后，修改订单状态
     * @param state 要修改成的状态
     * @return 返回受影响的行数
     */
    @Update("update nongsukeordercontent set state = #{state} where id =#{id}")
    public int updataOrderState(int state,String id);
    /**
     * 获取订单的创建时间信息
     * @param id 订单号
     * @return 时间
     */
    @Select ("select create_time from nongsukeordercontent where id =#{id}")
    public String getcreateTime(Long id);

    /**
     * 获取订单的更新时间
     * @param id 订单号
     * @return 时间
     */
    @Select ("select update_time from nongsukeordercontent where id =#{id}")
    public String getupdateTime(Long id);

    /**
     * 获取订单状态
     * @param id
     * @return
     */
    @Select("select state from nongsukeordercontent where id =#{id}")
    public int getOrderState(Long id);
}
