/**
 * @Aurhor 把手给我
 * @Date 2022/8/24 20:36
 * @Version 1.0
 * 自定义的系统异常类
 **/
package com.ft.excepion;

public class SystemException extends RuntimeException{
    private Integer code;

    public SystemException() {
        super();
    }

    public SystemException(Integer code , String message) {
        super(message);
        this.code = code;
    }

    public SystemException(Integer code , String message, Throwable cause) {
        super(message, cause);
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }
}
