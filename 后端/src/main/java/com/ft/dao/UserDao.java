package com.ft.dao;

import com.ft.domain.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ft.domain.UserAddress;
import org.apache.ibatis.annotations.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author com/ft
 * @since 2022-08-24
 */
@Mapper
public interface UserDao extends BaseMapper<User> {

    /**
     * ID登录校验
     * @param id 用户名
     * @param password 密码
     * @return ID字符串
     */
    @Select("select id from user where id = #{id} and password = #{password} and state != 2")
    public String selectByID(String id,String password);

    /**
     * 手机号登录校验
     * @param phone 手机号
     * @param password 密码
     * @return ID字符串
     */
    @Select("select id from user where phone = #{phone} and password = #{password} and state != 2")
    public String selectByPhone(String phone,String password);

    /**
     * 登录校验根据id查询用户
     * @param id 用户id
     * @return 用户对象
     */
    @Select("select * from user where id = #{id}")
    public User checkSelectById(String id);

    /**
     * 根据手机号查询用户
     * @param phone 查询的手机号
     * @return 返回用户对象
     */
    @Select("select * from user where phone = #{phone}")
    public User selectUserByPhone(String phone);

    /**
     * 查询用户名是否存在
     * @param name :查询的用户名
     * @return 用户名
     */
    @Select("select name from user where name = #{name}")
    public String selectByName(String name);

    /**
     * 根据用户id查询用户名
     * @param id 用户id
     * @return 用户名
     */
   @Select("select name from user where id = #{id}")
    public String selectUserByID(String id);

    @Select("select * from user where id = #{id}")
   public User selectUserAllById(int id);
   @Select ("select sex from user where id = #{id}")
    public int getUserSex(int id);
    /**
     * 添加用户
     * @param user 添加的用户对象
     * @return 受影响的行数 1成功，0失败
     */
    @Insert("insert into user(id,name,password,phone,img) values(#{id},#{name},#{password},#{phone},#{img})")
    public int addUser(User user);

    /**
     *修改用户-管理员
     * @param user 传入修改的用户对象
     * @return 返回受影响的行数
     */
    @Update("update user set name=#{name},password=#{password},sex=#{sex},state=#{state},role=#{role},phone=#{phone},address=#{address} where id=#{id}")
    public int updateUser(User user);

    /**
     * 修改用户-个人
     * @param user
     * @return
     */
    @Update("update user set name=#{name},sex=#{sex},img=#{img},phone=#{phone} where id=#{id}")
    public int updateUserInfo(User user);
    /**
     * 验证码功能获取手机是否存在
     * @param phone
     * @return
     */
    @Select("select phone from user where phone=#{phone}")
    public String selectRedisByPhone(String phone);
    /**
     * 查询所有用户
     * @return 返回所有用户集合
     */
    @Select("select * from user where state != 2")
    public List<User> selectAll();

    /**
     * 查询已经逻辑删除的用户
     * @return 返回查询的list集合
     */
    @Select("select * from user where state = 2")
    public List<User> selectByState();


    /**
     *根据传入的用户id对用户做逻辑删除
     * @param id 要删除用户的id
     * @return 返回受影响的结果
     */
    @Update("update user set state=2 where id=#{id}")
    public int deleteUser(int id);

    /**
     * 根据传入的List集合里id，动态获取用户图片
     *
     */
    @Select("<script>" +
            "select name,img from user where id in" +
            "<foreach collection='list' open='(' close=')' separator=',' item='id'>" +
                "#{id}" +
            "</foreach>" +
            "</script>"
    )
    public List<User> getUserImgList(List list);

    /**
     * 根据id查询地址信息
     * @param id 用户id
     * @return
     */
    @Select("select address from user where id = #{id}")
    public String selectAddress(int id);

    /**
     * 增加修改用户地址信息
     * @param address 修改的地址信息
     * @param id 用户id
     * @return
     */
    @Update("update user set address=#{address} where id = #{id}")
    public int updateAddress(String address,int id);

    @Update("update user set password=#{password} where phone = #{phone}")
    public int resetPassword(String phone,String password);

    /**
     * 修改用户金额
     * @param money
     * @param uderid
     * @return
     */
    @Update("update user set money = #{money} where id = #{uderid}")
    public int updateUserMoney(BigDecimal money, int uderid);

    /**
     * 获取用户钱
     * @param userid
     * @return
     */
    @Select("select money from user where id = #{id}")
    public double getUserMoney(int userid);
}
