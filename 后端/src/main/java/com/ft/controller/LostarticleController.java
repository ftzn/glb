package com.ft.controller;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.ft.domain.Buyorder;
import com.ft.domain.Lostarticle;
import com.ft.service.IBuyorderService;
import com.ft.service.ILostarticleService;
import com.ft.utils.Code;
import com.ft.utils.Result;
import com.ft.utils.SnowFlakeGenerateIdWorker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author com/ft
 * @since 2022-08-24
 */
@RestController
@RequestMapping("/lostarticle")
@CrossOrigin
public class LostarticleController {
    @Autowired
    private ILostarticleService lostarticleService;
    /**
     * 创建订单
     * @param lostarticle 订单对象
     * @return 创建的订单对象
     * @throws JsonProcessingException
     */
    @PostMapping()
    public Result creatrLostarticle(@RequestBody Lostarticle lostarticle) throws JsonProcessingException, IllegalAccessException {
        //创建订单编号
        SnowFlakeGenerateIdWorker snowFlakeGenerateIdWorker =
                new SnowFlakeGenerateIdWorker(0L,0L);
        String id = snowFlakeGenerateIdWorker.generateNextId();
        //设置订单编号
        lostarticle.setId(Long.valueOf(id));
        //返回创建结果
        int result = lostarticleService.createLostarticle (lostarticle);
        if (result == 1){
            Lostarticle bod = lostarticleService.selectLostarticle (Long.valueOf(id));
            System.out.println("返回的数据是："+bod);
            return new Result(Code.POST_OK,bod,"创建订单成功");
        }else {
            System.out.println("添加失败");
            return new Result(Code.POST_ERR,"创建订单失败");
        }
    }
    /**
     * 修改订单状态为已付款
     * @param orderid 订单id
     * @return
     */
    @PutMapping("/{orderid}/{userid}")
    public Result updataOrder(@PathVariable String orderid,@PathVariable int userid){
        int i = lostarticleService.updataOrder (1,orderid,userid);
        System.out.println("修改的结果是："+i);
        if(i == 1){
            return new Result(Code.PUT_OK,"修改成功");
        }else {
            return new Result(Code.PUT_ERR,"修改失败");
        }
    }

    /**
     * 失物招领分页，根据类型获取订单数据
     * @param page
     * @return
     */
    @GetMapping("/{type}/{page}")
    public Result getOrderAll(@PathVariable int type,@PathVariable int page){
        System.out.println ("接收的数据是：type:"+type+"page:"+page);
        List order = lostarticleService.getOrder (type,page);
        return new Result(Code.GET_OK,order,"获取成功");
    }

    /**
     * 失物招领搜索内容
     * @param content
     * @return
     */
    @GetMapping("/search/{content}/{type}/{page}")
    public Result getOrderBySearch(@PathVariable String content,@PathVariable int type, @PathVariable int page){
        List search = lostarticleService.search (content,type,page);
        return new Result(Code.GET_OK,search,"获取成功");
    }
}

