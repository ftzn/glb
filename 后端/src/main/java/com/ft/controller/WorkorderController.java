package com.ft.controller;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.ft.domain.Sendorder;
import com.ft.domain.Workorder;
import com.ft.service.ISendorderService;
import com.ft.service.IWorkorderService;
import com.ft.utils.Code;
import com.ft.utils.Result;
import com.ft.utils.SnowFlakeGenerateIdWorker;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author com/ft
 * @since 2022-08-24
 */
@RestController
@RequestMapping("/workorder")
public class WorkorderController {
    @Autowired
    private IWorkorderService workorderService;

    /**
     * 创建订单
     *
     * @param workorder 订单对象
     * @return 创建的订单对象
     * @throws JsonProcessingException
     */
    @PostMapping()
    public Result creatrBuyOrder(@RequestBody Workorder workorder) throws JsonProcessingException, IllegalAccessException {
        //创建订单编号
        SnowFlakeGenerateIdWorker snowFlakeGenerateIdWorker =
                new SnowFlakeGenerateIdWorker (0L, 0L);
        String id = snowFlakeGenerateIdWorker.generateNextId ();
        //设置订单编号
        workorder.setId (Long.valueOf (id));
        //返回创建结果
        int result = workorderService.createWorkorder(workorder);
        if (result == 1) {
            Workorder workorder1 = workorderService.selectWorkorder(Long.valueOf (id));
            return new Result (Code.POST_OK, workorder1, "创建订单成功");
        } else {
            return new Result (Code.POST_ERR, "创建订单失败");
        }
    }
    /**
     * 修改订单状态为已付款
     * @param orderid 订单id
     * @return
     */
    @PutMapping("/{orderid}/{userid}")
    public Result updataOrder(@PathVariable String orderid,@PathVariable int userid){
        int i = workorderService.updataOrder (1,orderid,userid);
        System.out.println("修改的结果是："+i);
        if(i == 1){
            return new Result(Code.PUT_OK,"修改成功");
        }else {
            return new Result(Code.PUT_ERR,"修改失败");
        }
    }
    /**
     * 懒人中心分页获取订单数据
     * @param page
     * @return
     */
    @GetMapping("/{page}")
    public Result getOrderAll(@PathVariable int page){
        List order = workorderService.getOrder (page);
        return new Result(Code.GET_OK,order,"获取成功");
    }
    /**
     * 懒人中心搜索内容
     * @param content
     * @return
     */
    @GetMapping("/search/{content}")
    public Result getOrderBySearch(@PathVariable String content){
        List search = workorderService.search(content);
        return new Result(Code.GET_OK,search,"获取成功");
    }


    /**
     * 获取用户已发布订单
     * @param userid
     * @return
     */
    @GetMapping("/getuserissue/{userid}")
    public Result getIssueOrderListByUser(@PathVariable String userid){
        List issueOrderListByUser = workorderService.getIssueOrderListByUser(userid);
        return new Result(Code.GET_OK,issueOrderListByUser,"获取成功");
    }

    /**
     * 用户接单
     * @param orderid
     * @param userid
     * @return
     */
    @PutMapping("/userorder/{orderid}/{userid}")
    public Result UserOrder(@PathVariable String orderid,@PathVariable int userid) throws IOException, IllegalAccessException {
        int i = workorderService.UserOrder(orderid, userid);
        if(i == 1){
            return new Result(Code.PUT_OK,1,"接单成功");
        }else if(i == 2){
            return new Result(Code.PUT_ERR,2,"已有用户接单");
        }
        return new Result(Code.PUT_ERR,0,"接单失败");
    }

    /**
     * 用户取消订单
     * @param orderid
     * @param userid
     * @return
     */
    @PutMapping("/cancelorder/{orderid}/{userid}")
    public Result OverUserOrder(@PathVariable String orderid,@PathVariable int userid) throws IOException, IllegalAccessException {
        int i = workorderService.OverUserOrder(orderid, userid);
        if(i == 1){
            return new Result(Code.PUT_OK,"取消订单成功");
        }else if(i == 2){
            return new Result(Code.PUT_ERR,"取消订单失败,订单必须为接单状态才能取消");
        }
        return new Result(Code.PUT_ERR,"取消订单失败");
    }

    /**
     * 用户完成订单
     * @param orderid
     * @return
     */
    @PutMapping("/accomplishOrder/{orderid}")
    public Result accomplishOrder(@PathVariable String orderid) throws IOException, IllegalAccessException {
        int i = workorderService.accomplishOrder(orderid);
        if(i == 1){
            return new Result(Code.PUT_OK,"完成订单成功");
        }else if(i == 2){
            return new Result(Code.PUT_ERR,"订单必须是已接单状态喔");
        }
        return new Result(Code.PUT_ERR,"完成订单失败");
    }


    /**
     * 发布用户完成订单
     * @param orderid
     * @param userid
     * @return
     */
    @PutMapping("/issueoverorder/{orderid}/{userid}")
    public Result issueOverOrder(@PathVariable String orderid,@PathVariable int userid) throws IOException, IllegalAccessException {
        int i = workorderService.issueOverOrder(orderid, userid);
        if(i == 1){
            return new Result(Code.PUT_OK,"订单已完成");
        }else if(i == 2){
            return new Result(Code.PUT_ERR,"接单用户必须确认订单完成了喔");
        }
        return new Result(Code.PUT_ERR,"订单完成失败");
    }
    /**
     * 获取用户已接单订单
     * @param userid
     * @return
     */
    @GetMapping("/getissueList/{userid}")
    public Result getOrderListByUser(@PathVariable String userid){
        List orderListByUser = workorderService.getOrderListByUser(userid);
        return new Result(Code.GET_OK,orderListByUser,"获取成功");
    }
}

