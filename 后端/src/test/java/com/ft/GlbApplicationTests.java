package com.ft;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ft.config.properties.WeiXinLoginProperties;
import com.ft.config.properties.WeiXinPayProperties;
import com.ft.dao.StudyAnswerDao;
import com.ft.dao.StudyDao;
import com.ft.dao.UserDao;
import com.ft.domain.StudyAnswer;
import com.ft.domain.User;
import com.ft.domain.UserAddress;

import com.ft.utils.MD5Utils;
import com.ft.utils.SnowFlakeGenerateIdWorker;
import com.ft.utils.WxPayUtils;
import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.TrustStrategy;
import org.apache.http.util.EntityUtils;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

import javax.net.ssl.SSLContext;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.sql.Array;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class GlbApplicationTests {
    @Autowired
    RedisTemplate redisTemplate;
    @Autowired
    UserDao userdao;
    @Autowired
    WeiXinPayProperties weiXinPay;
    @Autowired
    WeiXinLoginProperties wxlogin;
    @Autowired
    StudyAnswerDao studyAnswerDao;

    public static void main(String[] args) {

    }

    @Test
    void testMybatis() throws ParseException {
        String orderId = "";
        //随机字符串的随机字符库
        String keyString = "abcdefghijkmnopqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ0123456789";
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < 12; i++) {
            sb.append(keyString.charAt(random.nextInt(keyString.length())));
        }
        System.out.println("结果是:"+sb.toString());
    }
//    @Test
//    void testArrayUtils(){
//        String str = "{\"address\":\"1\",\"id\":10001,\"name\":\"长度\",\"phone\":\"15668988789\"}";
//        UserAddress ud =  JSON.parseObject(str, UserAddress.class);
//        System.out.println( ud instanceof UserAddress);
//    }
//    @Test
//    //秘钥生成工具
//    void testMd5Utils(){
//        String str = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
//        String st = "";
//        System.out.println("长度是："+str.length());
//        for (int i = 0; i < 32; i++) {
//          int a = (int) (Math.random() * str.length());
//          System.out.println("a的值是："+a);
//          st += str.charAt(a);
//        }
//        System.out.println("结果是："+st.length()+"|"+st);
//    }
//    @Test
//    void httpclientTest(){
//        /**
//         * 使用httpclient发送get请求
//         */
//        //1.通过HttpClients创建CloseableHttpClient对象，它相当于就是打开一个浏览器
//        CloseableHttpClient aDefault = HttpClients.createDefault();
//        //2.创建请求地址
//        int id = 1;
//        String url = "http://124.220.191.116:8081/users/login";
//        //3.创建发送get方法的HttpGet对象
//        HttpPost request = new HttpPost(url);
//        //创建代理
//        String ip = "182.90.224.115";
//        int port = 3128;
//        HttpHost httpHost = new HttpHost(ip,port);
//        //使用NameValuePair对象给POST对象设置参数，NameValuePair就相当于是input标签，和它输入的值合成的这个对象
//        List<NameValuePair> params = new ArrayList<>();
//        params.add(new BasicNameValuePair("name","ft"));
//        params.add(new BasicNameValuePair("password","ft099437"));
//        //把参数设置到FormEntity里
//        UrlEncodedFormEntity FormEntity = new UrlEncodedFormEntity(params, Consts.UTF_8);
//        //添加到请求里
//        request.setEntity(FormEntity);
//        //对请求进行设置
//        RequestConfig.Builder customReqConf = RequestConfig.custom();
//        //设置链接超时时间
//        customReqConf.setConnectTimeout(5000);
//        //读取超时，表示从请求的网址出获取数据超时的时间
//        customReqConf.setSocketTimeout(3000);
//        request.setConfig(customReqConf.build());
//        //设置请求头
//        request.setHeader("user-agent","Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36");
//        //4.创建响应对象
//        CloseableHttpResponse response = null ;
//        try {
//            //执行
//            response = aDefault.execute(request);
//            //5.获取响应结果
//            HttpEntity  ey = response.getEntity();
//            //6.通过自带的EntityUtils工具类对ey处理
//            String s = EntityUtils.toString(ey,"utf-8");
//            System.out.println("请求返回结果是:"+s);
//            //确保流关闭
//            EntityUtils.consume(ey);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }finally {
//            //关闭资源
//            if (aDefault != null) {
//                try {
//                    aDefault.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//            if(response != null) {
//                try {
//                    response.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//    }
//    @Test
//    void httpclientTest1() throws Exception {
//        /**
//         * 使用httpclient发送post/json请求
//         */
//        //1.通过定制创建CloseableHttpClient对象，它相当于就是打开一个浏览器
//        Registry<ConnectionSocketFactory> rer = RegistryBuilder.<ConnectionSocketFactory>create()
//        .register("http", PlainConnectionSocketFactory.INSTANCE)
//        //通过自定义方法构建
//        .register("https",trustHttpCertificates())
//        .build();
//        //创建一个ConnectionManager对象
//        PoolingHttpClientConnectionManager pool = new PoolingHttpClientConnectionManager(rer);
//        //定制closeableHttpClient对象
//        HttpClientBuilder httpClientBuilder = HttpClients.custom().setConnectionManager(pool);
//        //配置好Httpclient之后，通过build()方法来创建HttpClient对象
//        CloseableHttpClient aDefault = httpClientBuilder.build();
//        //2.创建请求地址
//        String url = "http://baidu.com";
//        //3.创建发送post方法的HttpPost对象
//        HttpGet request = new HttpGet(url);
//        //对请求进行设置
//        RequestConfig.Builder customReqConf = RequestConfig.custom();
//        //设置链接超时时间
//        customReqConf.setConnectTimeout(5000);
//        //读取超时，表示从请求的网址出获取数据超时的时间
//        customReqConf.setSocketTimeout(3000);
//        request.setConfig(customReqConf.build());
//        //设置请求头
//        request.setHeader("user-agent","Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36");
//        //4.创建响应对象
//        CloseableHttpResponse response = null ;
//        try {
//            //执行
//            response = aDefault.execute(request);
//            //5.获取响应结果
//            HttpEntity  ey = response.getEntity();
//            //6.通过自带的EntityUtils工具类对ey处理
//            String s = EntityUtils.toString(ey,"utf-8");
//            System.out.println("请求返回结果是:"+s);
//            //确保流关闭
//            EntityUtils.consume(ey);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }finally {
//            //关闭资源
//            if (aDefault != null) {
//                try {
//                    aDefault.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//            if(response != null) {
//                try {
//                    response.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//    }
//    @Test
//    /**
//     * 测试xml
//     */
//    void httpclientTest2() throws Exception {
//        Map<String,String> js = new HashMap<String,String>();
//        String ip = "127.0.0.1";
//        js.put("appid", weiXinPay.getAppid());
//        js.put("mch_id", weiXinPay.getMchid());
//        js.put("nonce_str", "5K8264ILTKCH16CQ2502SI8ZNMTM67VS");
//        js.put("body", "测试支付");
//        js.put("out_trade_no", "20150806125346");
//        js.put("total_fee", "88");
//        js.put("spbill_create_ip", ip);
//        js.put("notify_url", "https://www.baidu.com");
//        js.put("trade_type", weiXinPay.getType());
////        js.put("openid",openid);
//        String postStr = WxPayUtils.Md5Sgin(js,weiXinPay.getAppsecret());
//        System.out.println("加密后的数据是："+postStr);
//    }
//    @Test
//    void test3(){
//        String fileUrl = "https://ft-glb.oss-cn-chengdu.aliyuncs.com/1665986813571.png";
//        String imgFile = fileUrl.replace("https://ft-glb.oss-cn-chengdu.aliyuncs.com/", "");
//        System.out.println("imgFile=:"+ imgFile);
//    }
//    private ConnectionSocketFactory trustHttpCertificates() throws Exception {
//        //创建SSLContext对象
//        SSLContextBuilder sslContextBuilder = new SSLContextBuilder();
//        //加载下信任的证书
//        sslContextBuilder.loadTrustMaterial(null, new TrustStrategy() {
//            //判断是否信任url，直接返回true信任
//            @Override
//            public boolean isTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
//                return true;
//            }
//        });
//        //设置证书类型
//        SSLContext sslContext = sslContextBuilder.build();
//        //第一个参数数ssl对象，第二个是协议数组
//        SSLConnectionSocketFactory sslfy = new SSLConnectionSocketFactory(sslContext,
//                new String[]{"SSLv2Hello","SSLv3","TLSv1","TLSv1.1","TLSv1.2"}
//                ,null, NoopHostnameVerifier.INSTANCE);
//        return sslfy;
//    }
}
