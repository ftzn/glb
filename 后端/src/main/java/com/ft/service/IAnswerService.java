package com.ft.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ft.domain.Answer;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Author: ft
 * @Date: 2023/01/12/12:29
 * @Description:
 */
public interface IAnswerService extends IService<Answer> {
    @Transactional
    //增加回复
    public int addAnswer(Answer answer,String num) throws IllegalAccessException;
    @Transactional
    //根据评论id获取回复
    public List<Answer> getfistcommentsByPostId(String postId);
    //获取创建时间
    public String getCreateTime(Long id);
    @Transactional
    //删除单个回复记录
    public int deleteAnswer(Long id);
    @Transactional
    //根据评论id删除多个回复记录
    public int deleteArray(String[] array,Long belongFistcomment);
}
