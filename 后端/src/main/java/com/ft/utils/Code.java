/**
 * @Aurhor ft
 * @Date 2022/8/24 18:23
 * @Version 1.0
 **/
package com.ft.utils;

/**
 * 状态码：大于1结尾的就是成功，以零结尾的就是失败
 */
public class Code {
    public static final Integer POST_OK = 20011;
    public static final Integer GET_OK = 20021;
    public static final Integer GET_BYID_OK = 20022;
    public static final Integer DELETE_OK = 20031;
    public static final Integer PUT_OK = 20041;

    public static final Integer POST_ERR = 20010;
    public static final Integer GET_ERR = 20020;
    public static final Integer GET_BYID_ERR = 20022;
    public static final Integer DELETE_ERR = 20030;
    public static final Integer PUT_ERR = 20040;
    //异常
    public static final Integer SYSTEM_ERR = 7777;//系统异常
    public static final Integer BUSINESS_ERR = 8888;//业务异常
    public static final Integer OTHER_ERR = 9999;//其他异常
}
