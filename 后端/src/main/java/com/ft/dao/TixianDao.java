package com.ft.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ft.domain.Tixian;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface TixianDao extends BaseMapper<Tixian> {

    @Insert("insert into tixian(id,tixianuser,money) values(#{id},#{tixianuser},#{money})")
    public int addtixian(Tixian tixian);

    @Select("select * from tixian where tixianuser = #{userid}")
    public List selectTixianByuser(int userid);
}
