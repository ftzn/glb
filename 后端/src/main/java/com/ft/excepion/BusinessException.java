/**
 * @Aurhor 把手给我
 * @Date 2022/8/24 20:37
 * @Version 1.0
 * 业务异常类
 **/
package com.ft.excepion;

public class BusinessException extends RuntimeException {
    private Integer code;

    public BusinessException() {
        super();
    }

    public BusinessException(Integer code , String message) {
        super(message);
        this.code = code;
    }

    public BusinessException(Integer code , String message, Throwable cause) {
        super(message, cause);
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }
}
