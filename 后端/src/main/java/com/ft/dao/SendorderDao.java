package com.ft.dao;

import com.ft.domain.Buyorder;
import com.ft.domain.Sendorder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author com/ft
 * @since 2022-08-24
 */
@Mapper
public interface SendorderDao extends BaseMapper<Sendorder> {
    /**
     * 创建订单
     * @param order 添加的订单对象
     * @return 受影响的行数
     */
    @Insert("insert into sendorder(id,type,state,title,price,content,order_img,remarks,contacts,contacts_phone,contacts_address," +
            "issue_user,issue_user_name,issue_user_img,pay_type,hide_name,ordertypeid) values(#{id},#{type},#{state},#{title},#{price},#{content},#{orderImg}," +
            "#{remarks},#{contacts},#{contactsPhone},#{contactsAddress},#{issueUser},#{issueUserName},#{issueUserImg},#{payType},#{hideName},#{ordertypeid})")
    public int createSendOrder(Sendorder order);

    /**
     * 根据订单id查询订单信息
     * @param id 订单id
     * @return 订单对象
     */
    @Select("select * from sendorder where id =#{id}")
    public Sendorder selectSendOrder(Long id);

    /**
     * 查询指定字段的数据用于微信支付
     * @param id 订单id
     * @return 订单对象
     */
    @Select("select id,title,price from sendorder where id =#{id}")
    public Sendorder selectSendOrderToPay(Long id);

    /**
     * 付款后，修改订单状态
     * @param state 要修改成的状态
     * @return 返回受影响的行数
     */
    @Update ("update sendorder set state = #{state} where id =#{id}")
    public int updataOrder(int state,String id);
    /**
     * 获取订单的创建时间信息
     * @param id 订单号
     * @return 时间
     */
    @Select ("select create_time from sendorder where id =#{id}")
    public String getcreateTime(Long id);

    /**
     * 获取订单的更新时间
     * @param id 订单号
     * @return 时间
     */
    @Select ("select update_time from sendorder where id =#{id}")
    public String getupdateTime(Long id);


    /**
     * 修改订单状态为接单状态
     * @param orderid
     * @param userid
     * @return
     */
    @Update("update sendorder set state = 2,belong_user = #{userid} where id =#{orderid}")
    public int UserOrder(String orderid,int userid);

    /**
     * 用户取消订单
     * @param orderid
     * @return
     */
    @Update("update sendorder set state = 1,belong_user = null where id =#{orderid}")
    public int OverUserOrder(String orderid);

    /**
     * 接单用户完成订单
     * @param orderid
     * @return
     */
    @Update("update sendorder set state = 3 where id =#{orderid}")
    public int accomplishOrder(String orderid);

    /**
     * 发布用户完成订单
     * @param orderid
     * @return
     */
    @Update("update sendorder set state = 4 where id =#{orderid}")
    public int issueaccomplishOrder(String orderid);

    /**
     * 查询订单状态
     * @param orderid
     * @return
     */
    @Select("select state from sendorder where id =#{orderid}")
    public int selectState(String orderid);

    /**
     * 退款后修改订单状态
     * @param orderid
     * @return
     */
    @Update("update sendorder set state = 5 where id =#{orderid}")
    public int refundState(String orderid);
}
