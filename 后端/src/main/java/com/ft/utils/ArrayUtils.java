/**
 * @Aurhor 把手给我
 * @Date 2022/9/27 15:59
 * @Version 1.0
 * 将字符串转换成数组
 **/
package com.ft.utils;

import com.alibaba.fastjson.JSON;
import com.ft.domain.UserAddress;

import java.util.ArrayList;

public class ArrayUtils {
        //将字符串转成List集合
    public static ArrayList<UserAddress> atoString(String str) {
        ArrayList<UserAddress> arr = new ArrayList();
        String st = "";
        boolean T = false;//添加元素的标记
        for (int i = 0; i < str.length(); i++) {
            //是 { 的情况
            if (str.charAt(i) == 123) {
                T = true;
                st += str.charAt(i);
                continue;
            }
            //是 } 的情况
            if (str.charAt(i) == 125) {
                st += str.charAt(i);
                arr.add(JSON.parseObject(st,UserAddress.class));
                st = "";
                T = false;
                continue;
            }
            if (T) {
                st += str.charAt(i);
            }
        }
        return arr;
    }
}
