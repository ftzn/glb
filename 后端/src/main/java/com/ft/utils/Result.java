/**
 * @Aurhor ft
 * @Date 2022/8/24 18:19
 * @Version 1.0
 **/
package com.ft.utils;

public class Result {
    /**
     * date:返回的数据
     * code：返回的状态码
     * msg：返回的信息
     */
    private Object data;
    private Integer code;
    private String msg;

    public Result(){}
    public Result(Integer code, Object data){
        this.code = code;
        this.data = data;
    }
    public Result(Integer code, String msg){
        this.code = code;
        this.msg = msg;
    }
    public Result(Integer code, Object data, String msg) {
        this.data = data;
        this.code = code;
        this.msg = msg;
    }

    public Object getDate() {
        return data;
    }

    public void setDate(Object data) {
        this.data = data;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
