package com.ft.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ft.domain.SystemMessage;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ISystemService  extends IService<SystemMessage> {
    /**
     * 增加公告
     * @param message
     * @return
     */
    @Transactional
    public int addSystemMessage(SystemMessage message) throws IllegalAccessException;

    /**
     * 修改公告
     * @param message
     * @return
     */
    @Transactional
    public int updateSystemMessage(SystemMessage message) throws IllegalAccessException;

    /**
     * 删除公告
     * @param state
     * @param id
     * @return
     */
    @Transactional
    public int deleteSystemMessage(int state,Long id);

    /**
     * 查询所有公告
     * @return
     */
    @Transactional
    public List<SystemMessage> getSystemMessagesList();
}
