package com.ft.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.ft.domain.Lostarticle;
import com.ft.domain.Tzmarket;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author com/ft
 * @since 2022-08-24
 */
@Transactional
public interface ITzmarketService extends IService<Tzmarket> {
    /**
     * 创建跳蚤市场订单
     * @param order 跳蚤市场订单对象
     * @return 受影响的行数
     */
    public int createTzmarket(Tzmarket order) throws JsonProcessingException, IllegalAccessException;

    /**
     * 根据订单id查询所有订单信息
     * @param id 订单id
     * @return 订单对象
     */
    public Tzmarket selectTzmarket(Long id);
    /**
     * 查询指定字段的数据用于微信支付
     * @param id 订单id
     * @return 订单对象
     */
    public Tzmarket selectTzmarketToPay(Long id);

    /**
     * 付款后修改订单状态
     * state:修改的状态
     * id:订单id
     * belongUser:购买用户id
     * @return 返回受影响的结果
     */
    public int  updataOrder(int state,String id,int belongUser) throws IOException, IllegalAccessException;
    /**
     * 跳蚤市场分页获取订单数据
     * @param page
     * @return
     */
    public List getOrder(int type,int page);

    /**
     * 跳蚤市场领搜索内容
     * @param content 搜索的数据
     * @return 订单集合
     */
    public List search(String content,int type,int page);


    /**
     * 发布用户取消订单
     * @param
     * @return
     */
    public int updateOrderRefund(String orderid);

    /**
     * 购买用户退款
     * @param orderid
     * @return
     */
    @Transactional
    public int buyUserRefund(String orderid) throws IOException, IllegalAccessException;

    /**
     * 获取用户发布订单
     * @param userid
     * @return
     */
    public List getUserissueTzmarket(int userid);

    /**
     * 获取用户购买订单
     * @param userid
     * @return
     */
    public List getUserBuyorders(int userid);

    /**
     * 用户确认完成订单
     * @param orderid
     * @param userid
     * @return
     * @throws IOException
     * @throws IllegalAccessException
     */
    public int issueOverOrder(String orderid, int userid) throws IOException, IllegalAccessException;

    /**
     * 获取订单状态
     * @param orderid
     * @return
     */
    public int getState(String orderid);
}
