package com.ft.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ft.domain.SystemMessage;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import javax.sound.midi.SysexMessage;
import java.util.List;

@Mapper
public interface SystemMessageDao extends BaseMapper<SystemMessage> {
    /**
     * 创建系统公告
     * @param message
     * @return
     */
    @Insert("insert into systemmessage(id,title,content,imglist,issue_user_id,issue_user_name,issue_user_img,state)" +
            "values(#{id},#{title},#{content},#{imglist},#{issueUserId},#{issueUserName},#{issueUserImg},#{state})")
    public int addSystemMessage(SystemMessage message);

    @Update("update systemmessage set title = #{title},content = #{content}, imglist = #{imglist} where id = #{id}")
    public int updateSystemMessage(SystemMessage message);

    @Update("update systemmessage set state = #{state} where id = #{id}")
    public int deleteSystemMessage(int state,Long id);

    @Select("select * from systemmessage ")
    public List<SystemMessage> getSystemMessagesList();

    /**
     * 获取帖子的更新时间
     * @param id 公告id
     * @return 时间
     */
    @Select ("select update_time from systemmessage where id =#{id}")
    public String getcreateTime(Long id);
}
