/**
 * @Aurhor 把手给我
 * @Date 2022/9/21 8:44
 * @Version 1.0
 **/
package com.ft.controller;

import com.ft.config.properties.AliyunOssProperties;
import com.ft.dao.UserDao;
import com.ft.domain.aliyun;
import com.ft.utils.Code;
import com.ft.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author com/ft
 * @since 2022-09-21
 */
@RestController
@RequestMapping("/alyapi")
@CrossOrigin
public class aliyunController {
    @Autowired
    UserDao userDao;
    @Autowired
    AliyunOssProperties alyOss;

    @GetMapping("/{id}")
    public Result getAliyunKey(@PathVariable int id) {
        String s = userDao.selectUserByID (String.valueOf (id));
        if (s == null || s.length () == 0) {
            return new Result (Code.GET_ERR, "用户不存在，获取失败");
        } else {
            return new Result (Code.GET_OK, "获取key成功");
        }
    }

    /**
     * 上传图片
     *
     * @param aly 封装的aly对象
     * @return
     * @throws Exception
     */
    @PostMapping("/uploadimg")
    public Result uploadImg(@RequestBody aliyun aly) throws Exception {
        String url = alyOss.uploadOneFile (aly.getBase64 (), aly.getName ());
        if (!"".equals (url) & url.length () > 0) {
            return new Result (Code.POST_OK, url, "成功");
        } else {
            return new Result (Code.POST_ERR, "上传图片失败！");
        }
    }

    /**
     * @param delepath 删除的文件路径，或者链接
     * @return
     */
    @DeleteMapping("")
    public Result deleteImg(@RequestParam String delepath) {
        boolean b = alyOss.deleteImages (delepath);
        System.out.println ("b:" + b);
        if (!b) {
            return new Result (Code.DELETE_OK, "删除成功！");
        } else {
            return new Result (Code.DELETE_ERR, "删除失败！");
        }
    }

}
