package com.ft.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ft.domain.Lostarticle;
import com.ft.domain.Seekarticle;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author com/ft
 * @since 2022-08-24
 */
@Mapper
public interface SeekarticleDao extends BaseMapper<Seekarticle> {
    /**
     * 创建订单
     * @param order 添加的订单对象
     * @return 受影响的行数
     */
    @Insert("insert into seekarticle(id,type,state,title,price,content,order_img,index_img,remarks,weizhi,paid," +
            "issue_user,issue_user_name,issue_user_img,pay_type,hide_name,ordertypeid) values(#{id},#{type},#{state},#{title},#{price},#{content},#{orderImg}," +
            "#{indexImg},#{remarks},#{weizhi},#{paid},#{issueUser},#{issueUserName},#{issueUserImg},#{payType},#{hideName},#{ordertypeid})")
    public int createSeekarticle(Seekarticle order);

    /**
     * 根据订单id查询订单信息
     * @param id 订单id
     * @return 订单对象
     */
    @Select("select * from seekarticle where id =#{id}")
    public Seekarticle selectSeekarticle(Long id);

    /**
     * 查询指定字段的数据用于微信支付
     * @param id 订单id
     * @return 订单对象
     */
    @Select("select id,title,price from seekarticle where id =#{id}")
    public Seekarticle selectSeekarticleToPay(Long id);

    /**
     * 付款后，修改订单状态
     * @param state 要修改成的状态
     * @return 返回受影响的行数
     */
    @Update ("update seekarticle set state = #{state} where id =#{id}")
    public int updataOrder(int state,String id);
    /**
     * 获取订单的创建时间信息
     * @param id 订单号
     * @return 时间
     */
    @Select ("select create_time from seekarticle where id =#{id}")
    public String getcreateTime(Long id);

    /**
     * 获取订单的更新时间
     * @param id 订单号
     * @return 时间
     */
    @Select ("select update_time from seekarticle where id =#{id}")
    public String getupdateTime(Long id);
}
