package com.ft.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ft.domain.Fistcomment;
import com.ft.domain.StudyFistcomment;
import org.apache.ibatis.annotations.*;

/**
 * @Author: ft
 * @Date: 2023/01/12/11:53
 * @Description:
 */
@Mapper
public interface StudyFistcommentDao extends BaseMapper<StudyFistcomment>{
    /**
     * 创建一级评论
     * @param fist
     * @return
     */
    @Insert ("insert into studyfistcomment(id,content,comment_userid,comment_username,comment_userimg,answer_idlist,belongpost,state)" +
            "values(#{id}, #{content}, #{commentUserid},#{commentUsername},#{commentUserimg},#{answerIdlist},#{belongpost},#{state})")
    public int addFistcomment(StudyFistcomment fist);

    /**
     * 获取评论的创建时间信息
     * @param id 订单号
     * @return 时间
     */
    @Select("select create_time from studyfistcomment where id =#{id}")
    public String getcreateTime(Long id);

    /**
     * 获取评论的更新时间
     * @param id 评论id
     * @return 时间
     */
    @Select ("select update_time from studyfistcomment where id =#{id}")
    public String getupdateTime(Long id);
    /**
     * 更新评论表中的用户id集合
     * @param answerIdlist 回复id集合字符串
     * @param id 增加的回复id
     * @return 受影响的行数
     */
    @Update("update studyfistcomment set answer_idlist = #{answerIdlist} where id = #{id}")
    public int addAnswerIdlist(String answerIdlist,Long id);

    /**
     * 获取用户评论id字段
     * @param id
     * @return
     */
    @Select ("select answer_idlist from studyfistcomment where id = #{id}")
    public String getAnswerIdlist(Long id);

    /**
     * 获取评论所属帖子id
     * @return
     */
    @Select ("select belongpost from studyfistcomment where id = #{id}")
    public Long getStudyId(Long id);

    /**
     * 更加id删除评论
     * @param id
     * @return
     */
    @Delete ("delete from studyfistcomment where id = #{id}")
    public int deleteFistComment(Long id);
    /**
     * 根据数组id删除对应评论数据
     * @param fistcommentidList
     * @return
     */
    @Delete(
            "<script>" +
                    "delete from studyfistcomment where id in" +
                    "<foreach collection='fistcommentidList' open='(' close=')' separator=',' item='id'>" +
                    "#{id}" +
                    "</foreach>" +
             "</script>"
    )
    public int deletefistcommentArray(String[] fistcommentidList);
}
