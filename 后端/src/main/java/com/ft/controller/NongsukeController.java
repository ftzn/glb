package com.ft.controller;

import com.ft.domain.Nongsuke;
import com.ft.domain.Tzmarket;
import com.ft.service.INongsukeService;
import com.ft.utils.Code;
import com.ft.utils.Result;
import com.ft.utils.SnowFlakeGenerateIdWorker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author: ft
 * @Date: 2023/02/09/9:04
 * @Description:
 */
@RestController
@RequestMapping("/nongsuke")
@CrossOrigin
public class NongsukeController {
    @Autowired
    private INongsukeService nongsukeService;
    @Autowired
    private RedisTemplate redisTemplate;

    @PostMapping("")
    public Result addNongsuke(@RequestBody Nongsuke nongsuke) throws IllegalAccessException {
        //创建订单编号
        SnowFlakeGenerateIdWorker snowFlakeGenerateIdWorker =
                new SnowFlakeGenerateIdWorker (0L, 0L);
        String id = snowFlakeGenerateIdWorker.generateNextId ();
        //设置订单编号
        nongsuke.setId (Long.valueOf (id));
        //返回创建结果
        int result = nongsukeService.addNongsuke (nongsuke);
        if (result == 1) {
            Nongsuke ns = nongsukeService.getNongsuke (Long.valueOf (id));
            return new Result (Code.POST_OK, ns, "创建商品成功");
        } else {
            return new Result (Code.POST_ERR, "创建商品失败");
        }
    }
    /**
     * 修改订单状态为已付款
     * @param orderid 订单id
     * @return
     */
    @PutMapping("/{orderid}/{userid}")
    public Result updataOrder(@PathVariable String orderid,@PathVariable int userid){
        int i = nongsukeService.updataOrder (1,orderid,userid);
        System.out.println("修改的结果是："+i);
        if(i == 1){
            return new Result(Code.PUT_OK,"修改成功");
        }else {
            return new Result(Code.PUT_ERR,"修改失败");
        }
    }


    /**
     * 跳蚤市场分页，根据类型获取订单数据
     * @param page
     * @return
     */
    @GetMapping("/{type}/{page}")
    public Result getOrderAll(@PathVariable int type,@PathVariable int page){
        System.out.println ("接收的数据是：type:"+type+"page:"+page);
        List order = nongsukeService.getOrder (type,page);
        System.out.println("跳蚤市场首页返回的数据是"+order);
        return new Result(Code.GET_OK,order,"获取成功");
    }

    /**
     * 跳蚤市场搜索内容
     * @param content
     * @return
     */
    @GetMapping("/search/{content}/{type}/{page}")
    public Result getOrderBySearch(@PathVariable String content,@PathVariable int type, @PathVariable int page){
        System.out.println("接收的数据是："+content+":"+page);
        List search = nongsukeService.search (content,type,page);
        return new Result(Code.GET_OK,search,"获取成功");
    }
    /**
     * 根据id查询订单数据
     * id
     */
    @GetMapping("/getnongsukeone/{id}")
    public Result getNongsukeByIdAndOne(@PathVariable String id){
        Nongsuke nongsukeByIdAndOne = nongsukeService.getNongsukeByIdAndOne(id);
        return new Result(Code.GET_OK,nongsukeByIdAndOne,"查询成功");
    }

    /**
     * 获取用户发布的所有商品信息
     * userid
     */
    @GetMapping("/getuserissuegoods/{userid}")
    public Result getUserIssueOrderList(@PathVariable int userid){
        List userIssueOrderList = nongsukeService.getUserIssueOrderList(userid);
        return new Result(Code.GET_OK,userIssueOrderList,"获取成功");
    }
}

