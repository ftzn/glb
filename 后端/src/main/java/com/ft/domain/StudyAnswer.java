package com.ft.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * @Author: ft
 * @Date: 2023/01/11/15:07
 * @Description:
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("studyanswer")
public class StudyAnswer implements Serializable {
    private static final long serialVersionUID = 1L;
    //所有属性的总和，每增加一个加1
    public static final int total = 11;
    //id
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    //回复内容
    private String answerContent;
    //发表该回复的用户id
    private int answerUserid;
    //创建回复的用户名称
    private String answerUsername;
    //所属上级评论id
    @JsonSerialize(using = ToStringSerializer.class)
    private Long belongtofistcomment;
    //回复的用户id
    private int answerTouserid;
    //回复的用户名称
    private String answerTousername;
    //帖子创建时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp createTime;
    //帖子修改时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp updateTime;
    //帖子删除时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp overTime;
    //回复状态
    private int state;
}
