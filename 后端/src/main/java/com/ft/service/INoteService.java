package com.ft.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ft.domain.Note;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Author: ft
 * @Date: 2023/02/09/21:28
 * @Description:
 */
public interface INoteService  extends IService<Note> {
    //增加纸条
    @Transactional
    public int addNote(Note note) throws IllegalAccessException;

    //删除纸条
    public int deleteNote(Long id);

    //修改纸条
    @Transactional
    public int updateNote(String title, String content, Long id);

    //获取纸条创建时间
    public String getNoteCreate(Long id);

    //获取纸条所有信息
    public Note getNote(Long id);

    //获取用户放入纸条
    public List<Note> getNotes(int page, int userid);

    //获取用户抽取纸条
    public List<Note> getOutNotes(int page, int userId);
    //获取男生女生纸条个数
    public Long getnoteNumber(int T);

    //获取男女生纸条数量
    public Long getNotenumBoy();
    public Long getNotenumGirl();
    //随机抽取一张男生，女生纸条,0女生，1男生
    @Transactional
    public Note getNoteRandom(int T,int userid);
}