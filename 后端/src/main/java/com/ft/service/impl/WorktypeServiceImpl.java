package com.ft.service.impl;

import com.ft.domain.Worktype;
import com.ft.dao.WorktypeDao;
import com.ft.service.IWorktypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author com/ft
 * @since 2022-08-24
 */
@Service
public class WorktypeServiceImpl extends ServiceImpl<WorktypeDao, Worktype> implements IWorktypeService {

}
