/**
 * @Aurhor 把手给我
 * @Date 2022/5/18 20:46
 * @Version 1.0
 **/
package com.ft.controller;
import com.ft.excepion.BusinessException;
import com.ft.excepion.SystemException;
import com.ft.utils.Code;
import com.ft.utils.Result;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice//声明该类是异常处理类
public class ProjectException {
    //处理系统异常
    @ExceptionHandler(BusinessException.class)//该注解表示处理那种异常(业务异常)
    public Result doBusiness(BusinessException ex){
        return new Result(ex.getCode(),null, ex.getMessage());
    }
    //处理业务异常
    @ExceptionHandler(SystemException.class)//该注解表示处理那种异常（系统异常）
    public Result doSystem(SystemException ex){
        return new Result(ex.getCode(),null, ex.getMessage());
    }
    //处理其他异常
    @ExceptionHandler(Exception.class)//该注解表示处理其他异常（比如sql语句编写错误）
    public Result doBusiness(Exception ex){
        System.out.println("出现了其他异常："+ex.getMessage());
        return new Result(Code.OTHER_ERR,null,"系统繁忙请稍后在试！");
    }
}
