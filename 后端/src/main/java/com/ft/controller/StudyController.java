package com.ft.controller;

import com.ft.domain.Post;
import com.ft.domain.Study;
import com.ft.service.IPostService;
import com.ft.service.IStudyService;
import com.ft.utils.Code;
import com.ft.utils.Result;
import com.ft.utils.SnowFlakeGenerateIdWorker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author: ft
 * @Date: 2023/01/11/19:49
 * @Description:
 */
@RestController
@RequestMapping("/study")
@CrossOrigin
public class StudyController {
    @Autowired
    private IStudyService studyService;
    @PostMapping("")
    public Result addpost(@RequestBody Study Study) throws IllegalAccessException {
        //创建帖子编号
        SnowFlakeGenerateIdWorker snowFlakeGenerateIdWorker =
                new SnowFlakeGenerateIdWorker(0L,0L);
        String id = snowFlakeGenerateIdWorker.generateNextId();
        //设置帖子编号
        Study.setId (Long.valueOf (id));
        int i = studyService.addStudy(Study);
        if(i== 1){
            return new Result(Code.POST_OK,Study,"增加帖子成功!");
        }else {
            return new Result(Code.POST_ERR,"创建帖子失败!");
        }
    }

    /**
     * 获取分页数据
     * @return
     */
    @GetMapping("/{type}/{page}")
    public Result getpost(@PathVariable int type,@PathVariable int page){
        List order = studyService.getStudyList(type,page);
        return new Result(Code.GET_OK,order,"获取成功");
    }

    /**
     * 更新点赞数据
     * @param praise
     * @param number
     * @param id
     * @return
     */
    @PutMapping ("/praise/{praise}/{number}/{id}")
    public Result updatePraise(@PathVariable String praise ,@PathVariable int number,@PathVariable Long id){
        //点赞数据为空的情况
        if(praise.equals ("aaa")){
            praise = "";
        }
        int i = studyService.addPraiseAndNumber (praise, number, id);
        if(i == 1){
            return new Result(Code.PUT_OK,"点赞成功!");
        }
        return new Result(Code.PUT_ERR,"点赞失败");
    }
    /**
     * 帖子搜索内容
     * @param content
     * @return
     */
    @GetMapping("/search/{content}/{type}/{page}")
    public Result getOrderBySearch(@PathVariable String content,@PathVariable int type, @PathVariable int page){
        List search = studyService.search (content,type,page);
        return new Result(Code.GET_OK,search,"获取成功");
    }
    //获取帖子点赞用户id
    @GetMapping("/commentNum/{id}")
    public Result getPostcommentNum(@PathVariable Long id){
        String postcommentNum = studyService.getPraiseUseridlist(id);
        System.out.println("返回的值是："+postcommentNum);
        return new Result(Code.GET_OK,postcommentNum,"获取成功");
    }
    @DeleteMapping("/{id}")
    public Result deletePostById(@PathVariable Long id){
        int i = studyService.deleteStudy(id);
        if(i == 1){
            return new Result(Code.DELETE_OK,"删除成功");
        }
        return new Result(Code.DELETE_ERR,"删除失败");
    }
}
