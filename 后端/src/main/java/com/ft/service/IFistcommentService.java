package com.ft.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ft.domain.Fistcomment;
import com.ft.domain.Post;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Author: ft
 * @Date: 2023/01/12/12:28
 * @Description:
 */

public interface IFistcommentService extends IService<Fistcomment> {
    @Transactional
    //增加评论
    public int addfistcomment(Fistcomment fistcomment) throws IllegalAccessException;
    //根据帖子id获取评论
    public List<Fistcomment> getfistcommentsByPostId(String postId);
    //删除单个回复记录
    public int deleteFistcomment(Long id);
    @Transactional
    //根据评论id删除多个回复记录
    public int deleteFistcommentArray(String[] array,Long belongFistcomment);
}
