package com.ft.service;

import java.util.List;

/**
 * @Author: ft
 * @Date: 2022/11/04/10:26
 * @Description:
 */
public interface IndexService {
    public List getIndexBuyOrder();
    public List getIndexSendorder();
    public List getIndexWorkorder();
    public List getIndexNeed();
}
