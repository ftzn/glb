/**
 * @Aurhor 把手给我
 * @Date 2022/9/30 17:38
 * @Version 1.0
 **/
package com.ft.domain;

public class UserAddress {
    private int id;
    private String name;
    private String phone;
    private String address;
    private Boolean defaultaddress;

    public Boolean getDefaultaddress() {
        return defaultaddress;
    }

    public void setDefaultaddress(Boolean defaultaddress) {
        this.defaultaddress = defaultaddress;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "UserAddress{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                ", address='" + address + '\'' +
                ", defaultaddress='" + defaultaddress + '\'' +
                '}';
    }
}
