package com.ft.service.impl;

import com.ft.domain.Sendtype;
import com.ft.dao.SendtypeDao;
import com.ft.service.ISendtypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author com/ft
 * @since 2022-08-24
 */
@Service
public class SendtypeServiceImpl extends ServiceImpl<SendtypeDao, Sendtype> implements ISendtypeService {

}
