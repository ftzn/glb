package com.ft.controller;

import com.ft.domain.Nongsuke;
import com.ft.domain.NongsukeOrder;
import com.ft.service.INongsukeOrderService;
import com.ft.service.INongsukeService;
import com.ft.utils.Code;
import com.ft.utils.Result;
import com.ft.utils.SnowFlakeGenerateIdWorker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author: ft
 * @Date: 2023/02/09/9:04
 * @Description:
 */
@RestController
@RequestMapping("/nongsukeorder")
@CrossOrigin
public class NongsukeOrderController {
    @Autowired
    private INongsukeOrderService nongsukeorderService;
    @Autowired
    private RedisTemplate redisTemplate;

    @PostMapping("")
    public Result addNongsuke(@RequestBody NongsukeOrder nongsuke) throws IllegalAccessException {
        //创建订单编号
        SnowFlakeGenerateIdWorker snowFlakeGenerateIdWorker =
                new SnowFlakeGenerateIdWorker (0L, 0L);
        String id = snowFlakeGenerateIdWorker.generateNextId ();
        //设置订单编号
        nongsuke.setId (Long.valueOf (id));
        //返回创建结果
        int result = nongsukeorderService.addOrder (nongsuke);
        if (result == 1) {
            NongsukeOrder ns = nongsukeorderService.getOrder (Long.valueOf (id));
            return new Result (Code.POST_OK, ns, "创建商品成功");
        } else {
            return new Result (Code.POST_ERR, "创建商品失败");
        }
    }
    /**
     * 修改订单状态为已付款
     * @param orderid 订单id
     * @return
     */
    @PutMapping("/{orderid}")
    public Result updataOrder(@PathVariable String orderid){
        int i = nongsukeorderService.updataOrderState (1,orderid);
        if(i == 1){
            return new Result(Code.PUT_OK,"修改成功");
        }else {
            return new Result(Code.PUT_ERR,"修改失败");
        }
    }

}

