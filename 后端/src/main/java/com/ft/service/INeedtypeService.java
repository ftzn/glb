package com.ft.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ft.domain.Buytype;
import com.ft.domain.Needtype;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author com/ft
 * @since 2022-08-24
 */
public interface INeedtypeService extends IService<Needtype> {

}
