package com.ft.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.sql.Timestamp;

@Data
@EqualsAndHashCode(callSuper = false)
@TableName("tzmarketorder")
public class TzmarketOrder implements Serializable {
    private static final long serialVersionUID = 1L;
    //所有属性的总和，每增加一个加1
    public static final int total = 13;
    //id
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    //订单标题
    private String title;
    //商品id
    @JsonSerialize(using = ToStringSerializer.class)
    private Long goodsid;
    //下单时商品单价
    private String goodprice;
    //下单时商品分类
    private int goodtype;
    //购买的商品数量
    private int buynum;
    //商品总价
    private Double totalprice;
    //帖子创建时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp createTime;
    //帖子修改时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp updateTime;
    //帖子删除时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp overTime;
    //订单状态
    private int state;
    //发布用户id
    private int issuseuserid;
    //订单图片
    private String imglist;
    //联系人
    private String contacts;
    //联系电话
    private String phone;
    //地址
    private String address;
}
