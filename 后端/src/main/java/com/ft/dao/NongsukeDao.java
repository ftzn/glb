package com.ft.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ft.domain.Nongsuke;
import com.ft.domain.Tzmarket;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

/**
 * @Author: ft
 * @Date: 2023/02/08/9:26
 * @Description:
 */
@Mapper
public interface NongsukeDao extends BaseMapper<Nongsuke> {
    @Insert("insert into nongsuke(id,type,state,title,price,content,order_img,index_img," +
            "issue_user,issue_user_name,issue_user_img,num,pay_type,ordertypeid,buyusernum,norms) values(#{id},#{type},#{state},#{title},#{price},#{content},#{orderImg}," +
            "#{indexImg},#{issueUser},#{issueUserName},#{issueUserImg},#{num},#{payType},#{ordertypeid},#{buyusernum},#{norms})")
    public int addNongsuke(Nongsuke nongsuke);
    /**
     * 根据订单id查询订单信息
     * @param id 订单id
     * @return 订单对象
     */
    @Select("select * from nongsuke where id =#{id}")
    public Nongsuke selectNongsuke(Long id);

    /**
     * 查询指定字段的数据用于微信支付
     * @param id 订单id
     * @return 订单对象
     */
    @Select("select id,title,price from nongsuke where id =#{id}")
    public Nongsuke selectNongsukeToPay(Long id);

    /**
     * 付款后，修改订单状态
     * @param state 要修改成的状态
     * @return 返回受影响的行数
     */
    @Update("update nongsuke set state = #{state} where id =#{id}")
    public int updataOrder(int state,String id);
    /**
     * 获取订单的创建时间信息
     * @param id 订单号
     * @return 时间
     */
    @Select ("select create_time from nongsuke where id =#{id}")
    public String getcreateTime(Long id);

    /**
     * 获取订单的更新时间
     * @param id 订单号
     * @return 时间
     */
    @Select ("select update_time from nongsuke where id =#{id}")
    public String getupdateTime(Long id);

}
