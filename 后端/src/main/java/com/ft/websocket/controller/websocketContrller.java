package com.ft.websocket.controller;

import com.ft.domain.Message;
import com.ft.domain.SystemMessage;
import com.ft.service.ISystemService;
import com.ft.utils.Code;
import com.ft.utils.Result;
import com.ft.websocket.service.ChatEndpoint;
import org.apache.ibatis.annotations.Select;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.xml.ws.Action;
import java.util.List;

/**
 * @Author: ft
 * @Date: 2023/02/14/9:43
 * @Description:
 */
@RestController
@RequestMapping("/wt")
@CrossOrigin
public class websocketContrller {
    @Autowired
    private ISystemService service;
    @PostMapping("/addsysetm")
    public Result webSocket(@RequestBody SystemMessage message) throws IllegalAccessException {
        int i = service.addSystemMessage(message);
        if(i == 1){
            return new Result(Code.POST_OK,1,"推送全部消息成功");
        }
        return new Result(Code.POST_ERR,0,"推送系统消息成功");
    }

    /**
     * 更新系统消息
     * @param message
     * @return
     * @throws IllegalAccessException
     */
    @PutMapping("/update")
    public Result updateSystemMessage(@RequestBody SystemMessage message) throws IllegalAccessException {
        int i = service.updateSystemMessage(message);
        if (i == 1){
            return new Result(Code.PUT_OK,1,"更新系统消息成功");
        }
        return new Result(Code.PUT_ERR,0,"更新系统消息失败");
    }

    /**
     * 删除系统消息
     * @param state
     * @param id
     * @return
     */
    @DeleteMapping("/delete/{state}/{id}")
    public Result deleteSystemMessage(@PathVariable int state, @PathVariable Long id){
        int i = service.deleteSystemMessage(state, id);
        if (i == 1){
            return new Result(Code.DELETE_OK,"删除系统消息成功");
        }
        return new Result(Code.DELETE_ERR,"删除系统消息失败");
    }

    /**
     * 获取所有的公告
     * @return
     */
    @GetMapping("list")
    public Result getSystemMesageList(){
        List<SystemMessage> systemMessagesList = service.getSystemMessagesList();
        return new Result(Code.GET_OK,systemMessagesList,"获取公告成功");
    }

    /**
     * 获取在线人数
     * @param userid
     * @return
     */
    @GetMapping("/usernum/{userid}")
    public Result getUserNum(@PathVariable int userid){
        if(userid== 10001){
            return  new Result(Code.GET_OK,ChatEndpoint.onlineCount,"获取在线人数成功");
        }else {
            return new Result(Code.GET_ERR,"您没有权限！");
        }
    }

    /**
     * 获取在线用户信息
     * @param userid
     * @return
     */
    @GetMapping("/userinfo/{userid}")
    public Result getUserInfo(@PathVariable int userid){
        if(userid == 10001){
            System.out.println(ChatEndpoint.onlineUsers);
            return  new Result(Code.GET_OK,ChatEndpoint.onlineUsers,"获取在线用户成功");
        }else {
            return new Result(Code.GET_ERR,"您没有权限！");
        }
    }
}
