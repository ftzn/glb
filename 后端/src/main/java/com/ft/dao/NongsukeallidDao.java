package com.ft.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ft.domain.NongsukeOrder;
import com.ft.domain.Nongsukeallid;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface NongsukeallidDao extends BaseMapper<Nongsukeallid>  {
    @Insert("insert into nongsukeallid(id,issueid,title,orderlist,buyname,buyphone,buyaddress,state,price) " +
            "values(#{id},#{issueid},#{title},#{orderlist},#{buyname},#{buyphone},#{buyaddress},#{state},#{price})")
    public int addOrder(Nongsukeallid nongsuke);
    /**
     * 根据订单id查询订单信息
     * @param id 订单id
     * @return 订单对象
     */
    @Select("select * from nongsukeallid where id =#{id}")
    public Nongsukeallid selectOrder(Long id);

    /**
     * 查询指定字段的数据用于微信支付
     * @param id 订单id
     * @return 订单对象
     */
    @Select("select id,title,price from nongsukeallid where id =#{id}")
    public Nongsukeallid selectOrderToPay(Long id);

    /**
     * 付款后，修改订单状态
     * @return 返回受影响的行数
     */
    @Update("update nongsukeallid set state = 1 where id =#{id}")
    public int updataOrderState(String id);
    /**
     * 获取订单的创建时间信息
     * @param id 订单号
     * @return 时间
     */
    @Select ("select create_time from nongsukeallid where id =#{id}")
    public String getcreateTime(Long id);

    /**
     * 获取订单的更新时间
     * @param id 订单号
     * @return 时间
     */
    @Select ("select update_time from nongsukeallid where id =#{id}")
    public String getupdateTime(Long id);

    /**
     * 获取订单状态
     * @param id
     * @return
     */
    @Select("select state from nongsukeallid where id =#{id}")
    public int getOrderState(Long id);

    /**
     * 退款后修改订单状态
     * @param orderid
     * @return
     */
    @Update("update nongsukeallid set state = 5 where id =#{orderid}")
    public int refundState(String orderid);

    /**
     * 购买户完成订单
     * @param orderid
     * @return
     */
    @Update("update nongsukeallid set state = 3 where id =#{orderid}")
    public int issueaccomplishOrder(String orderid);
}
