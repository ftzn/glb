package com.ft.controller;

import com.ft.domain.Note;
import com.ft.service.INoteService;
import com.ft.utils.Code;
import com.ft.utils.Result;
import com.ft.utils.SnowFlakeGenerateIdWorker;
import org.apache.ibatis.annotations.Delete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author: ft
 * @Date: 2023/02/09/22:08
 * @Description:
 */
@RestController
@RequestMapping("/note")
@CrossOrigin
public class NoteController {
    @Autowired
    private INoteService noteService;
    @PostMapping("")
    public Result addNote(@RequestBody Note note) throws IllegalAccessException {
        //创建订单编号
        SnowFlakeGenerateIdWorker snowFlakeGenerateIdWorker =
                new SnowFlakeGenerateIdWorker(0L,0L);
        String id = snowFlakeGenerateIdWorker.generateNextId();
        //设置订单编号
        note.setId(Long.valueOf(id));
        int i = noteService.addNote (note);
        Note note1 = noteService.getNote (Long.valueOf (id));
        if (i == 1){
            return new Result(Code.POST_OK,note1,"创建纸条成功");
        }else if (i == 3){
            return new Result(Code.POST_ERR,1,"今日可添加次数已达上限");
        }else {
            return new Result(Code.POST_ERR,2,"创建纸条失败");
        }
    }

    @DeleteMapping("/delete")
    public Result deleteNote(@PathVariable Long id){
        int i = noteService.deleteNote (id);
        if(i == 1){
            return new Result(Code.DELETE_OK,"删除纸条成功");
        }else {
            return new Result(Code.DELETE_ERR,"删除纸条失败");
        }
    }

    @PutMapping("/update/{title}/{content}/{id}")
    public Result updataNote(@PathVariable String title,@PathVariable String content,@PathVariable Long id){
        int i = noteService.updateNote (title, content, id);
        if (i == 1){
            return new Result(Code.PUT_OK,"更新纸条成功");
        }else {
            return new Result(Code.PUT_ERR,"更新纸条失败");
        }
    }

    @GetMapping("/getnote/{page}/{userid}")
    public Result listNote(@PathVariable int page,@PathVariable int userid){
        List<Note> boyNotes = noteService.getNotes (page, userid);
        return new Result(Code.GET_OK,boyNotes,"列表成功");
    }

    @GetMapping("/gettakeoutnote/{page}/{userid}")
    public Result gettakeOutNote(@PathVariable int page,@PathVariable int userid){
        List<Note> outNotes = noteService.getOutNotes (page, userid);
        return new Result(Code.GET_OK,outNotes,"获取用户抽取纸条成功");
    }
    @GetMapping("/notenum/{T}")
    public Result getNoteNumber(@PathVariable int T){
        Long aLong = noteService.getnoteNumber (T);
        return new Result(Code.GET_OK, aLong,"查询成功");
    }
    @GetMapping("/getnotenum/boy")
    public Result getNotenumToBoy(){
        String notenum = String.valueOf (noteService.getNotenumBoy());
        return new Result(Code.GET_OK,notenum,"获取成功");
    }
    @GetMapping("/getnotenum/girl")
    public Result getNotenumToGirl(){
        String notenum = String.valueOf (noteService.getNotenumGirl());
        return new Result(Code.GET_OK,notenum,"获取成功");
    }
    @GetMapping("/getnoterandom/{T}/{userid}")
    public Result getNoteRandom(@PathVariable int T,@PathVariable int userid){
        Note noteRandom = noteService.getNoteRandom (T, userid);
        return new Result(Code.GET_OK,noteRandom,"获取成功");
    }
}
