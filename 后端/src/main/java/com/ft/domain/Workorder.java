package com.ft.domain;

import com.baomidou.mybatisplus.annotation.TableName;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author com/ft
 * @since 2022-08-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("workorder")
public class Workorder implements Serializable {
    private static final long serialVersionUID = 1L;
    //所有属性的总和，每增加一个加1
    public static final int total = 21;
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    //商品类型
    private Integer type;
    /**
     *  0发布，1已付款，2接单中，3完成,，4问题订单
     */
    private Integer state;
    //标题
    private String title;
    /**
     * 订单内容
     */
    private String content;

    /**
     * 联系人名称
     */
    private String contacts;

    /**
     * 联系人电话
     */
    private String contactsPhone;

    /**
     * 联系人地址
     */
    private String contactsAddress;

    /**
     * 发布用户
     */
    private Integer issueUser;
    /**
     * 发布用户名
     */
    private String issueUserName;
    /**
     * 发布用户头像
     */
    private String issueUserImg;
    /**
     * 接单用户
     */
    private Integer belongUser;

    /**
     * 支付方式，0支付宝,1微信
     */
    private Integer payType;

    /**
     * 订单创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp createTime;

    /**
     * 订单修改时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp updateTime;

    /**
     * 订单完成时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp overTime;
    //是否匿名0不匿名，1匿名
    private int hideName;
    //价格
    private int price;
    //订单备注
    private String remarks;
    //订单图片地址
    private String orderImg;
    /**
     * 订单所属类型
     */
    private Integer ordertypeid;
}
