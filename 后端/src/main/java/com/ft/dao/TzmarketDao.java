package com.ft.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ft.domain.Lostarticle;
import com.ft.domain.Tzmarket;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author com/ft
 * @since 2022-08-24
 */
@Mapper
public interface TzmarketDao extends BaseMapper<Tzmarket> {
    /**
     * 创建订单
     * @param order 添加的订单对象
     * @return 受影响的行数
     */
    @Insert("insert into tzmarket(id,type,state,title,price,content,order_img,index_img," +
            "issue_user,issue_user_name,issue_user_img,belong_user,pay_type,hide_name,ordertypeid,likeuser,lodandnew,norms,color,model) values(#{id},#{type},#{state},#{title},#{price},#{content},#{orderImg}," +
            "#{indexImg},#{issueUser},#{issueUserName},#{issueUserImg},#{belongUser},#{payType},#{hideName},#{ordertypeid},#{likeuser},#{lodandnew},#{norms},#{color},#{model})")
    public int createTzmarket(Tzmarket order);

    /**
     * 根据订单id查询订单信息
     * @param id 订单id
     * @return 订单对象
     */
    @Select("select * from tzmarket where id =#{id}")
    public Tzmarket selectTzmarket(Long id);

    @Select("select state from tzmarket where id =#{id}")
    public int getState(Long id);

    /**
     * 查询指定字段的数据用于微信支付
     * @param id 订单id
     * @return 订单对象
     */
    @Select("select id,title,price from tzmarket where id =#{id}")
    public Tzmarket selectTzmarketToPay(Long id);

    /**
     * 付款后，修改订单状态
     * @param state 要修改成的状态
     * @return 返回受影响的行数
     */
    @Update ("update tzmarket set state = #{state}, belong_user = #{belongUser} where id =#{id}")
    public int updataOrder(int state,String id,int belongUser);
    /**
     * 获取订单的创建时间信息
     * @param id 订单号
     * @return 时间
     */
    @Select ("select create_time from tzmarket where id =#{id}")
    public String getcreateTime(Long id);

    /**
     * 获取订单的更新时间
     * @param id 订单号
     * @return 时间
     */
    @Select ("select update_time from tzmarket where id =#{id}")
    public String getupdateTime(Long id);

    /**
     * 购买用户退款
     * @param
     * @return
     */
    @Update("update tzmarket set state = 0,belong_user = null where id =#{orderid}")
    public int buyUserRefund (String orderid);

    /**
     * 发布用户取消订单
     * @param orderid
     * @return
     */
    @Update("update tzmarket set state = -1 where id =#{orderid}")
    public int  updateOrderRefund (String orderid);

    /**
     * 购买用户取消订单
     * @param orderid
     * @return
     */
    @Update("update tzmarket set state = 0,belong_user = null where id =#{orderid}")
    public int OverUserOrder(String orderid);

    /**
     * 购买户完成订单
     * @param orderid
     * @return
     */
    @Update("update tzmarket set state = 3 where id =#{orderid}")
    public int issueaccomplishOrder(String orderid);
}
