package com.ft; /**
 * @Aurhor 把手给我
 * @Date 2022/6/1 10:20
 * @Version 1.0
 **/

import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;

public class mybatisplusMain {
    public static void main(String[] args) {
        //1.创建代码生成器
        AutoGenerator ar = new AutoGenerator();
        //2.配置数据源
        DataSourceConfig datasource = new DataSourceConfig();
        datasource.setDriverName("com.mysql.cj.jdbc.Driver");
        datasource.setUrl("jdbc:mysql://localhost:3306/glb?serverTimezone=UTC");
        datasource.setUsername("root");
        datasource.setPassword("root");
        ar.setDataSource(datasource);
        //3.设置全局配置
        GlobalConfig ag = new GlobalConfig();
        //设置输出目录
        ag.setOutputDir("D:\\web\\吉利校园帮\\glb\\后端\\src\\main\\java");
        ag.setAuthor("com/ft");
        ag.setOpen(false);
        //设置是否覆盖原始文件
        ag.setFileOverride(true);
        //设置数据库接口名
        ag.setMapperName("%sDao");
        ar.setGlobalConfig(ag);
        //4.设置包名相关配置
        PackageConfig pg = new PackageConfig();
        pg.setParent("com.ft");//设置生成的包名
        pg.setEntity("domain");//设置实体类包名
        pg.setMapper("dao");//设置数据层包名
        ar.setPackageInfo(pg);
        //5.策略配置
        StrategyConfig sg = new StrategyConfig();
        sg.setTablePrefix("");//设置数据库表的前缀名称；模块名 = 数据库名 - 前缀
        sg.setRestControllerStyle(true);//设置rest风格
        sg.setEntityLombokModel(true);
        ar.setStrategy(sg);
        //执行生成
        ar.execute();
    }
}
