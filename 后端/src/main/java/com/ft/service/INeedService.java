package com.ft.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.ft.domain.Need;

import java.io.IOException;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author com/ft
 * @since 2022-08-24
 */
public interface INeedService{
    /**
     * 创建需求订单
     * @param order 帮我买订单对象
     * @return 受影响的行数
     */
    public int createNeed(Need order) throws JsonProcessingException, IllegalAccessException;

    /**
     * 根据订单id查询所有订单信息
     * @param id 订单id
     * @return 订单对象
     */
    public Need selectNeed(Long id);
    /**
     * 查询指定字段的数据用于微信支付
     * @param id 订单id
     * @return 订单对象
     */
    public Need selectNeedToPay(Long id);

    /**
     * 付款后修改订单状态
     * state:修改的状态
     * id:订单id
     * userid:用户id
     * @return 返回受影响的结果
     */
    public int  updataOrder(int state,String id,int userid);
    /**
     * 懒人中心分页获取订单数据
     * @param page
     * @return
     */
    public List getOrder(int page);
    /**
     * 懒人中心搜索内容
     * @param content 搜索的数据
     * @return 订单集合
     */
    public List search(String content);


    /**
     * 获取用户以发布订单
     * @param userid
     * @return
     */
    public List getIssueOrderListByUser(String userid);

    /**
     * 获取用户已接单订单
     * @param userid
     * @return
     */
    public List getOrderListByUser(String userid);

    /**
     * 退款后修改订单状态
     * @param
     * @return
     */
    public int updateOrderRefund(String orderid);

    /**
     * 接单用户完成订单，确认修改订单状态
     * @return
     */
    public int accomplishOrder(String orderid) throws IOException, IllegalAccessException;


    /**
     * 发布用户完成订单
     * @param orderid
     * @param userid
     * @return
     * @throws IOException
     * @throws IllegalAccessException
     */
    public int issueOverOrder(String orderid,int userid) throws IOException, IllegalAccessException;

    /**
     * 用户接单
     * @param userid
     * @return
     */
    public int UserOrder(String orderid, int userid) throws IOException, IllegalAccessException;


    /**
     * 用户取消接单
     * @param userid
     * @return
     */
    public int OverUserOrder(String orderid,int userid) throws IOException, IllegalAccessException;
}
