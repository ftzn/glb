package com.ft.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * @Author: ft
 * @Date: 2023/01/11/15:06
 * @Description: 发帖表
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("post")
public class Post implements Serializable {
    private static final long serialVersionUID = 1L;
    //所有属性的总和，每增加一个加1
    public static final int total = 18;
    //id
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    //帖子类型
    private int type;
    //帖子内容
    private String content;
    //发布用户id
    private int issueUser;
    //发布用户名
    private  String issueUserName;
    //发布用户头像
    private  String issueUserImg;
    //贴子点赞数
    private int praisenum;
    //帖子评论数
    private int commentnum;
    //帖子分享数
    private int sharenum;
    //发帖定位
    private String location;
    //点赞用户id集合
    private String praiseUseridlist;
    //一级评论回复id集合
    private String commentidlist;
    //帖子图片集合
    private String postImglist;
    //帖子首页图片
    private String postIndeximg;
    //帖子创建时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp createTime;
    //帖子修改时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp updateTime;
    //帖子删除时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp overTime;
    //帖子状态
    private int state;

}
