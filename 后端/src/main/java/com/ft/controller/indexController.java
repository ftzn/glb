package com.ft.controller;

import com.ft.domain.Buyorder;
import com.ft.service.IndexService;
import com.ft.utils.Code;
import com.ft.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Set;

/**
 * @Author: ft
 * @Date: 2022/11/04/10:24
 * @Description: 前端首页控制层
 */
@RestController
@RequestMapping("/index")
@CrossOrigin
public class indexController {
    @Autowired
    IndexService indexService;

    /**
     * 前端首页获取帮我买优质订单
     * @return Result
     */
    @GetMapping("/buyorder")
    public Result indexGetBuyOrder(){
        List indexBuyOrder = indexService.getIndexBuyOrder ();
        return new Result(Code.GET_OK,indexBuyOrder,"获取成功");
    }

    /**
     * 前端首页获取帮我做优质订单
     * @return 优质订单集合
     */
    @GetMapping("/sendorder")
    public Result indexGetSendorders(){
        List indexSendOrder = indexService.getIndexSendorder();
        return new Result(Code.GET_OK,indexSendOrder,"获取成功");
    }
    /**
     * 前端首页获取帮我做优质订单
     * @return 优质订单集合
     */
    @GetMapping("/workorder")
    public Result indexGetWorkOrders(){
        List indexWorkOrder = indexService.getIndexWorkorder ();
        return new Result(Code.GET_OK,indexWorkOrder,"获取成功");
    }
    /**
     * 前端首页获取帮我做优质订单
     * @return 优质订单集合
     */
    @GetMapping("/need")
    public Result indexGetNeeds(){
        List indexNeedOrder = indexService.getIndexNeed ();
        return new Result(Code.GET_OK,indexNeedOrder,"获取成功");
    }
}
