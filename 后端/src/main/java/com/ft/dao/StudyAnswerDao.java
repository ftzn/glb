package com.ft.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ft.domain.Answer;
import com.ft.domain.StudyAnswer;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Author: ft
 * @Date: 2023/01/12/12:00
 * @Description:
 */
@Mapper
public interface StudyAnswerDao extends BaseMapper<StudyAnswer> {
    /**
     * 创建回复
     *
     * @param answer
     * @return
     */
    @Insert("insert into studyanswer(id,answer_content,answer_userid,answer_username,belongtofistcomment,answer_touserid,answer_tousername," +
            "state) values(#{id}, #{answerContent}, #{answerUserid},#{answerUsername}, #{belongtofistcomment},#{answerTouserid},#{answerTousername},#{state})")
    public int addAnswer(StudyAnswer answer);

    /**
     * 获取评论的创建时间信息
     *
     * @param id 订单号
     * @return 时间
     */
    @Select("select create_time from studyanswer where id =#{id}")
    public String getcreateTime(Long id);

    /**
     * 获取评论的更新时间
     *
     * @param id 评论id
     * @return 时间
     */
    @Select("select update_time from studyanswer where id =#{id}")
    public String getupdateTime(Long id);

    /**
     * 更加回复id获取所属评论id
     * @param id
     * @return
     */
    @Select("select belongtofistcomment from studyanswer where id = #{id}")
    public Long getBelongFistcomment(Long id);
    /**
     * 更具id删除回复
     *
     * @param id
     * @return
     */
    @Delete("delete from studyanswer where id = #{id}")
    public int deleteAnswer(Long id);

    /**
     * 根据数组id删除对应回复数据
     * @param answeridList
     * @return
     */
    @Delete(
            "<script>" +
                    "delete from studyanswer where id in" +
                    "<foreach collection='answeridList' open='(' close=')' separator=',' item='id'>" +
                    "#{id}" +
                    "</foreach>" +
            "</script>"
    )
    public int deleteAnswerArray(String[] answeridList);
}
