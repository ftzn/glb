package com.ft.controller;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.ft.domain.Buyorder;
import com.ft.domain.Tzmarket;
import com.ft.service.IBuyorderService;
import com.ft.service.ITzmarketService;
import com.ft.utils.Code;
import com.ft.utils.Result;
import com.ft.utils.SnowFlakeGenerateIdWorker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author com/ft
 * @since 2022-08-24
 */
@RestController
@RequestMapping("/tzmarket")
@CrossOrigin
public class TzmarketController {
    @Autowired
    private ITzmarketService tzmarketService;
    @Autowired
    private RedisTemplate redisTemplate;
    /**
     * 创建订单
     * @param tzmarket 订单对象
     * @return 创建的订单对象
     * @throws JsonProcessingException
     */
    @PostMapping()
    public Result creatrBuyOrder(@RequestBody Tzmarket tzmarket) throws JsonProcessingException, IllegalAccessException {
        //创建订单编号
        SnowFlakeGenerateIdWorker snowFlakeGenerateIdWorker =
                new SnowFlakeGenerateIdWorker(0L,0L);
        String id = snowFlakeGenerateIdWorker.generateNextId();
        //设置订单编号
        tzmarket.setId(Long.valueOf(id));
        //返回创建结果
        int result = tzmarketService.createTzmarket (tzmarket);
        if (result == 1){
            Tzmarket tz = tzmarketService.selectTzmarket (Long.valueOf(id));
            return new Result(Code.POST_OK,tz,"创建订单成功");
        }else {
            return new Result(Code.POST_ERR,"创建订单失败");
        }
    }
    /**
     * 修改订单状态为已付款
     * @param orderid 订单id
     * @return
     */
    @PutMapping("/{orderid}/{userid}")
    public Result updataOrder(@PathVariable String orderid,@PathVariable int userid) throws IOException, IllegalAccessException {
        int i = tzmarketService.updataOrder (1,orderid,userid);
        System.out.println("修改的结果是："+i);
        if(i == 1){
            return new Result(Code.PUT_OK,"修改成功");
        }else {
            return new Result(Code.PUT_ERR,"修改失败");
        }
    }


    /**
     * 跳蚤市场分页，根据类型获取订单数据
     * @param page
     * @return
     */
    @GetMapping("/{type}/{page}")
    public Result getOrderAll(@PathVariable int type,@PathVariable int page){
        System.out.println ("接收的数据是：type:"+type+"page:"+page);
        List order = tzmarketService.getOrder (type,page);
        System.out.println("跳蚤市场首页返回的数据是"+order);
        return new Result(Code.GET_OK,order,"获取成功");
    }

    /**
     * 跳蚤市场搜索内容
     * @param content
     * @return
     */
    @GetMapping("/search/{content}/{type}/{page}")
    public Result getOrderBySearch(@PathVariable String content,@PathVariable int type, @PathVariable int page){
        System.out.println("接收的数据是："+content+":"+page);
        List search = tzmarketService.search (content,type,page);
        return new Result(Code.GET_OK,search,"获取成功");
    }

    /**
     * 发布用户取消订单
     * @param orderid
     * @return
     */
    @PutMapping("/userrefund/{orderid}/{userid}")
    public Result updateOrderRefund(@PathVariable String orderid, @PathVariable int userid){
        int i = tzmarketService.updateOrderRefund(orderid);
        if(i == 1){
            return new Result(Code.PUT_OK,"取消订单成功");
        }else if(i == 2){
            return new Result(Code.PUT_ERR,"订单已经取消了喔");
        }
        return  new Result(Code.PUT_ERR,"取消订单失败");
    }

    /**
     * 购买用户完成订单
     * @param orderid
     * @param userid
     * @return
     */
    @PutMapping("/issueoverorder/{orderid}/{userid}")
    public Result issueOverOrder(@PathVariable String orderid,@PathVariable int userid) throws IOException, IllegalAccessException {
        int i = tzmarketService.issueOverOrder(orderid, userid);
        if(i == 1){
            return new Result(Code.PUT_OK,"订单已完成");
        }else if(i == 2){
            return new Result(Code.PUT_ERR,"必须付款后才能完成订单喔");
        }
        return new Result(Code.PUT_ERR,"订单完成失败");
    }
    /**
     * 获取用户发布订单
     */
    @GetMapping("/getuserissue/{userid}")
    public Result getUserissueTzmarket(@PathVariable int userid){
        List userissueTzmarket = tzmarketService.getUserissueTzmarket(userid);
        return new Result(Code.GET_OK,userissueTzmarket,"获取成功");
    }

    /**
     * 获取用户承接订单
     * @param userid
     * @return
     */
    @GetMapping("/getuserbuyorder/{userid}")
    public Result getUserBuyorders(@PathVariable int userid){
        List userBuyorders = tzmarketService.getUserBuyorders(userid);
        return new Result(Code.GET_OK,userBuyorders,"获取成功");
    }

    /**
     * 获取订单状态
     * @param orderid
     * @return
     */
    @GetMapping("/orderstate/{orderid}")
    public Result getOrderState(@PathVariable String orderid){
        int state = tzmarketService.getState(orderid);
        return new Result(Code.GET_OK,state,"获取成功");
    }
}

