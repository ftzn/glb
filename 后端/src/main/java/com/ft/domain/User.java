package com.ft.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *s
 * @author com/ft
 * @since 2022-08-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("user")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private String img;
    private String name;
    private  String password;
    /**
     * 0女，1男，默认女
     */
    private String sex;

    private String phone;

    private String address;

    /**
     * 0正常，1锁定，2逻辑删除
     */
    private Integer state;

    /**
     * 0普通用户，1vip用户，2管理员
     */
    private Integer limit;

    /**
     * 用户创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp createTime;

    /**
     * 用户更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp updateTime;
    /**
     * 用户最后一条消息
     */
    private String lastmessage;
    /**
     * 用户消息总数
     */
    private int totalMessage;
    /**
     * 用户好友列表
     */
    private String userFriend;
    /**
     * 用户金额
     */
    private double money;
}
