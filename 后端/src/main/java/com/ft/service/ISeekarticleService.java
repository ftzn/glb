package com.ft.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.ft.domain.Lostarticle;
import com.ft.domain.Seekarticle;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author com/ft
 * @since 2022-08-24
 */
public interface ISeekarticleService extends IService<Seekarticle> {
    /**
     * 创建失物招领订单
     * @param order 帮我买订单对象
     * @return 受影响的行数
     */
    public int createSeekarticle(Seekarticle order) throws JsonProcessingException, IllegalAccessException;

    /**
     * 根据订单id查询所有订单信息
     * @param id 订单id
     * @return 订单对象
     */
    public Seekarticle selectSeekarticle(Long id);
    /**
     * 查询指定字段的数据用于微信支付
     * @param id 订单id
     * @return 订单对象
     */
    public Seekarticle selectSeekarticleToPay(Long id);

    /**
     * 付款后修改订单状态
     * state:修改的状态
     * id:订单id
     * userid:用户id
     * @return 返回受影响的结果
     */
    public int  updataOrder(int state,String id,int userid);
    /**
     * 失物招领分页获取订单数据
     * @param page
     * @return
     */
    public List getOrder(int type,int page);

    /**
     * 失物招领搜索内容
     * @param content 搜索的数据
     * @return 订单集合
     */
    public List search(String content,int type,int page);
}
