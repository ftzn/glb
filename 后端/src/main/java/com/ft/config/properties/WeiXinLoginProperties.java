package com.ft.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 小程序配置类
 */
@Configuration
@ConfigurationProperties("weixin.login.info")
@Data
public class WeiXinLoginProperties {
    private String appid = "";
    private String appsecret = "";
    private String redirectUrl = "";
}
