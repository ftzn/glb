package com.ft.controller;

import com.ft.domain.Fistcomment;
import com.ft.service.IAnswerService;
import com.ft.service.IFistcommentService;
import com.ft.utils.Code;
import com.ft.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author: ft
 * @Date: 2023/01/12/18:04
 * @Description:
 */
@RestController
@CrossOrigin
@RequestMapping("/fistcomment")
public class FIstcommentController {
    @Autowired
    private IFistcommentService fistcommentService;
    @PostMapping()
    public Result addFistComment(@RequestBody Fistcomment fistcomment) throws IllegalAccessException {
        int i = fistcommentService.addfistcomment (fistcomment);
        if(i == 1){
            return new Result(Code.POST_OK,fistcomment,"创建评论成功!");
        }else {
            return new Result(Code.POST_ERR,"创建评论失败!");
        }
    }
    @GetMapping("/{id}")
    public Result getfistcommentsByPostId(@PathVariable String id){
        List list = fistcommentService.getfistcommentsByPostId (id);
        if(list.size () > 0){
            return new Result(Code.GET_OK,list,"获取成功!");
        }
        return new Result(Code.GET_ERR,"获取失败!");
    }
    @DeleteMapping("/byid/{id}")
    public Result deleteFistcommentById(@PathVariable Long id){
        int i = fistcommentService.deleteFistcomment (id);
        if(i == 1){
            return new Result(Code.DELETE_OK,"删除成功");
        }else {
            return new Result(Code.DELETE_ERR,"删除失败!");
        }
    }
    @DeleteMapping("/byarray/{answerIdlist}/{id}")
    public Result deleteStudyAnswerByArray(@PathVariable String answerIdlist ,@PathVariable Long id){
        String[] answerlist = answerIdlist.split (",");
        int i = fistcommentService.deleteFistcommentArray (answerlist, id);
        if(i > 0){
            return new Result (Code.DELETE_OK,"删除成功");
        }else{
            return new Result (Code.DELETE_ERR,"删除失败");
        }
    }
}
