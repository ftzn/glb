/**
 * @Aurhor 把手给我
 * @Date 2022/8/31 16:41
 * @Version 1.0
 * 阿里云短信工具类
 **/
package com.ft.utils;


import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.profile.DefaultProfile;

import java.util.Random;

public class SmsUtils {
//    public static void main(String[] args) {
//        String phone = "";
//        sendMessage(phone);
//    }
    //产生随机数，用于短信验证码
    public static String getNumber(){
        String tatle = "";
        for (int i = 0; i < 4;i++){
            int a = new Random().nextInt(10);
            tatle += String.valueOf(a);
        }
        return tatle;
    }
    /**
     * 发送验证码
     */
    public static int sendMessage(String phone,String code){
        // 短信API产品名称
        final String product = "Dysmsapi";
        // 短信API产品域名
        final String domain = "dysmsapi.aliyuncs.com";
        DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", "LTAI5t9QiwAFuFka6ZsferSF", "ZG7tzH36Ds6IkNIqkbG0a2gMV0v6OZ");
        //引入下面代码，固定
        try {
            DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
        } catch (ClientException e) {
            e.printStackTrace();
        }

        IAcsClient client = new DefaultAcsClient(profile);

        SendSmsRequest request = new SendSmsRequest();
        //签名
        request.setSignName("kaideo");
        //短信服务的模板Code
        request.setTemplateCode("SMS_250910656");
        //发送短信的手机号
        request.setPhoneNumbers(phone);
        String param = code;
        //param发送的验证码
        request.setTemplateParam("{\"code\":\""+param+"\"}");

        try {
            SendSmsResponse response = client.getAcsResponse(request);
            //发送成功返回2
            return 2;
        } catch (ServerException e) {
            System.out.println(e);
            return 0;
        } catch (ClientException e) {
            System.out.println(e);
            return 1;
        }

    }
}
