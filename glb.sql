/*
 Navicat Premium Data Transfer

 Source Server         : mysql
 Source Server Type    : MySQL
 Source Server Version : 80028
 Source Host           : localhost:3306
 Source Schema         : glb

 Target Server Type    : MySQL
 Target Server Version : 80028
 File Encoding         : 65001

 Date: 03/03/2023 15:27:00
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for answer
-- ----------------------------
DROP TABLE IF EXISTS `answer`;
CREATE TABLE `answer`  (
  `id` bigint(0) NOT NULL COMMENT '回复表',
  `answer_content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '回复类容',
  `answer_userid` int(0) NULL DEFAULT NULL COMMENT '发布该回复的用户id',
  `answer_username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '发布用户名称',
  `belongtofistcomment` bigint(0) NULL DEFAULT NULL COMMENT '所属上级评论id',
  `answer_touserid` int(0) NULL DEFAULT NULL COMMENT '回复的用户id',
  `answer_tousername` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '回复的用户名',
  `state` int(0) NULL DEFAULT 0 COMMENT '回复状态',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `over_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `answer_userid`(`answer_userid`) USING BTREE,
  INDEX `answer_touserid`(`answer_touserid`) USING BTREE,
  INDEX `belongtofistcomment`(`belongtofistcomment`) USING BTREE,
  CONSTRAINT `answer_ibfk_1` FOREIGN KEY (`answer_userid`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `answer_ibfk_2` FOREIGN KEY (`answer_touserid`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `answer_ibfk_3` FOREIGN KEY (`belongtofistcomment`) REFERENCES `fistcomment` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of answer
-- ----------------------------

-- ----------------------------
-- Table structure for buyorder
-- ----------------------------
DROP TABLE IF EXISTS `buyorder`;
CREATE TABLE `buyorder`  (
  `id` bigint(0) NOT NULL COMMENT '帮我买',
  `type` int(0) NULL DEFAULT NULL COMMENT '订单类型',
  `state` int(0) NULL DEFAULT 0 COMMENT ' 0发布，1已付款，2接单中，3完成,，4问题订单,5已退款',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '订单标题',
  `content` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '订单内容',
  `contacts` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '联系人名称',
  `contacts_phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '联系人电话',
  `contacts_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '联系人地址',
  `issue_user` int(0) NULL DEFAULT NULL COMMENT '发布人id',
  `issue_user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '发布用户名称',
  `issue_user_img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '发布用户头像',
  `belong_user` int(0) NULL DEFAULT NULL COMMENT '接单人id',
  `pay_type` int(0) NULL DEFAULT NULL COMMENT '支付方式，0支付宝,1微信',
  `create_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '订单创建时间',
  `update_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '订单修改时间',
  `over_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '订单完成时间',
  `hide_name` int(0) NULL DEFAULT 0 COMMENT '是否匿名0不匿名，1匿名',
  `price` int(0) NULL DEFAULT NULL COMMENT '订单价格',
  `remarks` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '订单备注',
  `order_img` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '订单图片',
  `ordertypeid` int(0) NULL DEFAULT NULL COMMENT '订单所属类型id',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `buytype`(`type`) USING BTREE,
  INDEX `buyissue_user`(`issue_user`) USING BTREE,
  INDEX `buybelong_user`(`belong_user`) USING BTREE,
  INDEX `buyistype`(`ordertypeid`) USING BTREE,
  CONSTRAINT `buybelong_user` FOREIGN KEY (`belong_user`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `buyissue_user` FOREIGN KEY (`issue_user`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `buyistype` FOREIGN KEY (`ordertypeid`) REFERENCES `orderall` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `buytype` FOREIGN KEY (`type`) REFERENCES `buytype` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of buyorder
-- ----------------------------
INSERT INTO `buyorder` VALUES (1078082799900033024, 3, 4, '帮我买药品', '孩子生病了需要一位好心人在校医院带一包感冒灵救命，感谢', '张三', '17628178054', '俊园3号公寓俊3-311', 10001, '宇宙最帅的那个人', 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/用户头像/7EJBLrOeoTR959a32458f2a7a8ddec3933707fab9fda.jpg', 10003, NULL, '2023-02-22 22:36:09', '2023-02-26 13:33:15', '2023-02-22 22:36:09', 0, 1, '越快越好，等着救命', 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/帮我买/药品/Je8z3NN2avhS8dfef6d6ba7253204b23af36f1a7d8b9.jpg,https://ft-glb.oss-cn-chengdu.aliyuncs.com/帮我买/药品/gLWnjn0nNJVO184b98106b8063bd14e41e6ef869aace.jpg', 1);
INSERT INTO `buyorder` VALUES (1078346441665019904, 2, 5, '帮我买零食', '帮我买一包黄瓜味的薯片，一包卫龙辣条，一包泡面', '小李', '17628178054', '秀园3-312', 10003, '帅气的小王', 'https://cdn.uviewui.com/uview/album/3.jpg', NULL, NULL, '2023-02-23 16:03:46', '2023-02-23 16:08:43', '2023-02-23 16:03:46', 0, 1, '越快越好', 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/帮我买/零食/2dBXKiXRmVJq8dfef6d6ba7253204b23af36f1a7d8b9.jpg,https://ft-glb.oss-cn-chengdu.aliyuncs.com/帮我买/零食/7vdM9tA35V44184b98106b8063bd14e41e6ef869aace.jpg', 1);
INSERT INTO `buyorder` VALUES (1079447211433000960, 3, 4, '帮我买一瓶酒精', '需要一瓶酒精，来个靓仔帮我买下', '小李', '17628178054', '秀园3-312', 10003, '帅气的小王', 'https://cdn.uviewui.com/uview/album/3.jpg', 10001, NULL, '2023-02-26 16:57:50', '2023-02-26 17:03:19', '2023-02-26 16:57:50', 0, 1, '越快越好', '', 1);
INSERT INTO `buyorder` VALUES (1079507657821257728, 6, 5, '帮我买奶茶', '帮我在上海嬢嬢哪里带一杯珍珠奶茶', '张三', '17628178054', '俊园3号公寓俊3-311', 10001, '宇宙最帅的那个人', 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/用户头像/7EJBLrOeoTR959a32458f2a7a8ddec3933707fab9fda.jpg', NULL, NULL, '2023-02-26 20:58:02', '2023-02-26 21:17:07', '2023-02-26 20:58:02', 0, 1, '越快越好', '', 1);
INSERT INTO `buyorder` VALUES (1079539070675189760, 1, 5, '买包烟', '1', '张三', '17628178054', '俊园3号公寓俊3-311', 10001, '宇宙最帅的那个人', 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/用户头像/7EJBLrOeoTR959a32458f2a7a8ddec3933707fab9fda.jpg', NULL, NULL, '2023-02-26 23:02:51', '2023-02-26 23:04:57', '2023-02-26 23:02:51', 0, 1, '1', '', 1);
INSERT INTO `buyorder` VALUES (1080104144166977536, 5, 5, '帮我买盒水果捞', '帮我买盒水果捞，多来点苹果', '张三', '17628178054', '俊园3号公寓俊3-311', 10001, '宇宙最帅的那个人', 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/用户头像/fyH0eYwCiGwM62b824bb48d92e588d5cd16d6e151594.jpg', NULL, NULL, '2023-02-28 12:28:15', '2023-02-28 12:41:12', '2023-02-28 12:28:15', 0, 1, '1', '', 1);

-- ----------------------------
-- Table structure for buytype
-- ----------------------------
DROP TABLE IF EXISTS `buytype`;
CREATE TABLE `buytype`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '帮我买类型表',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '类型名称',
  `state` int(0) NULL DEFAULT 0 COMMENT '类型状态，默认0正常，1锁定，2删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of buytype
-- ----------------------------
INSERT INTO `buytype` VALUES (1, '烟酒', 0);
INSERT INTO `buytype` VALUES (2, '零食', 0);
INSERT INTO `buytype` VALUES (3, '药品', 0);
INSERT INTO `buytype` VALUES (4, '小吃', 0);
INSERT INTO `buytype` VALUES (5, '水果', 0);
INSERT INTO `buytype` VALUES (6, '饮料奶茶', 0);
INSERT INTO `buytype` VALUES (7, '主食套餐', 0);
INSERT INTO `buytype` VALUES (8, '生活用品', 0);
INSERT INTO `buytype` VALUES (9, '学习用品', 0);
INSERT INTO `buytype` VALUES (10, '其他', 0);

-- ----------------------------
-- Table structure for fistcomment
-- ----------------------------
DROP TABLE IF EXISTS `fistcomment`;
CREATE TABLE `fistcomment`  (
  `id` bigint(0) NOT NULL COMMENT '评论表',
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '评论内容',
  `comment_userid` int(0) NULL DEFAULT NULL COMMENT '发布该回复的用户id',
  `comment_username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '发布用户名称',
  `comment_userimg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '发布用户头像',
  `answer_idlist` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '回复给评论的回复id集合',
  `belongpost` bigint(0) NULL DEFAULT NULL COMMENT '所属上级帖子id',
  `state` int(0) NULL DEFAULT 0 COMMENT '回复状态',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `over_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `answer_userid`(`comment_userid`) USING BTREE,
  INDEX `belongpost`(`belongpost`) USING BTREE,
  CONSTRAINT `fistcomment_ibfk_1` FOREIGN KEY (`comment_userid`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fistcomment_ibfk_2` FOREIGN KEY (`belongpost`) REFERENCES `post` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fistcomment
-- ----------------------------
INSERT INTO `fistcomment` VALUES (1625694055394095106, '评论2', 10003, '帅气的小王', 'https://cdn.uviewui.com/uview/album/3.jpg', '', 1075371406499250176, 0, '2023-02-15 11:11:47', '2023-02-15 11:11:47', '2023-02-15 11:11:47');

-- ----------------------------
-- Table structure for lostarticle
-- ----------------------------
DROP TABLE IF EXISTS `lostarticle`;
CREATE TABLE `lostarticle`  (
  `id` bigint(0) NOT NULL COMMENT '失物招领订单',
  `type` int(0) NULL DEFAULT 0 COMMENT '订单类型',
  `ordertypeid` int(0) NULL DEFAULT NULL COMMENT '订单所属类型id',
  `state` int(0) NULL DEFAULT 0 COMMENT ' 0发布，1已付款，2认领中，3完成,，4问题订单',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '订单标题',
  `content` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '订单内容',
  `weizhi` int(0) NULL DEFAULT 0 COMMENT '物品当前位置，0，本人代管,1.遗失处，2.其他',
  `paid` int(0) NULL DEFAULT 0 COMMENT '是否有偿，0无偿，1有偿',
  `issue_user` int(0) NULL DEFAULT NULL COMMENT '发布人id',
  `issue_user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '发布用户名称',
  `issue_user_img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '发布用户头像',
  `belong_user` int(0) NULL DEFAULT NULL COMMENT '接单人id',
  `pay_type` int(0) NULL DEFAULT NULL COMMENT '支付方式，0支付宝,1微信',
  `create_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '订单创建时间',
  `update_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '订单修改时间',
  `over_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '订单完成时间',
  `hide_name` int(0) NULL DEFAULT 0 COMMENT '是否匿名0不匿名，1匿名',
  `price` double(2, 0) NULL DEFAULT 0 COMMENT '订单价格',
  `remarks` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '订单备注',
  `order_img` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '订单图片',
  `index_img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '订单首页图片',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `buytype`(`type`) USING BTREE,
  INDEX `buyissue_user`(`issue_user`) USING BTREE,
  INDEX `buybelong_user`(`belong_user`) USING BTREE,
  INDEX `buyorderid`(`ordertypeid`) USING BTREE,
  CONSTRAINT `lostarticle_ibfk_1` FOREIGN KEY (`belong_user`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `lostarticle_ibfk_2` FOREIGN KEY (`issue_user`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `lostarticleistype` FOREIGN KEY (`ordertypeid`) REFERENCES `orderall` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `lostarticletype` FOREIGN KEY (`type`) REFERENCES `lostarticletype` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of lostarticle
-- ----------------------------
INSERT INTO `lostarticle` VALUES (1046108782842609664, 1, 5, 0, '测试', '11', 0, 0, 10002, '机智的小美', 'https://cdn.uviewui.com/uview/album/1.jpg', NULL, NULL, '2022-11-26 17:02:50', '2022-11-26 17:02:50', '2022-11-26 17:02:50', 0, 0, '', 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/失物招领/XrPILrPNCxzQ6e84a30cb92e615d44f465467edce340.jpg,https://ft-glb.oss-cn-chengdu.aliyuncs.com/失物招领/r5M6ewQAg7un9e0432dfa0eda47d3bb4045c1439a73d.jpg', 'h');
INSERT INTO `lostarticle` VALUES (1046110351168372736, 1, 5, 0, '1', '1', 0, 0, 10002, '机智的小美', 'https://cdn.uviewui.com/uview/album/1.jpg', NULL, NULL, '2022-11-26 17:09:03', '2022-11-26 17:09:03', '2022-11-26 17:09:03', 0, 0, '1', 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/失物招领/s3B0fl0tzhQW6e84a30cb92e615d44f465467edce340.jpg,https://ft-glb.oss-cn-chengdu.aliyuncs.com/失物招领/hm5lIMzYQxyO9e0432dfa0eda47d3bb4045c1439a73d.jpg,https://ft-glb.oss-cn-chengdu.aliyuncs.com/失物招领/pkOgJOvjwrSY8dfef6d6ba7253204b23af36f1a7d8b9.jpg', 'h');
INSERT INTO `lostarticle` VALUES (1046111283641843712, 1, 5, 0, '123', '23', 0, 0, 10002, '机智的小美', 'https://cdn.uviewui.com/uview/album/1.jpg', NULL, NULL, '2022-11-26 17:12:46', '2022-11-26 17:12:46', '2022-11-26 17:12:46', 0, 0, '1', 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/失物招领/y8tUe7YcPqRy6e84a30cb92e615d44f465467edce340.jpg,https://ft-glb.oss-cn-chengdu.aliyuncs.com/失物招领/oZ7l0C9tM04T9e0432dfa0eda47d3bb4045c1439a73d.jpg,https://ft-glb.oss-cn-chengdu.aliyuncs.com/失物招领/4bjCdpH30qao8dfef6d6ba7253204b23af36f1a7d8b9.jpg', 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/失物招领/y8tUe7YcPqRy6e84a30cb92e615d44f465467edce340.jpg');
INSERT INTO `lostarticle` VALUES (1046122136407638016, 1, 5, 0, '测试4', '111', 0, 0, 10002, '机智的小美', 'https://cdn.uviewui.com/uview/album/1.jpg', NULL, NULL, '2022-11-26 17:55:53', '2022-11-26 17:55:53', '2022-11-26 17:55:53', 0, 0, '1', '', '');
INSERT INTO `lostarticle` VALUES (1046125180780281856, 1, 5, 0, '测试5', '111', 0, 0, 10002, '机智的小美', 'https://cdn.uviewui.com/uview/album/1.jpg', NULL, NULL, '2022-11-26 18:07:59', '2022-11-26 18:07:59', '2022-11-26 18:07:59', 0, 0, '', 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/失物招领/nTCSRCGV5h7c9e0432dfa0eda47d3bb4045c1439a73d.jpg,https://ft-glb.oss-cn-chengdu.aliyuncs.com/失物招领/jmBkFURx8XGPb0f270306a7e40b2d6fe8e450c1deda4.jpg', 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/失物招领/nTCSRCGV5h7c9e0432dfa0eda47d3bb4045c1439a73d.jpg');
INSERT INTO `lostarticle` VALUES (1046125412305862656, 1, 5, 0, '测试6', '11', 0, 0, 10002, '机智的小美', 'https://cdn.uviewui.com/uview/album/1.jpg', NULL, NULL, '2022-11-26 18:08:54', '2022-11-26 18:08:54', '2022-11-26 18:08:54', 0, 0, '1', '', '');
INSERT INTO `lostarticle` VALUES (1046125635606413312, 1, 5, 0, '测试7', '11', 0, 0, 10002, '机智的小美', 'https://cdn.uviewui.com/uview/album/1.jpg', NULL, NULL, '2022-11-26 18:09:47', '2022-11-26 18:09:47', '2022-11-26 18:09:47', 0, 0, '', 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/失物招领/dGMJXiPhBuSi6e84a30cb92e615d44f465467edce340.jpg,https://ft-glb.oss-cn-chengdu.aliyuncs.com/失物招领/PkfrNQDHf83w9e0432dfa0eda47d3bb4045c1439a73d.jpg', 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/失物招领/dGMJXiPhBuSi6e84a30cb92e615d44f465467edce340.jpg');
INSERT INTO `lostarticle` VALUES (1046180788669775872, 2, 5, 0, '捡到一个身份证', '捡到一个身份证，丢失的同学请联系我，无偿', 0, 0, 10002, '机智的小美', 'https://cdn.uviewui.com/uview/album/1.jpg', NULL, NULL, '2022-11-26 21:49:00', '2022-11-26 21:49:00', '2022-11-26 21:49:00', 0, 0, '', '', '');
INSERT INTO `lostarticle` VALUES (1046181163133042688, 2, 5, 0, '捡到一个银行卡', '今天在操场捡到一张银行卡，丢失的同学联系我，无偿', 0, 0, 10002, '机智的小美', 'https://cdn.uviewui.com/uview/album/1.jpg', NULL, NULL, '2022-11-26 21:50:26', '2022-11-26 21:50:26', '2022-11-26 21:50:26', 0, 0, '', 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/失物招领/7ipdEJ8idTRyb0f270306a7e40b2d6fe8e450c1deda4.jpg,https://ft-glb.oss-cn-chengdu.aliyuncs.com/失物招领/cAnKNMbEqiKu184b98106b8063bd14e41e6ef869aace.jpg', 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/失物招领/7ipdEJ8idTRyb0f270306a7e40b2d6fe8e450c1deda4.jpg');

-- ----------------------------
-- Table structure for lostarticletype
-- ----------------------------
DROP TABLE IF EXISTS `lostarticletype`;
CREATE TABLE `lostarticletype`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '失物招领类型表',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `state` int(0) NULL DEFAULT 0 COMMENT '类型状态，0正常，1锁定，2删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of lostarticletype
-- ----------------------------
INSERT INTO `lostarticletype` VALUES (1, '电子产品', 0);
INSERT INTO `lostarticletype` VALUES (2, '生活用品', 0);
INSERT INTO `lostarticletype` VALUES (3, '学习用品', 0);
INSERT INTO `lostarticletype` VALUES (4, '其他', 0);

-- ----------------------------
-- Table structure for moneyinfo
-- ----------------------------
DROP TABLE IF EXISTS `moneyinfo`;
CREATE TABLE `moneyinfo`  (
  `id` bigint(0) NOT NULL COMMENT '金额详细表',
  `getmoenyorderid` bigint(0) NULL DEFAULT NULL COMMENT '这笔交易所属的订单',
  `issueuser` int(0) NULL DEFAULT NULL COMMENT '该订单的发布用户',
  `continueuser` int(0) NULL DEFAULT NULL COMMENT '接单用户',
  `money` double NULL DEFAULT NULL COMMENT '交易金额',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `over_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `state` int(0) NULL DEFAULT 0 COMMENT '交易状态，0正常，1锁定，2，退回',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of moneyinfo
-- ----------------------------
INSERT INTO `moneyinfo` VALUES (1629762423034138625, 1078286265347997696, 10003, 10001, 1.88, '2023-02-26 16:36:55', '2023-02-26 16:36:55', '2023-02-26 16:36:55', 0);
INSERT INTO `moneyinfo` VALUES (1629762946739130369, 1078293080257331200, 10003, 10001, 1.88, '2023-02-26 16:39:00', '2023-02-26 16:39:00', '2023-02-26 16:39:00', 0);
INSERT INTO `moneyinfo` VALUES (1629763434054340610, 1078293508478992384, 10003, 10001, 1.88, '2023-02-26 16:40:56', '2023-02-26 16:40:56', '2023-02-26 16:40:56', 0);
INSERT INTO `moneyinfo` VALUES (1629766257148092418, 1079445385870573568, 10003, 10001, 0.94, '2023-02-26 16:52:09', '2023-02-26 16:52:09', '2023-02-26 16:52:09', 0);
INSERT INTO `moneyinfo` VALUES (1629769068229328898, 1079447211433000960, 10003, 10001, 0.94, '2023-02-26 17:03:20', '2023-02-26 17:03:20', '2023-02-26 17:03:20', 0);
INSERT INTO `moneyinfo` VALUES (1629778183433404417, 1079055475984564224, 10001, 10003, 0.94, '2023-02-26 17:39:33', '2023-02-26 17:39:33', '2023-02-26 17:39:33', 0);
INSERT INTO `moneyinfo` VALUES (1629779830305898498, 1079447729295327232, 10003, 10001, 0.94, '2023-02-26 17:46:06', '2023-02-26 17:46:06', '2023-02-26 17:46:06', 0);
INSERT INTO `moneyinfo` VALUES (1629781498519089153, 1079455602515640320, 10003, 10001, 0.94, '2023-02-26 17:52:43', '2023-02-26 17:52:43', '2023-02-26 17:52:43', 0);
INSERT INTO `moneyinfo` VALUES (1629808735729381377, 1078721791192465408, 10001, 10003, 0.94, '2023-02-26 19:40:57', '2023-02-26 19:40:57', '2023-02-26 19:40:57', 0);
INSERT INTO `moneyinfo` VALUES (1629824422069837826, 1079502691333308416, 10003, 10001, 0.94, '2023-02-26 20:43:17', '2023-02-26 20:43:17', '2023-02-26 20:43:17', 0);
INSERT INTO `moneyinfo` VALUES (1630082121873383426, 1079712416608026624, 10001, 10002, 1.69, '2023-02-27 13:47:17', '2023-02-27 13:47:17', '2023-02-27 13:47:17', 0);
INSERT INTO `moneyinfo` VALUES (1630086367549198337, 1079764118782607360, 10002, 10001, 0.94, '2023-02-27 14:04:10', '2023-02-27 14:04:10', '2023-02-27 14:04:10', 0);
INSERT INTO `moneyinfo` VALUES (1630102964556959746, 1079766516196442112, 10002, 10001, 1.88, '2023-02-27 15:10:07', '2023-02-27 15:10:07', '2023-02-27 15:10:07', 0);
INSERT INTO `moneyinfo` VALUES (1630111231324626946, 1079786852296163328, 10002, 10001, 0.94, '2023-02-27 15:42:58', '2023-02-27 15:42:58', '2023-02-27 15:42:58', 0);

-- ----------------------------
-- Table structure for need
-- ----------------------------
DROP TABLE IF EXISTS `need`;
CREATE TABLE `need`  (
  `id` bigint(0) NOT NULL COMMENT '帮我买',
  `type` int(0) NULL DEFAULT NULL COMMENT '订单类型',
  `state` int(0) NULL DEFAULT 0 COMMENT ' 0发布，1已付款，2接单中，3完成,，4问题订单',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '订单标题',
  `content` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '订单内容',
  `contacts` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '联系人名称',
  `contacts_phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '联系人电话',
  `contacts_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '联系人地址',
  `issue_user` int(0) NULL DEFAULT NULL COMMENT '发布人id',
  `issue_user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '发布用户名称',
  `issue_user_img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '发布用户头像',
  `belong_user` int(0) NULL DEFAULT NULL COMMENT '接单人id',
  `pay_type` int(0) NULL DEFAULT NULL COMMENT '支付方式，0支付宝,1微信',
  `create_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '订单创建时间',
  `update_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '订单修改时间',
  `over_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '订单完成时间',
  `hide_name` int(0) NULL DEFAULT 0 COMMENT '是否匿名0不匿名，1匿名',
  `price` int(0) NULL DEFAULT NULL COMMENT '订单价格',
  `remarks` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '订单备注',
  `order_img` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '订单图片',
  `ordertypeid` int(0) NULL DEFAULT NULL COMMENT '订单所属类型id',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `buytype`(`type`) USING BTREE,
  INDEX `buyissue_user`(`issue_user`) USING BTREE,
  INDEX `buybelong_user`(`belong_user`) USING BTREE,
  INDEX `buyorderid`(`ordertypeid`) USING BTREE,
  CONSTRAINT `need_ibfk_1` FOREIGN KEY (`belong_user`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `need_ibfk_2` FOREIGN KEY (`issue_user`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `need_ibfk_4` FOREIGN KEY (`type`) REFERENCES `needtype` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `needistype` FOREIGN KEY (`ordertypeid`) REFERENCES `orderall` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of need
-- ----------------------------
INSERT INTO `need` VALUES (1078293508478992384, 1, 4, '求租一个校园网', '求租一个校园网，15一个月有的联系我', '小李', '17628178054', '秀园3-312', 10003, '帅气的小王', 'https://cdn.uviewui.com/uview/album/3.jpg', 10001, NULL, '2023-02-23 12:33:26', '2023-02-26 16:40:56', '2023-02-23 12:33:26', 0, 1, '', '', 4);
INSERT INTO `need` VALUES (1079448056690114560, 2, 1, '拼车', '1月20号从南充回成都，还差一个，有的联系我', '小李', '17628178054', '秀园3-312', 10003, '帅气的小王', 'https://cdn.uviewui.com/uview/album/3.jpg', NULL, NULL, '2023-02-26 17:01:12', '2023-02-26 17:01:22', '2023-02-26 17:01:12', 0, 1, '男女不限', '', 4);

-- ----------------------------
-- Table structure for needtype
-- ----------------------------
DROP TABLE IF EXISTS `needtype`;
CREATE TABLE `needtype`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `state` int(0) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of needtype
-- ----------------------------
INSERT INTO `needtype` VALUES (1, '校园网', 0);
INSERT INTO `needtype` VALUES (2, '拼车类型', 0);

-- ----------------------------
-- Table structure for nongsuke
-- ----------------------------
DROP TABLE IF EXISTS `nongsuke`;
CREATE TABLE `nongsuke`  (
  `id` bigint(0) NOT NULL,
  `type` int(0) NULL DEFAULT NULL,
  `state` int(0) NULL DEFAULT 0,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `price` double(10, 2) NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
  `order_img` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
  `index_img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `issue_user` int(0) NULL DEFAULT NULL,
  `issue_user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `issue_user_img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `num` int(0) NULL DEFAULT 0,
  `pay_type` int(0) NULL DEFAULT NULL COMMENT ' 支付方式，0支付宝,1微信',
  `ordertypeid` int(0) NULL DEFAULT NULL,
  `buyusernum` int(0) NULL DEFAULT NULL COMMENT '购买人数',
  `norms` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '商品规格描述',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `over_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `type`(`type`) USING BTREE,
  INDEX `ordertypeid`(`ordertypeid`) USING BTREE,
  INDEX `issue_user`(`issue_user`) USING BTREE,
  CONSTRAINT `nongsuke_ibfk_1` FOREIGN KEY (`type`) REFERENCES `nongsuketype` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `nongsuke_ibfk_2` FOREIGN KEY (`ordertypeid`) REFERENCES `orderall` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `nongsuke_ibfk_3` FOREIGN KEY (`issue_user`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of nongsuke
-- ----------------------------
INSERT INTO `nongsuke` VALUES (1080779763661930496, 1, 1, '测试', 0.50, '1', 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/助农服务/DGQ8qAHzJ6rM8dfef6d6ba7253204b23af36f1a7d8b9.jpg,https://ft-glb.oss-cn-chengdu.aliyuncs.com/助农服务/G8ygJ6l6Ls0cb0f270306a7e40b2d6fe8e450c1deda4.jpg', 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/助农服务/DGQ8qAHzJ6rM8dfef6d6ba7253204b23af36f1a7d8b9.jpg', 10001, '宇宙最帅的那个人', 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/用户头像/fyH0eYwCiGwM62b824bb48d92e588d5cd16d6e151594.jpg', 199, NULL, 8, 0, '1', '2023-03-02 09:12:55', '2023-03-02 09:12:55', '2023-03-02 09:12:55');
INSERT INTO `nongsuke` VALUES (1080783092924284928, 3, 1, '峨眉山猕猴桃', 0.60, '猕猴桃，猕猴桃，峨眉山的猕猴桃，香甜好吃的猕猴桃', 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/助农服务/lg4FMCD1c30h62b824bb48d92e588d5cd16d6e151594.jpg,https://ft-glb.oss-cn-chengdu.aliyuncs.com/助农服务/YegCbWvXpDq394689b3b91a48488cd13e18b19c9ef47.jpg', 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/助农服务/lg4FMCD1c30h62b824bb48d92e588d5cd16d6e151594.jpg', 10001, '宇宙最帅的那个人', 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/用户头像/fyH0eYwCiGwM62b824bb48d92e588d5cd16d6e151594.jpg', 99, NULL, 8, 0, '10个一袋', '2023-03-02 09:26:09', '2023-03-02 09:26:09', '2023-03-02 09:26:09');

-- ----------------------------
-- Table structure for nongsukeallid
-- ----------------------------
DROP TABLE IF EXISTS `nongsukeallid`;
CREATE TABLE `nongsukeallid`  (
  `id` bigint(0) NOT NULL COMMENT '助农服务总订单表',
  `issueid` int(0) NULL DEFAULT NULL COMMENT '下单用户id',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '订单标题',
  `orderlist` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '订单内容id集合',
  `buyname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '联系人',
  `buyphone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '联系人电话',
  `buyaddress` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '联系人地址',
  `price` double(10, 2) NULL DEFAULT NULL COMMENT '订单价格',
  `state` int(0) NULL DEFAULT 0 COMMENT '订单状态，0未付款，1已付款，2已发货，3已完成,4问题订单，5已退款',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `over_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '完成时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of nongsukeallid
-- ----------------------------
INSERT INTO `nongsukeallid` VALUES (1080785519664693248, 10001, '助农服务商品订单:1080785519664693248', '[{\"belongid\":\"1080785519664693248\",\"buynum\":1,\"content\":\"1\",\"goodprice\":0.5,\"goodsid\":\"1080779763661930496\",\"goodtype\":1,\"id\":1631105993183731713,\"imglist\":\"https://ft-glb.oss-cn-chengdu.aliyuncs.com/助农服务/DGQ8qAHzJ6rM8dfef6d6ba7253204b23af36f1a7d8b9.jpg,https://ft-glb.oss-cn-chengdu.aliyuncs.com/助农服务/G8ygJ6l6Ls0cb0f270306a7e40b2d6fe8e450c1deda4.jpg\",\"indeximg\":\"https://ft-glb.oss-cn-chengdu.aliyuncs.com/助农服务/DGQ8qAHzJ6rM8dfef6d6ba7253204b23af36f1a7d8b9.jpg\",\"issuseuserid\":10001,\"state\":0,\"title\":\"测试\",\"totalprice\":0.5},{\"belongid\":\"1080785519664693248\",\"buynum\":1,\"content\":\"猕猴桃，猕猴桃，峨眉山的猕猴桃，香甜好吃的猕猴桃\",\"goodprice\":0.6,\"goodsid\":\"1080783092924284928\",\"goodtype\":3,\"id\":1631105993183731714,\"imglist\":\"https://ft-glb.oss-cn-chengdu.aliyuncs.com/助农服务/lg4FMCD1c30h62b824bb48d92e588d5cd16d6e151594.jpg,https://ft-glb.oss-cn-chengdu.aliyuncs.com/助农服务/YegCbWvXpDq394689b3b91a48488cd13e18b19c9ef47.jpg\",\"indeximg\":\"https://ft-glb.oss-cn-chengdu.aliyuncs.com/助农服务/lg4FMCD1c30h62b824bb48d92e588d5cd16d6e151594.jpg\",\"issuseuserid\":10001,\"state\":0,\"title\":\"峨眉山猕猴桃\",\"totalprice\":0.6}]', '小王', '17765519698', '俊园3-311', 1.10, 5, '2023-03-02 22:13:27', '2023-03-02 22:13:27', '2023-03-02 22:13:27');
INSERT INTO `nongsukeallid` VALUES (1080786582740402176, 10001, '助农服务商品订单:1080786582740402176', '[{\"belongid\":\"1080786582740402176\",\"buynum\":1,\"content\":\"1\",\"goodprice\":0.5,\"goodsid\":\"1080779763661930496\",\"goodtype\":1,\"id\":1631107056217497602,\"imglist\":\"https://ft-glb.oss-cn-chengdu.aliyuncs.com/助农服务/DGQ8qAHzJ6rM8dfef6d6ba7253204b23af36f1a7d8b9.jpg,https://ft-glb.oss-cn-chengdu.aliyuncs.com/助农服务/G8ygJ6l6Ls0cb0f270306a7e40b2d6fe8e450c1deda4.jpg\",\"indeximg\":\"https://ft-glb.oss-cn-chengdu.aliyuncs.com/助农服务/DGQ8qAHzJ6rM8dfef6d6ba7253204b23af36f1a7d8b9.jpg\",\"issuseuserid\":10001,\"state\":0,\"title\":\"测试\",\"totalprice\":0.5},{\"belongid\":\"1080786582740402176\",\"buynum\":1,\"content\":\"猕猴桃，猕猴桃，峨眉山的猕猴桃，香甜好吃的猕猴桃\",\"goodprice\":0.6,\"goodsid\":\"1080783092924284928\",\"goodtype\":3,\"id\":1631107056267829249,\"imglist\":\"https://ft-glb.oss-cn-chengdu.aliyuncs.com/助农服务/lg4FMCD1c30h62b824bb48d92e588d5cd16d6e151594.jpg,https://ft-glb.oss-cn-chengdu.aliyuncs.com/助农服务/YegCbWvXpDq394689b3b91a48488cd13e18b19c9ef47.jpg\",\"indeximg\":\"https://ft-glb.oss-cn-chengdu.aliyuncs.com/助农服务/lg4FMCD1c30h62b824bb48d92e588d5cd16d6e151594.jpg\",\"issuseuserid\":10001,\"state\":0,\"title\":\"峨眉山猕猴桃\",\"totalprice\":0.6}]', '小范', '13559899789', '测试', 1.10, 5, '2023-03-02 21:51:55', '2023-03-02 21:51:55', '2023-03-02 21:51:55');
INSERT INTO `nongsukeallid` VALUES (1080868975069364224, 10001, '助农服务商品订单:1080868975069364224', '[{\"belongid\":\"1080868975069364224\",\"buynum\":0,\"content\":\"猕猴桃，猕猴桃，峨眉山的猕猴桃，香甜好吃的猕猴桃\",\"goodprice\":0.6,\"goodsid\":\"1080783092924284928\",\"goodtype\":3,\"id\":1631189449825722369,\"imglist\":\"https://ft-glb.oss-cn-chengdu.aliyuncs.com/助农服务/lg4FMCD1c30h62b824bb48d92e588d5cd16d6e151594.jpg,https://ft-glb.oss-cn-chengdu.aliyuncs.com/助农服务/YegCbWvXpDq394689b3b91a48488cd13e18b19c9ef47.jpg\",\"indeximg\":\"https://ft-glb.oss-cn-chengdu.aliyuncs.com/助农服务/lg4FMCD1c30h62b824bb48d92e588d5cd16d6e151594.jpg\",\"issuseuserid\":10001,\"state\":0,\"title\":\"峨眉山猕猴桃\",\"totalprice\":0.6}]', '王五', '17765519698', '311', 0.60, 0, '2023-03-02 15:07:26', '2023-03-02 15:07:26', '2023-03-02 15:07:26');

-- ----------------------------
-- Table structure for nongsukeordercontent
-- ----------------------------
DROP TABLE IF EXISTS `nongsukeordercontent`;
CREATE TABLE `nongsukeordercontent`  (
  `id` bigint(0) NOT NULL COMMENT '助农服务订单表',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '订单标题',
  `goodsid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '商品id',
  `goodprice` double(10, 2) NULL DEFAULT NULL COMMENT '下单时商品单价',
  `goodtype` int(0) NULL DEFAULT NULL COMMENT '商品的分类',
  `buynum` int(0) NULL DEFAULT NULL COMMENT '购买的商品数量',
  `totalprice` double(10, 2) NULL DEFAULT NULL COMMENT '订单总价',
  `state` int(0) NULL DEFAULT 0 COMMENT '订单状态，0,未付款,1,已付款，2已发货，3完成，4问题订单，5已退款',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `over_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '完成时间',
  `issuseuserid` int(0) NULL DEFAULT NULL COMMENT '创建订单用户id',
  `imglist` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '订单图片',
  `indeximg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '展示图片',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '订单内容',
  `belongid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '所属总订单',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `goodsid`(`goodsid`) USING BTREE,
  INDEX `issuseuserid`(`issuseuserid`) USING BTREE,
  CONSTRAINT `nongsukeordercontent_ibfk_2` FOREIGN KEY (`issuseuserid`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of nongsukeordercontent
-- ----------------------------
INSERT INTO `nongsukeordercontent` VALUES (1631105993183731713, '测试', '1080779763661930496', 0.50, 1, 1, 0.50, 0, '2023-03-02 09:35:47', '2023-03-02 09:35:47', '2023-03-02 09:35:47', 10001, 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/助农服务/DGQ8qAHzJ6rM8dfef6d6ba7253204b23af36f1a7d8b9.jpg,https://ft-glb.oss-cn-chengdu.aliyuncs.com/助农服务/G8ygJ6l6Ls0cb0f270306a7e40b2d6fe8e450c1deda4.jpg', 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/助农服务/DGQ8qAHzJ6rM8dfef6d6ba7253204b23af36f1a7d8b9.jpg', '1', '1080785519664693248');
INSERT INTO `nongsukeordercontent` VALUES (1631105993183731714, '峨眉山猕猴桃', '1080783092924284928', 0.60, 3, 1, 0.60, 0, '2023-03-02 09:35:47', '2023-03-02 09:35:47', '2023-03-02 09:35:47', 10001, 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/助农服务/lg4FMCD1c30h62b824bb48d92e588d5cd16d6e151594.jpg,https://ft-glb.oss-cn-chengdu.aliyuncs.com/助农服务/YegCbWvXpDq394689b3b91a48488cd13e18b19c9ef47.jpg', 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/助农服务/lg4FMCD1c30h62b824bb48d92e588d5cd16d6e151594.jpg', '猕猴桃，猕猴桃，峨眉山的猕猴桃，香甜好吃的猕猴桃', '1080785519664693248');
INSERT INTO `nongsukeordercontent` VALUES (1631107056217497602, '测试', '1080779763661930496', 0.50, 1, 1, 0.50, 0, '2023-03-02 09:40:01', '2023-03-02 09:40:01', '2023-03-02 09:40:01', 10001, 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/助农服务/DGQ8qAHzJ6rM8dfef6d6ba7253204b23af36f1a7d8b9.jpg,https://ft-glb.oss-cn-chengdu.aliyuncs.com/助农服务/G8ygJ6l6Ls0cb0f270306a7e40b2d6fe8e450c1deda4.jpg', 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/助农服务/DGQ8qAHzJ6rM8dfef6d6ba7253204b23af36f1a7d8b9.jpg', '1', '1080786582740402176');
INSERT INTO `nongsukeordercontent` VALUES (1631107056267829249, '峨眉山猕猴桃', '1080783092924284928', 0.60, 3, 1, 0.60, 0, '2023-03-02 09:40:01', '2023-03-02 09:40:01', '2023-03-02 09:40:01', 10001, 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/助农服务/lg4FMCD1c30h62b824bb48d92e588d5cd16d6e151594.jpg,https://ft-glb.oss-cn-chengdu.aliyuncs.com/助农服务/YegCbWvXpDq394689b3b91a48488cd13e18b19c9ef47.jpg', 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/助农服务/lg4FMCD1c30h62b824bb48d92e588d5cd16d6e151594.jpg', '猕猴桃，猕猴桃，峨眉山的猕猴桃，香甜好吃的猕猴桃', '1080786582740402176');
INSERT INTO `nongsukeordercontent` VALUES (1631189449825722369, '峨眉山猕猴桃', '1080783092924284928', 0.60, 3, 0, 0.60, 0, '2023-03-02 15:07:25', '2023-03-02 15:07:25', '2023-03-02 15:07:25', 10001, 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/助农服务/lg4FMCD1c30h62b824bb48d92e588d5cd16d6e151594.jpg,https://ft-glb.oss-cn-chengdu.aliyuncs.com/助农服务/YegCbWvXpDq394689b3b91a48488cd13e18b19c9ef47.jpg', 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/助农服务/lg4FMCD1c30h62b824bb48d92e588d5cd16d6e151594.jpg', '猕猴桃，猕猴桃，峨眉山的猕猴桃，香甜好吃的猕猴桃', '1080868975069364224');

-- ----------------------------
-- Table structure for nongsuketype
-- ----------------------------
DROP TABLE IF EXISTS `nongsuketype`;
CREATE TABLE `nongsuketype`  (
  `id` int(0) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `state` int(0) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of nongsuketype
-- ----------------------------
INSERT INTO `nongsuketype` VALUES (1, '禽畜肉类', 0);
INSERT INTO `nongsuketype` VALUES (2, '粮油米面', 0);
INSERT INTO `nongsuketype` VALUES (3, '水果', 0);
INSERT INTO `nongsuketype` VALUES (4, '蔬菜', 0);
INSERT INTO `nongsuketype` VALUES (5, '水产品', 0);
INSERT INTO `nongsuketype` VALUES (6, '其他', 0);

-- ----------------------------
-- Table structure for note
-- ----------------------------
DROP TABLE IF EXISTS `note`;
CREATE TABLE `note`  (
  `id` bigint(0) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `issue_userid` int(0) NULL DEFAULT NULL,
  `issue_username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `issue_userimg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `belong_userid` int(0) NULL DEFAULT NULL,
  `belong_username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `belong_userimg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `sex` int(0) NULL DEFAULT NULL,
  `state` int(0) NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `over_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `issue_userid`(`issue_userid`) USING BTREE,
  INDEX `belong_userid`(`belong_userid`) USING BTREE,
  CONSTRAINT `note_ibfk_1` FOREIGN KEY (`issue_userid`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `note_ibfk_2` FOREIGN KEY (`belong_userid`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of note
-- ----------------------------
INSERT INTO `note` VALUES (1075394615634296832, '测试', '1111', 10001, '宇宙最帅的那个人', '\r\nhttps://ft-glb.oss-cn-chengdu.aliyuncs.com/user-img/%E7%94%A8%E6%88%B7.jpg', NULL, NULL, NULL, 0, 0, '2023-02-15 12:34:16', '2023-02-15 12:34:16', '2023-02-15 12:34:16');
INSERT INTO `note` VALUES (1075394760396505088, '测试2', '测试内容2', 10001, '宇宙最帅的那个人', '\r\nhttps://ft-glb.oss-cn-chengdu.aliyuncs.com/user-img/%E7%94%A8%E6%88%B7.jpg', NULL, NULL, NULL, 0, 0, '2023-02-15 12:34:50', '2023-02-15 12:34:50', '2023-02-15 12:34:50');

-- ----------------------------
-- Table structure for orderall
-- ----------------------------
DROP TABLE IF EXISTS `orderall`;
CREATE TABLE `orderall`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `state` int(0) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of orderall
-- ----------------------------
INSERT INTO `orderall` VALUES (1, '帮我买', 0);
INSERT INTO `orderall` VALUES (2, '帮我送', 0);
INSERT INTO `orderall` VALUES (3, '帮我做', 0);
INSERT INTO `orderall` VALUES (4, '需求订单', 0);
INSERT INTO `orderall` VALUES (5, '失物招领', 0);
INSERT INTO `orderall` VALUES (6, '寻物启事', 0);
INSERT INTO `orderall` VALUES (7, '跳蚤市场', 0);
INSERT INTO `orderall` VALUES (8, '助农服务', 0);
INSERT INTO `orderall` VALUES (9, '学习中心', 0);
INSERT INTO `orderall` VALUES (10, '交友中心', 0);

-- ----------------------------
-- Table structure for orderstr
-- ----------------------------
DROP TABLE IF EXISTS `orderstr`;
CREATE TABLE `orderstr`  (
  `id` bigint(0) NOT NULL COMMENT '订单表支付id表，用于退款查询',
  `belongorderid` bigint(0) NULL DEFAULT NULL COMMENT '所属的订单id',
  `tkstrorderid` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '封装后的的订单加随机时间戳的字符串，也是退款id',
  `state` int(0) NULL DEFAULT 0 COMMENT '状态',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `over_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of orderstr
-- ----------------------------
INSERT INTO `orderstr` VALUES (1630108794790522881, 1079786852296163328, '1079786852296163328XLV4PiViezHC', 0, '2023-02-27 15:42:05', '2023-02-27 15:42:05', '2023-02-27 15:42:05');
INSERT INTO `orderstr` VALUES (1630414459228536833, 1080093738362994688, '1080093738362994688PvnwFopmE0WE', 0, '2023-02-28 11:47:53', '2023-02-28 11:47:53', '2023-02-28 11:47:53');
INSERT INTO `orderstr` VALUES (1630425446967042050, 1080104144166977536, '1080104144166977536MSdpV8k0f9Lo', 0, '2023-02-28 12:31:33', '2023-02-28 12:31:33', '2023-02-28 12:31:33');
INSERT INTO `orderstr` VALUES (1630885401260081154, 1080564900394172416, '1080564900394172416IbPygqa3KXaA', 0, '2023-03-01 18:59:15', '2023-03-01 18:59:15', '2023-03-01 18:59:15');
INSERT INTO `orderstr` VALUES (1630886052631298049, 1080565563224227840, '1080565563224227840kg6PDI5DqmkF', 0, '2023-03-01 19:01:50', '2023-03-01 19:01:50', '2023-03-01 19:01:50');
INSERT INTO `orderstr` VALUES (1630886935339405313, 1080566438923599872, '10805664389235998728pN0wjZFImEe', 0, '2023-03-01 19:05:20', '2023-03-01 19:05:20', '2023-03-01 19:05:20');
INSERT INTO `orderstr` VALUES (1630913753719181314, 1080593261908721664, '10805932619087216644T8rBHLCnMeP', 0, '2023-03-01 20:51:54', '2023-03-01 20:51:54', '2023-03-01 20:51:54');
INSERT INTO `orderstr` VALUES (1630941323877617665, 1080620780280086528, '1080620780280086528KywjAYo9RqwY', 0, '2023-03-01 22:41:27', '2023-03-01 22:41:27', '2023-03-01 22:41:27');
INSERT INTO `orderstr` VALUES (1631094438559883265, 1080773938692227072, '108077393869222707272SL1fu8Wu0T', 0, '2023-03-02 08:49:53', '2023-03-02 08:49:53', '2023-03-02 08:49:53');
INSERT INTO `orderstr` VALUES (1631095455087206401, 1080774828484460544, '10807748284844605449MW72Vg8Z8oJ', 0, '2023-03-02 08:53:55', '2023-03-02 08:53:55', '2023-03-02 08:53:55');
INSERT INTO `orderstr` VALUES (1631095830032818178, 1080775328953008128, '1080775328953008128mGrGnvq8mATL', 0, '2023-03-02 08:55:24', '2023-03-02 08:55:24', '2023-03-02 08:55:24');
INSERT INTO `orderstr` VALUES (1631096095075082242, 1080775543755898880, '1080775543755898880IiZIHbSEgkhA', 0, '2023-03-02 08:56:28', '2023-03-02 08:56:28', '2023-03-02 08:56:28');
INSERT INTO `orderstr` VALUES (1631106028822732802, 1080785519664693248, '1080785519664693248U7rkbkm5rccV', 0, '2023-03-02 09:35:56', '2023-03-02 09:35:56', '2023-03-02 09:35:56');
INSERT INTO `orderstr` VALUES (1631107391103311873, 1080786582740402176, '1080786582740402176jiSXMH8yjXun', 0, '2023-03-02 09:41:21', '2023-03-02 09:41:21', '2023-03-02 09:41:21');

-- ----------------------------
-- Table structure for post
-- ----------------------------
DROP TABLE IF EXISTS `post`;
CREATE TABLE `post`  (
  `id` bigint(0) NOT NULL COMMENT '帖子表',
  `type` int(0) NULL DEFAULT NULL COMMENT '类型',
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '帖子内容',
  `issue_user` int(0) NULL DEFAULT NULL COMMENT '发布人id',
  `issue_user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '发布用户名称',
  `issue_user_img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '发布用户头像',
  `praisenum` int(0) NULL DEFAULT NULL COMMENT '帖子点赞数',
  `commentnum` int(0) NULL DEFAULT NULL COMMENT '帖子评论数',
  `sharenum` int(0) NULL DEFAULT NULL COMMENT '帖子分享数',
  `location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '定位',
  `praise_useridlist` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '点赞用户集合',
  `commentidlist` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '一级评论回复集合',
  `post_imglist` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '帖子图片集合',
  `post_indeximg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '帖子首页图片',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `over_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `state` int(0) NULL DEFAULT 0 COMMENT '帖子状态',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `type`(`type`) USING BTREE,
  INDEX `issue_user`(`issue_user`) USING BTREE,
  CONSTRAINT `post_ibfk_1` FOREIGN KEY (`type`) REFERENCES `posttype` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `post_ibfk_2` FOREIGN KEY (`issue_user`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of post
-- ----------------------------
INSERT INTO `post` VALUES (1075371406499250176, 1, '1111', 10001, '宇宙最帅的那个人', '\r\nhttps://ft-glb.oss-cn-chengdu.aliyuncs.com/user-img/%E7%94%A8%E6%88%B7.jpg', 0, 1, 0, '武侯区人民政府(武侯祠大街南)', '', '1625694055394095106,', 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/校园论坛/fYgK8V71eQuTd2ac734c9622a938b91219431142468a.png', '[\"https://ft-glb.oss-cn-chengdu.aliyuncs.com/校园论坛/fYgK8V71eQuTd2ac734c9622a938b91219431142468a.png\"]', '2023-02-15 11:12:03', '2023-02-15 11:12:03', '2023-02-15 11:12:03', 0);

-- ----------------------------
-- Table structure for posttype
-- ----------------------------
DROP TABLE IF EXISTS `posttype`;
CREATE TABLE `posttype`  (
  `id` int(0) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `state` int(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of posttype
-- ----------------------------
INSERT INTO `posttype` VALUES (1, '生活动态', 0);
INSERT INTO `posttype` VALUES (2, '咨询求助', 0);
INSERT INTO `posttype` VALUES (3, '校园活动', 0);
INSERT INTO `posttype` VALUES (4, '好人好事', 0);
INSERT INTO `posttype` VALUES (5, '黑榜', 0);
INSERT INTO `posttype` VALUES (6, '其他', 0);

-- ----------------------------
-- Table structure for seekarticle
-- ----------------------------
DROP TABLE IF EXISTS `seekarticle`;
CREATE TABLE `seekarticle`  (
  `id` bigint(0) NOT NULL COMMENT '寻物启事订单',
  `type` int(0) NULL DEFAULT NULL COMMENT '订单类型',
  `ordertypeid` int(0) NULL DEFAULT NULL COMMENT '订单所属类型id',
  `state` int(0) NULL DEFAULT 0 COMMENT ' 0发布，1已付款，2认领中，3完成,，4问题订单',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '订单标题',
  `content` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '订单内容',
  `weizhi` int(0) NULL DEFAULT 0 COMMENT '物品当前位置',
  `paid` int(0) NULL DEFAULT 0 COMMENT '是否有偿，0无偿，1有偿',
  `issue_user` int(0) NULL DEFAULT NULL COMMENT '发布人id',
  `issue_user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '发布用户名称',
  `issue_user_img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '发布用户头像',
  `belong_user` int(0) NULL DEFAULT NULL COMMENT '接单人id',
  `pay_type` int(0) NULL DEFAULT NULL COMMENT '支付方式，0支付宝,1微信',
  `create_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '订单创建时间',
  `update_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '订单修改时间',
  `over_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '订单完成时间',
  `hide_name` int(0) NULL DEFAULT 0 COMMENT '是否匿名0不匿名，1匿名',
  `price` double(2, 0) NULL DEFAULT 0 COMMENT '订单价格',
  `remarks` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '订单备注',
  `order_img` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '订单图片',
  `index_img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '订单首页图片',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `buytype`(`type`) USING BTREE,
  INDEX `buyissue_user`(`issue_user`) USING BTREE,
  INDEX `buybelong_user`(`belong_user`) USING BTREE,
  INDEX `buyorderid`(`ordertypeid`) USING BTREE,
  CONSTRAINT `seekarticle_ibfk_1` FOREIGN KEY (`belong_user`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `seekarticle_ibfk_2` FOREIGN KEY (`issue_user`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `seekarticle_ibfk_4` FOREIGN KEY (`ordertypeid`) REFERENCES `orderall` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `seekarticle_type` FOREIGN KEY (`type`) REFERENCES `lostarticletype` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of seekarticle
-- ----------------------------
INSERT INTO `seekarticle` VALUES (1046093423712403456, 2, 6, 1, '掉了一个钱包', '今天上课的时候在明德楼掉了一个钱包，捡到的同学请联系下我，有偿', 0, 1, 10002, '机智的小美', 'https://cdn.uviewui.com/uview/album/1.jpg', NULL, NULL, '2022-11-26 16:01:48', '2022-11-26 16:02:14', '2022-11-26 16:01:48', 0, 1, '', 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/寻物启事/KO8IcBvRuBNNb0f270306a7e40b2d6fe8e450c1deda4.jpg,https://ft-glb.oss-cn-chengdu.aliyuncs.com/寻物启事/u02jc2oVlDbq184b98106b8063bd14e41e6ef869aace.jpg', NULL);
INSERT INTO `seekarticle` VALUES (1046205464909447168, 1, 6, 1, '掉了一个耳机', '掉了一个耳机捡到的同学请联系我，有偿', 0, 1, 10001, '宇宙最帅的那个人', '\r\nhttps://ft-glb.oss-cn-chengdu.aliyuncs.com/user-img/%E7%94%A8%E6%88%B7.jpg', NULL, NULL, '2022-11-26 23:27:01', '2022-11-26 23:27:25', '2022-11-26 23:27:01', 0, 1, '', 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/寻物启事/ordYHCCKFBKN6e84a30cb92e615d44f465467edce340.jpg,https://ft-glb.oss-cn-chengdu.aliyuncs.com/寻物启事/hdcFwC1X26B69e0432dfa0eda47d3bb4045c1439a73d.jpg', 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/寻物启事/ordYHCCKFBKN6e84a30cb92e615d44f465467edce340.jpg');
INSERT INTO `seekarticle` VALUES (1046206331108720640, 1, 6, 0, '掉了一个充电宝', '今天在明德楼上课的时候掉了一个充电宝，请捡到的同学联系下我，谢谢', 0, 0, 10001, '宇宙最帅的那个人', '\r\nhttps://ft-glb.oss-cn-chengdu.aliyuncs.com/user-img/%E7%94%A8%E6%88%B7.jpg', NULL, NULL, '2022-11-26 23:30:28', '2022-11-26 23:30:28', '2022-11-26 23:30:28', 1, 0, '', '', '');

-- ----------------------------
-- Table structure for sendorder
-- ----------------------------
DROP TABLE IF EXISTS `sendorder`;
CREATE TABLE `sendorder`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT,
  `type` int(0) NULL DEFAULT NULL COMMENT '订单类型',
  `state` int(0) NULL DEFAULT NULL COMMENT ' 0发布，1已付款，2接单中，3完成,，4问题订单',
  `content` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '订单内容',
  `contacts` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '联系人名称',
  `contacts_phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '联系人电话',
  `contacts_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '联系人地址',
  `issue_user` int(0) NULL DEFAULT NULL COMMENT '发布人id',
  `issue_user_img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '发布用户头像',
  `belong_user` int(0) NULL DEFAULT NULL COMMENT '接单人id',
  `hide_name` int(0) NULL DEFAULT 0 COMMENT '是否匿名0不匿名，1匿名',
  `pay_type` int(0) NULL DEFAULT NULL COMMENT '支付方式，0支付宝,1微信',
  `create_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '订单创建时间',
  `update_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '订单修改时间',
  `over_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '订单完成时间',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '订单标题',
  `price` int(0) NULL DEFAULT NULL COMMENT '订单价格',
  `order_img` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '订单图片',
  `remarks` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '订单备注',
  `ordertypeid` int(0) NULL DEFAULT NULL COMMENT '订单所属类型id',
  `issue_user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '发布用户名称',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `sendtype`(`type`) USING BTREE,
  INDEX `sendissue_user`(`issue_user`) USING BTREE,
  INDEX `sendbelong_user`(`belong_user`) USING BTREE,
  INDEX `sendistype`(`ordertypeid`) USING BTREE,
  CONSTRAINT `sendbelong_user` FOREIGN KEY (`belong_user`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `sendissue_user` FOREIGN KEY (`issue_user`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `sendistype` FOREIGN KEY (`ordertypeid`) REFERENCES `orderall` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `sendtype` FOREIGN KEY (`type`) REFERENCES `sendtype` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1079447491901915137 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sendorder
-- ----------------------------
INSERT INTO `sendorder` VALUES (1078286265347997696, 1, 4, '帮我送一个文件到明德楼5-501', '小王', '17628178054', '俊3-311', 10003, 'https://cdn.uviewui.com/uview/album/3.jpg', 10001, 0, NULL, '2023-02-23 12:04:41', '2023-02-26 16:36:55', '2023-02-23 12:04:41', '帮我送一个申请表', 1, 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/帮我送/文件/NuKPhjfiPkBwb0f270306a7e40b2d6fe8e450c1deda4.jpg,https://ft-glb.oss-cn-chengdu.aliyuncs.com/帮我送/文件/FLhNiLdI4xsW184b98106b8063bd14e41e6ef869aace.jpg', '越快越好，不重，两张纸', 2, '帅气的小王');
INSERT INTO `sendorder` VALUES (1078292607471190016, 2, 4, '室友忘记带钥匙了，需要一个帅哥来明德楼拿下钥匙，送到秀园3-12', '小李', '17628178054', '秀园3-312', 10003, 'https://cdn.uviewui.com/uview/album/3.jpg', 10001, 0, NULL, '2023-02-23 12:29:53', '2023-02-26 15:02:31', '2023-02-23 12:29:53', '帮我送一串钥匙', 1, 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/帮我送/钥匙/5bgCLuhc2RPAb0f270306a7e40b2d6fe8e450c1deda4.jpg,https://ft-glb.oss-cn-chengdu.aliyuncs.com/帮我送/钥匙/sbyTzZBHbiR0184b98106b8063bd14e41e6ef869aace.jpg', '女最好生', 2, '帅气的小王');
INSERT INTO `sendorder` VALUES (1079445385870573568, 3, 4, '帮我送一朵花给操场上跳舞的那个女生', '小李', '17628178054', '秀园3-312', 10003, 'https://cdn.uviewui.com/uview/album/3.jpg', 10001, 0, NULL, '2023-02-26 16:50:37', '2023-02-26 16:52:09', '2023-02-26 16:50:37', '帮我送花', 1, '', '最好是女生送', 2, '帅气的小王');
INSERT INTO `sendorder` VALUES (1079447491901915136, 5, 1, '帮我送本书到明德楼505', '小李', '17628178054', '秀园3-312', 10003, 'https://cdn.uviewui.com/uview/album/3.jpg', NULL, 0, NULL, '2023-02-26 16:58:59', '2023-03-02 13:01:30', '2023-02-26 16:58:59', '帮我送本书', 1, '', '越快越好', 2, '帅气的小王');

-- ----------------------------
-- Table structure for sendtype
-- ----------------------------
DROP TABLE IF EXISTS `sendtype`;
CREATE TABLE `sendtype`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `state` int(0) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sendtype
-- ----------------------------
INSERT INTO `sendtype` VALUES (1, '文件', 0);
INSERT INTO `sendtype` VALUES (2, '钥匙', 0);
INSERT INTO `sendtype` VALUES (3, '花', 0);
INSERT INTO `sendtype` VALUES (4, '蛋糕', 0);
INSERT INTO `sendtype` VALUES (5, '物品', 0);
INSERT INTO `sendtype` VALUES (6, '其他', 0);

-- ----------------------------
-- Table structure for study
-- ----------------------------
DROP TABLE IF EXISTS `study`;
CREATE TABLE `study`  (
  `id` bigint(0) NOT NULL,
  `type` int(0) NULL DEFAULT NULL,
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `issue_user` int(0) NULL DEFAULT NULL,
  `issue_user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `issue_user_img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `praisenum` int(0) NULL DEFAULT NULL,
  `commentnum` int(0) NULL DEFAULT NULL,
  `sharenum` int(0) NULL DEFAULT NULL,
  `location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `praise_useridlist` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `commentidlist` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `post_imglist` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `post_indeximg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `state` int(0) NULL DEFAULT 0,
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `over_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `type`(`type`) USING BTREE,
  INDEX `issue_user`(`issue_user`) USING BTREE,
  CONSTRAINT `study_ibfk_1` FOREIGN KEY (`type`) REFERENCES `studytype` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `study_ibfk_2` FOREIGN KEY (`issue_user`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of study
-- ----------------------------
INSERT INTO `study` VALUES (1078447807481774080, 1, '关于商品购买的流程分享', 10003, '帅气的小王', 'https://cdn.uviewui.com/uview/album/3.jpg', 1, 1, 0, '新北社区4期新乐中街2号院', '10003,', '1628768392749645825,', 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/学习中心/tmp_ff5eab32765685ab9e19ca8a14c355d151e8bb6b6f7572b5.jpg,https://ft-glb.oss-cn-chengdu.aliyuncs.com/学习中心/tmp_ffb0d5cc5cfbb598b6a58eefa62022a6d39a13c3763c745e.jpg', 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/学习中心/tmp_ff5eab32765685ab9e19ca8a14c355d151e8bb6b6f7572b5.jpg', 0, '2023-02-23 22:47:00', '2023-02-23 22:47:00', '2023-02-23 22:47:00');

-- ----------------------------
-- Table structure for studyanswer
-- ----------------------------
DROP TABLE IF EXISTS `studyanswer`;
CREATE TABLE `studyanswer`  (
  `id` bigint(0) NOT NULL COMMENT '回复表',
  `answer_content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '回复类容',
  `answer_userid` int(0) NULL DEFAULT NULL COMMENT '发布该回复的用户id',
  `answer_username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '发布用户名称',
  `belongtofistcomment` bigint(0) NULL DEFAULT NULL COMMENT '所属上级评论id',
  `answer_touserid` int(0) NULL DEFAULT NULL COMMENT '回复的用户id',
  `answer_tousername` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '回复的用户名',
  `state` int(0) NULL DEFAULT 0 COMMENT '回复状态',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `over_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `answer_userid`(`answer_userid`) USING BTREE,
  INDEX `answer_touserid`(`answer_touserid`) USING BTREE,
  INDEX `belongtofistcomment`(`belongtofistcomment`) USING BTREE,
  CONSTRAINT `studyanswer_ibfk_1` FOREIGN KEY (`answer_userid`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `studyanswer_ibfk_2` FOREIGN KEY (`answer_touserid`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `studyanswer_ibfk_3` FOREIGN KEY (`belongtofistcomment`) REFERENCES `studyfistcomment` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of studyanswer
-- ----------------------------

-- ----------------------------
-- Table structure for studyfistcomment
-- ----------------------------
DROP TABLE IF EXISTS `studyfistcomment`;
CREATE TABLE `studyfistcomment`  (
  `id` bigint(0) NOT NULL COMMENT '评论表',
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '评论内容',
  `comment_userid` int(0) NULL DEFAULT NULL COMMENT '发布该回复的用户id',
  `comment_username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '发布用户名称',
  `comment_userimg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '发布用户头像',
  `answer_idlist` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '回复给评论的回复id集合',
  `belongpost` bigint(0) NULL DEFAULT NULL COMMENT '所属上级帖子id',
  `state` int(0) NULL DEFAULT 0 COMMENT '回复状态',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `over_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `answer_userid`(`comment_userid`) USING BTREE,
  INDEX `belongpost`(`belongpost`) USING BTREE,
  CONSTRAINT `studyfistcomment_ibfk_1` FOREIGN KEY (`comment_userid`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `studyfistcomment_ibfk_2` FOREIGN KEY (`belongpost`) REFERENCES `study` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of studyfistcomment
-- ----------------------------
INSERT INTO `studyfistcomment` VALUES (1628768392749645825, '不要沉贴，顶顶顶', 10003, '帅气的小王', 'https://cdn.uviewui.com/uview/album/3.jpg', '', 1078447807481774080, 0, '2023-02-23 22:47:00', '2023-02-23 22:47:00', '2023-02-23 22:47:00');

-- ----------------------------
-- Table structure for studytype
-- ----------------------------
DROP TABLE IF EXISTS `studytype`;
CREATE TABLE `studytype`  (
  `id` int(0) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `state` int(0) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of studytype
-- ----------------------------
INSERT INTO `studytype` VALUES (1, '分享', 0);
INSERT INTO `studytype` VALUES (2, '求助', 0);
INSERT INTO `studytype` VALUES (3, '帮忙', 0);

-- ----------------------------
-- Table structure for systemmessage
-- ----------------------------
DROP TABLE IF EXISTS `systemmessage`;
CREATE TABLE `systemmessage`  (
  `id` bigint(0) NOT NULL COMMENT '公告id',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '公告标题',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '公告内容',
  `imglist` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '公告图片',
  `issue_user_id` int(0) NULL DEFAULT NULL COMMENT '发布人id',
  `issue_user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '发布用户名称',
  `issue_user_img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '发布用户头像',
  `state` int(0) NULL DEFAULT 0 COMMENT '公告状态',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `over_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of systemmessage
-- ----------------------------
INSERT INTO `systemmessage` VALUES (1626758117234241537, '', '欢迎大家来到，这是测试公告1', '', 10001, '宇宙最帅的那个人', 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/user-img/%E7%94%A8%E6%88%B7.jpg', 0, '2023-02-18 09:38:53', '2023-02-18 09:38:53', '2023-02-18 09:38:53');
INSERT INTO `systemmessage` VALUES (1626758209487958017, '', '欢迎大家来到，这是测试公告2', '', 10001, '宇宙最帅的那个人', 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/user-img/%E7%94%A8%E6%88%B7.jpg', 0, '2023-02-18 09:39:15', '2023-02-18 09:39:15', '2023-02-18 09:39:15');
INSERT INTO `systemmessage` VALUES (1626761461361209346, '', '欢迎大家来到，这是测试公告3', '', 10001, '宇宙最帅的那个人', 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/user-img/%E7%94%A8%E6%88%B7.jpg', 1, '2023-02-19 22:26:11', '2023-02-19 22:26:11', '2023-02-19 22:26:11');
INSERT INTO `systemmessage` VALUES (1627317997762465794, '新用户消息', '欢迎大家来到，这是欢迎您的系统信息', 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/助农服务/2EuKwhjfrxWk7d98125c5777ec2d58fe872b4bb73117.jpg,', 10001, '宇宙最帅的那个人', 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/user-img/%E7%94%A8%E6%88%B7.jpg', 0, '2023-02-19 22:43:39', '2023-02-19 22:43:39', '2023-02-19 22:43:39');

-- ----------------------------
-- Table structure for tixian
-- ----------------------------
DROP TABLE IF EXISTS `tixian`;
CREATE TABLE `tixian`  (
  `id` bigint(0) NOT NULL COMMENT '提现表',
  `tixianuser` int(0) NULL DEFAULT NULL COMMENT '提现用户id',
  `money` double NULL DEFAULT NULL COMMENT '提现金额',
  `state` int(0) NULL DEFAULT 0 COMMENT '0',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `over_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tixian
-- ----------------------------
INSERT INTO `tixian` VALUES (1081010156277334016, 0, 1, 0, '2023-03-03 00:28:25', '2023-03-03 00:28:25', '2023-03-03 00:28:25');
INSERT INTO `tixian` VALUES (1081015706687897600, 0, 1, 0, '2023-03-03 00:50:28', '2023-03-03 00:50:28', '2023-03-03 00:50:28');

-- ----------------------------
-- Table structure for tzmarket
-- ----------------------------
DROP TABLE IF EXISTS `tzmarket`;
CREATE TABLE `tzmarket`  (
  `id` bigint(0) NOT NULL COMMENT '跳蚤市场订单表',
  `type` int(0) NULL DEFAULT 0 COMMENT '订单类型',
  `ordertypeid` int(0) NULL DEFAULT NULL COMMENT '订单所属类型id',
  `state` int(0) NULL DEFAULT 0 COMMENT ' 0已发布，1买家已付款，2交易中，3交易完成，4问题订单，5已退款',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '订单标题',
  `content` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '订单内容',
  `issue_user` int(0) NULL DEFAULT NULL COMMENT '发布人id',
  `issue_user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '发布用户名称',
  `issue_user_img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '发布用户头像',
  `belong_user` int(0) NULL DEFAULT NULL COMMENT '接单人id',
  `pay_type` int(0) NULL DEFAULT NULL COMMENT '支付方式，0支付宝,1微信',
  `create_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '订单创建时间',
  `update_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '订单修改时间',
  `over_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '订单完成时间',
  `hide_name` int(0) NULL DEFAULT 0 COMMENT '是否匿名0不匿名，1匿名',
  `price` double(10, 2) NULL DEFAULT 0.00 COMMENT '订单价格',
  `order_img` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '订单图片',
  `index_img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '订单首页图片',
  `likeuser` int(0) NULL DEFAULT NULL COMMENT '订单喜欢的人数',
  `lodandnew` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '新旧程度',
  `norms` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '规格',
  `color` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '颜色',
  `model` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '型号',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `buytype`(`type`) USING BTREE,
  INDEX `buyissue_user`(`issue_user`) USING BTREE,
  INDEX `buybelong_user`(`belong_user`) USING BTREE,
  INDEX `buyorderid`(`ordertypeid`) USING BTREE,
  CONSTRAINT `tzmarket_b_user` FOREIGN KEY (`belong_user`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `tzmarket_i_user` FOREIGN KEY (`issue_user`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `tzmarket_ibfk_4` FOREIGN KEY (`ordertypeid`) REFERENCES `orderall` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `tzmarket_type` FOREIGN KEY (`type`) REFERENCES `tzmarkettype` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tzmarket
-- ----------------------------
INSERT INTO `tzmarket` VALUES (1079502691333308416, 1, 7, 3, '出售一个耳机', '多买了一个山水蓝牙耳机便宜出', 10003, '帅气的小王', 'https://cdn.uviewui.com/uview/album/3.jpg', 10001, NULL, '2023-02-26 20:38:18', '2023-02-26 20:43:17', '2023-02-26 20:38:18', 0, 1.00, 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/跳蚤市场/tmp_952ad3d6964af93372583b0058afde4c83e68af8582370ca.jpg,https://ft-glb.oss-cn-chengdu.aliyuncs.com/跳蚤市场/tmp_c29212773598d2a01ed299b397f1b98baff266f4aabfdd45.jpg', 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/跳蚤市场/tmp_952ad3d6964af93372583b0058afde4c83e68af8582370ca.jpg', 0, '全新未拆封', '中号', '白色', 'm7192b');
INSERT INTO `tzmarket` VALUES (1079504562437488640, 3, 7, -1, '出售一个笔记本', '新买的晨光笔记本便宜出了', 10001, '宇宙最帅的那个人', 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/用户头像/7EJBLrOeoTR959a32458f2a7a8ddec3933707fab9fda.jpg', 10003, NULL, '2023-02-26 20:45:44', '2023-02-26 23:45:49', '2023-02-26 20:45:44', 0, 1.00, 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/跳蚤市场/rW0OSJFg8NfL8dfef6d6ba7253204b23af36f1a7d8b9.jpg,https://ft-glb.oss-cn-chengdu.aliyuncs.com/跳蚤市场/9DYz49zbyBh9184b98106b8063bd14e41e6ef869aace.jpg', 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/跳蚤市场/rW0OSJFg8NfL8dfef6d6ba7253204b23af36f1a7d8b9.jpg', 0, '全新未拆封', '笔记本', '黑色', '晨光笔记本102');
INSERT INTO `tzmarket` VALUES (1079712416608026624, 2, 7, 3, '多买了一个保温杯', '富光保温杯304不锈钢茶杯真空商务杯壶男女士学生大容量直身水杯子', 10001, '宇宙最帅的那个人', 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/用户头像/7EJBLrOeoTR959a32458f2a7a8ddec3933707fab9fda.jpg', 10002, NULL, '2023-02-27 10:31:40', '2023-02-27 13:47:17', '2023-02-27 10:31:40', 0, 1.80, 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/跳蚤市场/2o2wqBFm8qlK8dfef6d6ba7253204b23af36f1a7d8b9.jpg,https://ft-glb.oss-cn-chengdu.aliyuncs.com/跳蚤市场/txa7Aw67cN1r184b98106b8063bd14e41e6ef869aace.jpg', 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/跳蚤市场/2o2wqBFm8qlK8dfef6d6ba7253204b23af36f1a7d8b9.jpg', 0, '全新未拆封', '480ML黑色', '黑色', '304不锈钢');
INSERT INTO `tzmarket` VALUES (1079764118782607360, 2, 7, 3, '多买了一盒牙膏', '多买了一盒牙膏，便宜出', 10002, '机智的小美', 'https://cdn.uviewui.com/uview/album/1.jpg', 10001, NULL, '2023-02-27 13:57:07', '2023-02-27 14:04:10', '2023-02-27 13:57:07', 0, 1.00, 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/跳蚤市场/tmp_69842b4008476f8e9b15c9b1216f58b1582f522a95d41aee.jpg,https://ft-glb.oss-cn-chengdu.aliyuncs.com/跳蚤市场/tmp_6653d5d44944f503d6d365e53730df23ffa1136b858ec051.jpg', 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/跳蚤市场/tmp_69842b4008476f8e9b15c9b1216f58b1582f522a95d41aee.jpg', 0, '全新', '黑人', '黑色', '无');
INSERT INTO `tzmarket` VALUES (1079766516196442112, 1, 7, 3, '1', '1', 10002, '机智的小美', 'https://cdn.uviewui.com/uview/album/1.jpg', 10001, NULL, '2023-02-27 14:06:38', '2023-02-27 15:10:07', '2023-02-27 14:06:38', 0, 2.00, 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/跳蚤市场/tmp_d4a10a70f79d12b6188ae9b90ad1e732c58dc4cf0d203379.jpg', 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/跳蚤市场/tmp_d4a10a70f79d12b6188ae9b90ad1e732c58dc4cf0d203379.jpg', 0, '1', '1', '1', '1');
INSERT INTO `tzmarket` VALUES (1079786852296163328, 1, 7, 3, '2', '2', 10002, '机智的小美', 'https://cdn.uviewui.com/uview/album/1.jpg', 10001, NULL, '2023-02-27 15:27:27', '2023-02-27 15:42:57', '2023-02-27 15:27:27', 0, 1.00, 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/跳蚤市场/tmp_6dba78458d3eb890a34fd1f77e2f591851b45583fabef2a2.jpg', 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/跳蚤市场/tmp_6dba78458d3eb890a34fd1f77e2f591851b45583fabef2a2.jpg', 0, '2', '2', '2', '2');
INSERT INTO `tzmarket` VALUES (1080093721707413504, 1, 7, 0, '出售一个蓝牙音响', '蓝牙音响便宜出', 10002, '机智的小美', 'https://cdn.uviewui.com/uview/album/1.jpg', NULL, NULL, '2023-02-28 11:46:51', '2023-02-28 11:46:51', '2023-02-28 11:46:51', 0, 1.00, 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/跳蚤市场/tmp_78f9741b8ec97bbfd0f4553bfccd5f683412286561bd7245.jpg,https://ft-glb.oss-cn-chengdu.aliyuncs.com/跳蚤市场/tmp_95c399f4c072824f9f4844cbf4276b64f03ebae20fbe99e4.jpg', 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/跳蚤市场/tmp_78f9741b8ec97bbfd0f4553bfccd5f683412286561bd7245.jpg', 0, '全新', '黑色20✘20cm', '蓝色', '');
INSERT INTO `tzmarket` VALUES (1080093738362994688, 1, 7, 0, '出售一个蓝牙音响', '蓝牙音响便宜出', 10002, '机智的小美', 'https://cdn.uviewui.com/uview/album/1.jpg', NULL, NULL, '2023-02-28 11:46:54', '2023-03-02 09:23:50', '2023-02-28 11:46:54', 0, 1.00, 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/跳蚤市场/tmp_78f9741b8ec97bbfd0f4553bfccd5f683412286561bd7245.jpg,https://ft-glb.oss-cn-chengdu.aliyuncs.com/跳蚤市场/tmp_95c399f4c072824f9f4844cbf4276b64f03ebae20fbe99e4.jpg', 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/跳蚤市场/tmp_78f9741b8ec97bbfd0f4553bfccd5f683412286561bd7245.jpg', 0, '全新', '黑色20✘20cm', '蓝色', '');

-- ----------------------------
-- Table structure for tzmarketorder
-- ----------------------------
DROP TABLE IF EXISTS `tzmarketorder`;
CREATE TABLE `tzmarketorder`  (
  `id` bigint(0) NOT NULL COMMENT '跳蚤市场订单表',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '订单标题',
  `goodsid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '商品id',
  `goodprice` double(10, 2) NULL DEFAULT NULL COMMENT '下单时商品单价',
  `goodtype` int(0) NULL DEFAULT NULL COMMENT '商品的分类',
  `buynum` int(0) NULL DEFAULT NULL COMMENT '购买的商品数量',
  `totalprice` double(10, 2) NULL DEFAULT NULL COMMENT '订单总价',
  `state` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '订单状态，0,未付款,1,已付款，2已发货，3以送到，4问题订单，5已退款',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `over_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '完成时间',
  `issuseuserid` int(0) NULL DEFAULT NULL COMMENT '创建订单用户id',
  `imglist` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '订单图片',
  `contacts` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '收货人',
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '电话',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '地址',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `goodsid`(`goodsid`) USING BTREE,
  INDEX `issuseuserid`(`issuseuserid`) USING BTREE,
  CONSTRAINT `tzmarketorder_ibfk_2` FOREIGN KEY (`issuseuserid`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tzmarketorder
-- ----------------------------

-- ----------------------------
-- Table structure for tzmarkettype
-- ----------------------------
DROP TABLE IF EXISTS `tzmarkettype`;
CREATE TABLE `tzmarkettype`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT 'tzmarket',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `state` int(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tzmarkettype
-- ----------------------------
INSERT INTO `tzmarkettype` VALUES (1, '电子产品', NULL);
INSERT INTO `tzmarkettype` VALUES (2, '生活用品', NULL);
INSERT INTO `tzmarkettype` VALUES (3, '学习用品', NULL);
INSERT INTO `tzmarkettype` VALUES (4, '交通工具', NULL);
INSERT INTO `tzmarkettype` VALUES (5, '其他', NULL);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `img` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `sex` int(0) NOT NULL DEFAULT 0 COMMENT '0女，1男，默认0',
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `address` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '',
  `state` int(0) NULL DEFAULT 0 COMMENT '0正常，1锁定，2逻辑删除',
  `limit` int(0) NULL DEFAULT 0 COMMENT ' 0普通用户，1vip用户，2管理员',
  `createTime` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '订单创建时间',
  `updateTime` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '订单修改时间',
  `lastmessage` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '最后一条消息',
  `totalMessage` int(0) NULL DEFAULT NULL COMMENT '用户消息总数',
  `user_friend` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '用户好友列表',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `over_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `money` double(10, 2) NULL DEFAULT 0.00 COMMENT '用户金额',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10007 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (10001, '宇宙最帅的那个人', 'ft099437', 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/用户头像/fyH0eYwCiGwM62b824bb48d92e588d5cd16d6e151594.jpg', 1, '17628178054', '[{\"address\":\"测试\",\"defaultaddress\":true,\"id\":10001,\"name\":\"王伟\",\"phone\":\"15668987856\"},{\"address\":\"俊园3号公寓俊3-311\",\"defaultaddress\":false,\"id\":10001,\"name\":\"张三\",\"phone\":\"17628178054\"}]', 0, 2, '2022-10-03 21:12:25', '2023-02-28 11:43:34', '', NULL, NULL, '2023-02-28 11:43:34', '2023-02-28 11:43:34', '2023-02-28 11:43:34', 5.45);
INSERT INTO `user` VALUES (10002, '机智的小美', 'ft099437', 'https://cdn.uviewui.com/uview/album/1.jpg', 0, '15082778996', '[{\"address\":\"1\",\"defaultaddress\":false,\"id\":10002,\"name\":\"测试\",\"phone\":\"15082778996\"},{\"address\":\"测试\",\"defaultaddress\":false,\"id\":10002,\"name\":\"范涛\",\"phone\":\"17602857862\"}]', 0, 0, '2022-10-04 20:58:19', '2023-02-27 15:42:57', NULL, NULL, NULL, '2023-02-27 15:42:57', '2023-02-27 15:42:57', '2023-02-27 15:42:57', 3.76);
INSERT INTO `user` VALUES (10003, '帅气的小王', 'ft099437', 'https://cdn.uviewui.com/uview/album/3.jpg', 0, '17602857862', '[{\"address\":\"秀园3-312\",\"defaultaddress\":true,\"id\":10003,\"name\":\"小李\",\"phone\":\"17628178054\"}]', 0, 0, '2022-10-04 20:59:59', '2023-02-26 20:43:17', NULL, NULL, NULL, '2023-02-26 20:43:17', '2023-02-26 20:43:17', '2023-02-26 20:43:17', 3.76);
INSERT INTO `user` VALUES (10004, '机灵的王伟', 'ft099437', 'https://cdn.uviewui.com/uview/album/4.jpg', 0, '17765519698', '[{\"address\":\"长度\",\"defaultaddress\":false,\"id\":10004,\"name\":\"王伟\",\"phone\":\"17628178054\"}]', 0, 0, '2022-10-04 21:11:45', '2023-03-02 13:12:59', NULL, NULL, NULL, '2023-03-02 13:12:59', '2023-03-02 13:12:59', '2023-03-02 13:12:59', 0.00);
INSERT INTO `user` VALUES (10005, '优雅的赵六', 'ft099437', 'https://cdn.uviewui.com/uview/album/1.jpg', 0, '18728470815', '', 0, 0, '2022-10-13 21:09:59', '2023-02-27 14:03:42', NULL, NULL, NULL, '2023-02-27 14:03:42', '2023-02-27 14:03:42', '2023-02-27 14:03:42', 0.00);
INSERT INTO `user` VALUES (10006, '无聊的赵六帅哥', 'ft099437', 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/用户头像/tmp_b75b07262400936bfe18829ebb1d8d119ba66563f99c48e8.jpg', 1, '17608183305', '[{\"address\":\"俊三311\",\"defaultaddress\":true,\"id\":10006,\"name\":\"张三\",\"phone\":\"17765519698\"}]', 0, 0, '2022-10-13 21:16:17', '2023-02-27 14:03:44', NULL, NULL, NULL, '2023-02-27 14:03:44', '2023-02-27 14:03:44', '2023-02-27 14:03:44', 0.00);

-- ----------------------------
-- Table structure for workorder
-- ----------------------------
DROP TABLE IF EXISTS `workorder`;
CREATE TABLE `workorder`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '帮我做',
  `type` int(0) NULL DEFAULT NULL COMMENT '订单类型',
  `state` int(0) NULL DEFAULT NULL COMMENT ' 0发布，1已付款，2接单中，3完成,，4问题订单',
  `content` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '订单内容',
  `contacts` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '联系人名称',
  `contacts_phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '联系人电话',
  `contacts_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '联系人地址',
  `issue_user` int(0) NULL DEFAULT NULL COMMENT '发布人id',
  `issue_user_img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '发布用户头像',
  `belong_user` int(0) NULL DEFAULT NULL COMMENT '接单人id',
  `pay_type` int(0) NULL DEFAULT NULL COMMENT '支付方式，0支付宝,1微信',
  `create_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '订单创建时间',
  `update_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '订单修改时间',
  `over_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '订单完成时间',
  `hide_name` int(0) NULL DEFAULT 0 COMMENT '是否匿名0不匿名，1匿名',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '订单标题',
  `price` int(0) NULL DEFAULT NULL COMMENT '订单价格',
  `order_img` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '订单图片',
  `remarks` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '订单备注',
  `ordertypeid` int(0) NULL DEFAULT NULL COMMENT '订单所属类型id',
  `issue_user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '发布用户名称',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `worktype`(`type`) USING BTREE,
  INDEX `workissue_user`(`issue_user`) USING BTREE,
  INDEX `workbelong_user`(`belong_user`) USING BTREE,
  INDEX `workistype`(`ordertypeid`) USING BTREE,
  CONSTRAINT `workbelong_user` FOREIGN KEY (`belong_user`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `workissue_user` FOREIGN KEY (`issue_user`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `workistype` FOREIGN KEY (`ordertypeid`) REFERENCES `orderall` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `worktype` FOREIGN KEY (`type`) REFERENCES `worktype` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1079447729295327232 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of workorder
-- ----------------------------
INSERT INTO `workorder` VALUES (1078293080257331200, 2, 4, '邮政快递，小件', '小李', '17628178054', '秀园3-312', 10003, 'https://cdn.uviewui.com/uview/album/3.jpg', 10001, NULL, '2023-02-23 12:31:45', '2023-02-26 16:39:00', '2023-02-23 12:31:45', 0, '帮我取下快递', 1, 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/帮我做/寄取快递/owloZ2bkw31s8dfef6d6ba7253204b23af36f1a7d8b9.jpg,https://ft-glb.oss-cn-chengdu.aliyuncs.com/帮我做/寄取快递/EQXja8y7k6pjb0f270306a7e40b2d6fe8e450c1deda4.jpg', '越快越好', 3, '帅气的小王');
INSERT INTO `workorder` VALUES (1079055475984564224, 3, 4, '今天中午12点在食堂帮我打一份黄焖鸡', '张三', '17628178054', '俊园3号公寓俊3-311', 10001, 'https://ft-glb.oss-cn-chengdu.aliyuncs.com/用户头像/7EJBLrOeoTR959a32458f2a7a8ddec3933707fab9fda.jpg', 10003, NULL, '2023-02-25 15:01:15', '2023-02-26 17:39:33', '2023-02-25 15:01:15', 0, '帮我打饭', 1, '', '无', 3, '宇宙最帅的那个人');
INSERT INTO `workorder` VALUES (1079447729295327232, 4, 4, '做核算帮我排个队，男生最好', '小李', '17628178054', '秀园3-312', 10003, 'https://cdn.uviewui.com/uview/album/3.jpg', 10001, NULL, '2023-02-26 16:59:55', '2023-02-26 17:46:06', '2023-02-26 16:59:55', 0, '帮我排队', 1, '', '越快越好', 3, '帅气的小王');

-- ----------------------------
-- Table structure for worktype
-- ----------------------------
DROP TABLE IF EXISTS `worktype`;
CREATE TABLE `worktype`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '帮我找类型表',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '类型名称',
  `state` int(0) NULL DEFAULT 0 COMMENT '类型状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of worktype
-- ----------------------------
INSERT INTO `worktype` VALUES (1, '打印资料', 0);
INSERT INTO `worktype` VALUES (2, '寄取快递', 0);
INSERT INTO `worktype` VALUES (3, '食堂打饭', 0);
INSERT INTO `worktype` VALUES (4, '代写服务', 0);
INSERT INTO `worktype` VALUES (5, '其他服务', 0);

SET FOREIGN_KEY_CHECKS = 1;
