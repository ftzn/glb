package com.ft.service;

import com.ft.domain.Worktype;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author com/ft
 * @since 2022-08-24
 */
public interface IWorktypeService extends IService<Worktype> {

}
