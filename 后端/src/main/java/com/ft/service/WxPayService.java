package com.ft.service;

import com.ft.domain.WeiXinPayToUniapp;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Author: ft
 * @Date: 2022/10/09/15:27
 * @Description:
 */
public interface WxPayService {
    /**
     * 微信支付
     * @return
     */
    @Transactional
    public WeiXinPayToUniapp wxPay(String url, String jsonString) throws Exception;

    /**
     * 退款
     */
    @Transactional
    public int wxTk(String url, String jsonString) throws Exception;
}
